## specnm: Spectral element normal mode code<br />
#### Authors: Johannes Kemper, Martin van Driel, Federico Munch<br />
Mailto: johannes.kemper@erdw.ethz.ch<br />
[![build status](https://gitlab.com/JohKem1/specnm/badges/main/pipeline.svg)](https://gitlab.com/JohKem1/specnm) 
<br />
<br />
### What is specnm?<br />
specnm is a tool for the computation of gravito-elastic free oscillations or normal modes of spherically symmetric bodies based on a spectral element discretization of the underlying radial ordinary differential equations as given in the journal publication.<br />
If you find this code useful, please cite:<br />
> J Kemper, M van Driel, F Munch, A Khan, D Giardini, A spectral element approach to computing normal modes, Geophysical Journal International, Volume 229, Issue 2, May 2022, Pages 915–932, [https://doi.org/10.1093/gji/ggab476](https://doi.org/10.1093/gji/ggab476)

<details>
<summary><b>BiBTeX data</b></summary>
@<area>article{Kemper_etal2021,<br />
    author = {Kemper, J and van Driel, M and Munch, F and Khan, A and Giardini, D},<br />
    title = "{A spectral element approach to computing normal modes}",<br />
    journal = {Geophysical Journal International},<br />
    volume = {229},<br />
    number = {2},<br />
    pages = {915-932},<br />
    year = {2021},<br />
    month = {12},<br />
    issn = {0956-540X},<br />
    doi = {10.1093/gji/ggab476},<br />
    url = {https://<area>doi.org/10.1093/gji/ggab476},<br />
    eprint = {https://<area>academic.oup.com/gji/article-pdf/229/2/915/43338209/ggab476.pdf},<br />
}<br />
</details>

### Documentation
Find the documentation [here](https://johkem1.gitlab.io/specnm).<br />

### Tests
After installation you can run tests in the specnm folder with the command:<br />
```bash
pytest
```
to check if everything works fine.<br />

### Dependencies
General:<br />
- pytest
- matplotlib
- sympy
- scipy
- flake8
- slepc4py

Optional for seismogram generation (only with python 3.8.8):<br />
- h5py
- obspy
- instaseis
