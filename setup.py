from setuptools import setup, find_packages

setup(
    name='specnm',
    version='1.1.2',
    author='Johannes Kemper, Federico Munch, Martin van Driel',
    author_email='johannes.kemper@erdw.ethz.ch',
    description='Spectral element normal mode code.',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/JohKem1/specnm',
    license='GNU GENERAL PUBLIC LICENSE Version 3',
    packages=find_packages(),
    install_requires=[
        "pytest",
        "matplotlib",
        "sympy",
        "scipy",
        "flake8",
        "slepc4py",
    ],
    extras_require={
        # Optional seismogram dependencies (strictly Python 3.8.8)
        "seismogram": [
            "h5py; python_version=='3.8.8'",
            "obspy; python_version=='3.8.8'",
            "instaseis; python_version=='3.8.8'"
        ]
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.8.8',
)
