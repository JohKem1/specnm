****************************
Mode database and seismogram
****************************

**specnm** is able to store mode data in a compressed database and calculate 
synthetic seismograms from such a mode database by mode summation. 

.. contents:: Table of Contents
   :local:

Mode database
=============

**specnm** uses the `h5 file format <https://docs.h5py.org/en/stable/index.html>`_ to store modes in a compressed database.

.. note::

    The HDF5 binary data format, allows users to store extensive numerical data and manipulate it seamlessly with NumPy. 

.. autoclass:: specnm.seismogram.mode_database

Setup the **mode_database** class that internally calls the compute function:

.. autofunction:: specnm.seismogram.mode_database.compute

One can also read a precomputed .h5 database:

.. autofunction:: specnm.seismogram.mode_database.read

A computed **mode_database** can be written to a compressed h5 file by:

.. autofunction:: specnm.seismogram.mode_database.write_h5

Seismogram
==========

After a **mode_database** is computed or read one can calculate seismograms of the database by using:

.. autofunction:: specnm.seismogram.mode_database.get_seismograms

**Code example**

.. literalinclude:: ../specnm/examples/seismogram.py
  :language: python
  :linenos:

**Output**

In this output we omit the calculation related output of specnm.

.. code-block:: console 

    $ python seismogram.py
    Polynomial order toroidal run: 5
    Arguments used to produce this database:
    {'model': '../models/prem_noocean',
     'fmax': 0.005,
     'mode_types': 'TRS',
     'gravity_mode': 'full',
     'verbose': True,
     'init_kwargs': {'verbose': 2},
     'problem_kwargs': {}}

.. plot:: ../specnm/examples/seismogram.py
   :context: reset

Direct interface
================

There is a direct interface to the seismogram class that can be used like this:

.. code-block:: console 

    $ python -m specnm.interface --model specnm/models/prem_noocean --moment_tensor 1e24 0.0 0.0 0.0 0.0 0.0 --loc_source 60.0 30.0 10000.0 --loc_receiver 80.0 40.0 0.0

Some more information on the input parameters for the interface can be displayed with:

.. code-block:: console 

    $ python -m specnm.interface --help
    Interface for seismology class of spectral element normal mode code (specnm)

    optional arguments:
      -h, --help            show this help message and exit
      --fmin FMIN           Minimum frequency for modes (default 0.5/3600 = PREM)
      --fmax FMAX           Maximum frequency for modes (default 0.05 Hz)
      --model MODEL, --m MODEL
                            1D mesher model file (deck, poly, etc.)
      --attenuation, --at   Attenuation on or off (default off)
      --gravity GRAVITY, --g GRAVITY
                            Type of gravity ('full', 'cowling', 'none')
      --moment_tensor M [M ...], --mt M [M ...]
                            Moment tensor of source in format --mt M_rr M_tt M_pp M_rt M_rp M_tp
      --loc_source LOC_SOURCE [LOC_SOURCE ...], --locs LOC_SOURCE [LOC_SOURCE ...]
                            Source location in format --locs lat_in_deg long_in_deg depth_in_m
      --loc_receiver LOC_RECEIVER [LOC_RECEIVER ...], --locr LOC_RECEIVER [LOC_RECEIVER ...]
                            Receiver location in format --locr lat_in_deg long_in_deg depth_in_m
      --start_time START_TIME, --t0 START_TIME
                            Start time for seismogram generation
      --stop_time STOP_TIME, --tn STOP_TIME
                            Stop time for seismogram generation
      --dt DT               dt for seismogram generation
      --read_file READ_FILE, --rfile READ_FILE
                            Read database from the specified filename
      --save_file SAVE_FILE, --sfile SAVE_FILE
                            Save database to filename with the specified filename
      --verbose, --v        Whether to print some textual output.
      --kind KIND           Which kind of trace [displacement, velocity, acceleration, displacement anelastic] (defaults to displacement)

