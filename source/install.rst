************
Installation
************

To install specnm there are multiple options:

.. note::

    We strongly recommend users to install a python environment manager.

If you use `anaconda <https://docs.conda.io/en/latest/miniconda.html/>`_ you may want to add conda-forge: 

.. code-block:: console

    conda config --add channels conda-forge 
    conda config --set channel_priority strict

The easiest way to install specnm is by making a new environment and using pip: 

.. code-block:: console

    conda create --name specnm
    conda activate specnm
    pip install specnm@https://gitlab.com/JohKem1/specnm/-/archive/main/specnm-main.zip

There is also the way to install specnm with seismogram support:

.. code-block:: console

    conda create --name specnm python=3.8.8
    conda activate specnm
    pip install specnm[seismogram]@https://gitlab.com/JohKem1/specnm/-/archive/main/specnm-main.zip

If this does not work for you, please continue with one of the following methods below, but start by downloading the code from the repository:

Downloading of repository and direct installation
=================================================

Clone the specnm git repository onto your harddisc:

.. code-block:: console

    git clone https://gitlab.com/JohKem1/specnm.git

Or directly download the code into a separate specnm folder then change directory (cd) into that folder.

Method 1
========
Please create a new conda environment for specnm and install the packages, activate the new environment and install specnm:

.. code-block:: console

    conda create -y --name specnm --file requirements.txt
    conda activate specnm
    conda develop <folder_to_specnm-or-.>

Method 2
========
Directly create the conda environment from the developer specnm yaml file (this is prefered if you want to use seismograms):

.. code-block:: console

    conda env create -f specnm.yml
    conda activate specnm
    conda develop <folder_to_specnm-or-.>