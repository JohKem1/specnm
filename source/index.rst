********************
specnm documentation
********************

.. toctree::
   :caption: Contents
   :hidden:
   :maxdepth: 1

   self
   install
   basicusage
   advancedusage
   seismogram
   visualization
   method_and_solver

.. automodule:: specnm
   :noindex:

Advantages using specnm
=======================

* Versatile in its usability for any spherical body (e.g. planet, moon)
    * `Earth <visualization.html#simple-eigenfunction-plot>`_
    * `Mars <advancedusage.html#usage-for-general-planet-or-moon>`_ (only liquid core, liquid-liquid interaction)
    * `Europa <visualization.html#interactive-spectrum-plot>`_ (specific ice related modes, such as Crary modes)
    * `Moon <advancedusage.html#usage-for-general-planet-or-moon>`_
* Implicit boundary conditions improve accuracy for boundary mode types (Stoneley modes).
* Flexibility in choise of polynomial degree of the Lagrange basis functions allows for a simple balancing of higher accuracy and efficiency of the solution. 
* No real upper limit of frequency (except max. RAM needed for caching matrices).
* Guarantee to find all modes below defined maximum frequency (avoid the potentially unstable mode counting in former codes).
* Simplicity in usage based on modern python.

Publications using specnm
=========================

.. bibliography::
   :filter: False
   :style: unsrt

   Duran_etal2024
   Bissig_etal2022
   vanDriel_etal2021

Possible extensions
===================

* Identifier for overtone number.
* Athmospheric interaction.
* Update to most recent Python version (for example type hinting).

Base class
==========

.. note::
   
   As this class is a meta class you will never directly call it, but will use
   it by initializing **rayleigh_fg**, **rayleigh**, **radial** or **toroidal** classes.

.. autoclass:: specnm.specnm_base.spc_base
    :special-members: __init__

**Code example**

.. code-block:: python
    :linenos:

    # instantiate the base class within a toroidal calculation setup
    # example for prem anisotropic model and up to a mesh frequency of 10 mHz
    from specnm import love

    lov = love(model='prem_ani', fmax=0.01)

Models
======

.. autoclass:: specnm.models_1D.model

One can read any 1D model with the following class member.

.. autoclass:: specnm.models_1D.model.read

.. code-block:: python
    :linenos:

    # instantiate numpy and base class of models_1D
    import numpy as np
    from specnm import models_1D

    # read anisotropic prem model from file
    m = models_1D.model.read('prem_ani')

    # get QMU value at 3500.0 km radius
    m.get_elastic_parameter('QMU', np.array([3500.0 / 6371.0]))

    # output: 312

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
