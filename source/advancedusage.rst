**************
Advanced usage
**************

Here you will find some approaches to tackle occuring challenges while using specnm.

.. contents:: Table of Contents
   :local:

Sensitivity kernels
===================

To generate sensivity kernels with specnm it is enough to set the **sensitivity_kernels** parameter to either *isotropic* or *anisotropic* in the **problem** function of the **radial**, **rayleigh**, **rayleigh_fg**, **love** classes. 

.. note::

    The sensitivity kernels are not benchmarked by tests, yet.

**Code example**

.. literalinclude:: ../specnm/examples/sensitivity.py
  :language: python
  :linenos:

.. plot:: ../specnm/examples/sensitivity.py
   :context: reset

Usage for general planet or moon
================================

To calculate the modes of a planet or moon that is not Earth there is the following recipe one can follow. 
In general, toroidal and radial modes are not much of a problem as they are much simpler in that they only have one eigenfunction each and are not very much affected by the inherent fluid complexities. 
Hence, this explanation is mainly concerned with the calculation of spheroidal modes. 

We need to find the specific minimum frequency (**fmin**) at which the physical spheroidal modes start for the new model. 
It is best practice to start very low (**fmin** < 1e-6) and if the code does not manage to produce a spectrum increase it slightly (see code example below). 
An additional important parameter is the **fmax** as one should start low to keep the calculation efficient. 
In general, smaller bodies have higher frequencies due to their smaller volume, a good indication here is Earth, where the gravest physical mode (ignoring Slichter mode) is at 54 minutes period. 
It then takes some experience in estimating the good maximum frequency of the spectrum for your body of choice in relation to the size of the Earth, but a good start might be something around **fmax** from 2 to 5 mHz. 
Looking at the fluid/solid kinetic energy ratio (1 is a mode that lives solely in the fluid and hence is an undertone) and the mapping error of the modes (high error means the mode might be a spurious numerical mode) allows one to estimate good boundaries for the cutoff values (**energy_ratio_spurious**, **epsilon_spurious**). 
For **energy_ratio_spurious** modes above the set value will get filtered from the spectrum and for **epsilon_spurious** modes above the specified error value will be filtered. 

There is more explanation on how to handle this including some visual help in the sense of plots within the following example for the Moon model :cite:p:`Garcia_etal2011`:

**Code example**

.. literalinclude:: ../specnm/examples/planet.py
 :language: python
 :linenos:

The following plot might help with the selection of the aforementioned filter parameters. 
The left panel of the plot show the energy ratio fluid/solid with the boundary value **energy_ratio_spurious** as a horizontal red line, while the minimum frequency is the vertical red line. 
The right panel shows the error of the solution after remapping which could be interpreted as error due to the finite discretization of the infinite spectrum on the modes. 
The horizontal red line is the boundary given by **epsilon_spurious**. 
The Modes are indicated as black crosses.

.. plot:: ../specnm/examples/planet.py
  :context: reset

What follows is an example to show a simple spectrum after finding the appropriate minimum frequency **fmin** for Mars with molten layer on top of core :cite:p:`Khan_etal2023`:

.. literalinclude:: ../specnm/examples/mars_lvl.py
 :language: python
 :linenos:

This corresponding spheroidal spectrum plot:

.. plot:: ../specnm/examples/mars_lvl.py
  :context: reset

Calculate Slichter mode
=======================

The Slichter mode :cite:p:`Slichter1961` is a mode that describes the 3-D translation of the Earth's solid inner core within the fluid outer core affected by buoyancy. 
Understanding the Slichter mode offers insights into the core's structural composition and dynamics, as its eigenperiod reveals details about density contrasts across the inner core boundary, while the associated quality factor (Q value) sheds light on energy dissipation mechanisms within the core :cite:p:`Ding_etal2013`.
We give an easy example here to calculate the Slichter mode for Earth :cite:p:`{PREM, }Dziewonski_etal1981` and plot its corresponding eigenfunctions in radius. 

**Code example**

.. literalinclude:: ../specnm/examples/slichter.py
  :language: python
  :linenos:

**Output**

.. code-block:: console

    $ python slichter.py
    slichter_period_in_h=5.419914516824565

.. plot:: ../specnm/examples/slichter.py
   :context: reset

Kinetic energy
==============

One can calculate the kinetic energy of modes in specific regions as follows:

**Code example**

.. literalinclude:: ../specnm/examples/kineticenergy.py
  :language: python
  :linenos:

**Output**

.. code-block:: console

    $ python kineticenergy.py
    Kinetic energies in fluid outer core ekin:
    nSl: frequency in mHz, period in s, ekin
    2S1:  0.38146,  2621.50019, 0.29048
    3S1:  0.95598,  1046.04485, 0.26479
    0S2:  0.24215,  4129.65772, 0.44165
    1S2:  0.68503,  1459.78091, 0.09872
    2S2:  0.86384,  1157.62342, 0.41918
    0S3:  0.39891,  2506.85959, 0.33266
    1S3:  0.94735,  1055.58077, 0.06326
    0S4:  0.59018,  1694.39293, 0.27057
    0S5:  0.79467,  1258.37940, 0.22778

Skin depth
==========

The skin depth in seismology refers to the depth at which the amplitude of the surface wave decays to 
:math:`1/\mathrm{e}` (about 37%) of its value at the surface. 
Understanding the skin depth of modes is crucial for various applications, including seismic imaging, earthquake monitoring, and exploration geophysics, as it helps to estimate the penetration depth of surface waves and interpret seismic data accurately. 

**Code example**

.. literalinclude:: ../specnm/examples/skindepth.py
  :language: python
  :linenos:

.. plot:: ../specnm/examples/skindepth.py
   :context: reset

Running specnm in HPC environment
=================================

To run specnm in an HPC environment on one core and not interfere with other processes and restrict the memory use one can use the following snippet:

**Code example**

.. literalinclude:: ../specnm/examples/multiproc.py
  :language: python
  :linenos:

**Output**

.. code-block:: console

    $ python multiproc.py
    First frequency in mHz= 0.6793496816552975
    Absolute difference in mHz= 0.0009903183447024623

Accuracy of solution
====================

The accuracy of the solution depends on the order of the Ansatzfunctions. 
By losing some efficiency one can run the code with a higher order by specifying **n** (default=5) in the base class, like this:

.. code-block:: python
   :linenos:

    from specnm import rayleigh_fg
    ray = rayleigh_fg(model='../models/prem_ani', n=9, fmax=0.01)

Other than that there is the option to increase the accuracy of the eigenvalue solver by specifying a smaller **residual_tol** (default=1e-12) in the problem statement (see :doc:`method_and_solver`):

.. code-block:: python
   :linenos:

    ray_out = rayleigh_problem(residual_tol=1e-15)

Renormalization of eigenfunctions
=================================

To renormalize the eigenfunctions we can make use of the integration of the eigenfunctions using the internal integral representation. 

**Code example**

.. literalinclude:: ../specnm/examples/renormalize.py
  :language: python
  :linenos:

.. plot:: ../specnm/examples/renormalize.py
   :context: reset

Bibliography
============

.. bibliography::
   :filter: docname in docnames 
