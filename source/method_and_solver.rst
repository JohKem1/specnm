*****************
Method and Solver
*****************

.. contents:: Table of Contents
   :local:

======
Method
======

Specnm uses the spectral element method (SEM) to solve the equations governing spherically symmetric normal modes. 
The problem can be separated into two mode types called toroidal and spheroidal modes. 
Additionally, there is a special subclass of spheroidal modes known as radial modes, which exhibit purely radial motion (corresponding to an angular order :math:`l` of zero). 

Toroidal modes are interferences of SH waves and do not propagate through liquid regions, where SH waves cannot exist. 
In contrast, spheroidal modes arise from the interference of pressure (P) waves and shear vertical (SV) waves and can propagate through both solid and liquid regions. 
Instead of the usual way of solving the first order ordinary differential equations in radius of the eigenfunctions associated with the modes, we combine the equations to form second order equations in radius. 
Afterwards we transform the equations into the weak form (integral form) and also set the equations into a symmetrized form to avoid problems related to the usual zero radius poles encountered in the equations given in the literature, see for example :cite:t:`TakeuchiSaito1972`. 

Below, the specific weak forms of the toroidal, radial, and spheroidal equations implemented in specnm are presented and explained. 
These equations include all relevant terms and boundary conditions.

Toroidal equation
=================

The weak symmetrized form for describing toroidal modes, which are equivalent to SH or Love waves, is given by:

.. math::
    :label: eq.toroidal

       \omega^2 \int_\Omega \rho &W \tilde W r^2\,\mathrm{d}r ={} \\ 
            -& \left[T_W \tilde{W} r^2\right]_{r_0}^{r_\mathrm{top}} + \left[T_W \tilde{W} r^2\right]_-^+ \\
            +& \int_\Omega L (r \partial_r W - W) (r \partial_r \tilde W - \tilde W)  \,\mathrm{d}r \\
            +& \int_\Omega N(k^2 - 2) W \tilde W \,\mathrm{d}r.

Here, :math:`\omega` is the angular frequency, :math:`k` is the wavenumber, :math:`\rho` is the density, :math:`r` is the radial coordinate and :math:`W` is the toroidal eigenfunction with its symmetric counterpart :math:`\tilde{W}`. 
The anisotropic material parameters are :math:`L` and :math:`N`. The traction :math:`T_W` is given by:

.. math::

    T_W = L (\partial_r W - r^{-1} W).

The first boundary term,

.. math::

    \left[T_W \tilde{W} r^2\right]_{r_0}^{r_\mathrm{top}},

applies to the boundary condition of the selected solid layer :math:`\Omega`. Here, :math:`r_0` denotes the bottom of the layer (such as the core-mantle boundary, CMB, in the Earth), while :math:`r_\mathrm{top}` represents the top boundary of the layer, which could be the surface or the top boundary of any solid layer within the planet. 
This boundary condition ensures that the traction associated with the toroidal displacement field is zero at the boundaries, reflecting the fact that no external shear forces act on these boundaries, whether they are at the surface or at the interface of another solid layer. 

The second boundary term,

.. math::

    \left[T_W \tilde{W} r^2\right]_-^+,

addresses the continuity of the toroidal eigenfunction :math:`W` across internal interfaces within the medium. This term represents the jump condition, ensuring that the eigenfunction remains continuous when crossing layers with different material properties. 

Radial equation
===============
The weak form of the radial equation, which describes purely radial modes in the spherical symmtric body :math:`\Omega`, is expressed as:

.. math::
   :label: eq.radial

        \omega^2 \int_\Omega \rho &U \tilde U r^2 \,\mathrm{d}r ={} \\
            -& \left[T_U(V=0) \tilde{U} r^2\right]_{0}^{r_\mathrm{surf}} + \left[T_U(V=0) \tilde{U} r^2\right]_-^+ \\
            +& \int_\Omega C r^2 \partial_r U \partial_r \tilde U  \,\mathrm{d}r \\
            +& \int_\Omega 2Fr(U\partial_r \tilde U + \partial_r U \tilde U) \,\mathrm{d}r \\
            +& \int_\Omega [4 (A - N) - 4 \rho g r] U \tilde U \,\mathrm{d}r. 

In equation :eq:`eq.radial`, :math:`\omega` is the angular frequency, :math:`k` is the wavenumber, :math:`\rho` is the density, and :math:`r` is the radial coordinate. 
The symbols :math:`U` and :math:`\tilde{U}` denote the radial eigenfunction and its corresponding symmetric function, respectively. 
The parameters :math:`C`, :math:`F`, :math:`A`, and :math:`N` are material properties that reflect the anisotropic medium. :math:`g` represents the gravitational acceleration.
The traction :math:`T_U` is given by the one given in the spheroidal equation section below with :math:`V` set to zero. 

Two boundary conditions are expressed as terms within square brackets. The first boundary term,

.. math::

    \left[T_U(V=0) \tilde{U} r^2\right]_{0}^{r\mathrm{surf}},

represents the free surface boundary condition at the surface denoted by :math:`r_\mathrm{surf}`. This condition ensures that the stress at the free surface is zero, reflecting the fact that there is no external force acting on the surface of the planet. The second boundary term,

.. math::

    \left[T_U(V=0) \tilde{U} r^2\right]_-^+,

represents the jump condition. This condition ensures that the radial eigenfunction :math:`U` remains continuous across internal boundaries, where material properties might change abruptly. 

Together, these integrals and boundary conditions describe the balance of kinetic and potential energies in the radial modes, while also ensuring that the solution satisfies the necessary physical constraints at the surface and across internal boundaries.

Spheroidal equation
===================
The weak form of the full gravity spheroidal equation, which governs the spheroidal modes of a spherically symmetric body, with radial domain :math:`\Omega`, is expressed as:

.. math::
   :label: eq.spheroidal

        \omega^2 \int_\Omega \rho  &(U \tilde U + V \tilde V) r^2 \,\mathrm{d}r ={}\\
        & - \left[T_U \tilde{U} r^2\right]_{0}^{r_\mathrm{surf}} - \left[T_V \tilde{V} r^2\right]_{0}^{r_\mathrm{surf}} - \left[T_P \tilde{P} r^2\right]_{0}^{r_\mathrm{surf}}\\
    & + \left[T_U \tilde{U} r^2\right]_-^+ + \left[T_V \tilde{V} r^2\right]_-^+ + \left[T_P \tilde{P} r^2\right]_-^+ \\
        &+ \int_\Omega L (k U - V + r \partial_r V) (k \tilde U - \tilde V + r \partial_r \tilde V) \,\mathrm{d}r \\
        &+ \int_\Omega (A - N) (2U - kV) (2 \tilde U - k \tilde V) \,\mathrm{d}r \\
        &- \int_\Omega Fr [2(\partial_r U\tilde U + U \partial_r \tilde U) - k(V\partial_r \tilde U + \partial_r U \tilde V)] \,\mathrm{d}r \\
        &+ \int_\Omega \rho g r[-4 U \tilde U + k (V \tilde U + U \tilde V)] \,\mathrm{d}r \\
        &+ \int_\Omega C r^2 \partial_r U \partial_r \tilde U \,\mathrm{d}r \\
        &+ \int_\Omega N (k^2 - 2) V \tilde V \,\mathrm{d}r \\
        &+ \int_\Omega \rho r^2 (\partial_r P \tilde U + P \partial_r \tilde U) \,\mathrm{d}r \\
        &+ \int_\Omega 4 \pi G \rho^2 r^2 U \tilde U \,\mathrm{d}r \\
        &+ \int_\Omega k \rho r (P \tilde V + V \tilde P) \, \mathrm{d}r \\
        &+ \int_\Omega (4 \pi G)^{-1} r^2 \partial_r P \partial_r \tilde P \, \mathrm{d}r \\
        &+ \int_\Omega (l+1) (4 \pi G)^{-1} r (P \partial_r \tilde P + \partial_r P \tilde P) \, \mathrm{d}r \\
        &+ \int_\Omega (l+1)^2 (4 \pi G)^{-1} P \tilde P \, \mathrm{d}r

In equation :eq:`eq.spheroidal`, :math:`\omega` is the angular frequency, :math:`k` is the wavenumber, :math:`\rho` is the density of the medium, and :math:`r` is the radial coordinate. 
The variables :math:`U`, :math:`V`, and :math:`P` represent the radial, tangential, and gravity potential perturbation eigenfunctions, respectively, with :math:`\tilde{U}`, :math:`\tilde{V}`, and :math:`\tilde{P}` being their corresponding symmetrized functions. 
The parameters :math:`C`, :math:`F`, :math:`L`, :math:`A`, and :math:`N` are anisotropic material properties.
:math:`g` is the gravitational acceleration and :math:`G` is the universal gravitational constant.
The tractions :math:`T_U`, :math:`T_V`, and :math:`T_P` are given by:

.. math::

    T_U &= C \partial_r U + F r^{-1} (2 U - k V),\\
    T_V &= L ( \partial_r V - r^{-1} V + k r^{-1} U ),\\
    T_P &= \partial_r P + 4 \pi G \rho U + (l+1) r^{-1} P.

The equation includes three sets of boundary conditions, represented by terms within square brackets. The first set of boundary terms,

.. math::

    \left[T_U \tilde{U} r^2\right]_{0}^{r\mathrm{surf}}, \quad \left[T_V \tilde{V} r^2\right]_{0}^{r\mathrm{surf}}, \quad \text{and} \quad \left[T_P \tilde{P} r^2\right]_{0}^{r\mathrm{surf}},

corresponds to the free surface boundary condition. These conditions ensure that the traction components associated with the radial, tangential, and gravity potential perturbation fields are zero at the free surface, consistent with the absence of external forces acting on the surface.

The second set of boundary terms,

.. math::
    \left[T_U \tilde{U} r^2\right]_-^+ \quad \text{and} \quad \left[T_P \tilde{P} r^2\right]_-^+,

represents the jump conditions across internal boundaries. These conditions ensure that the eigenfunctions :math:`U`, and :math:`P` are continuous across layers with differing material properties. 

Like for the other eigenfunction types:

.. math::

    \left[T_V \tilde{V} r^2\right]_-^+,

represents the jump condition across internal boundaries for the eigenfunction :math:`V`. 
However, the tangential eigenfunction :math:`V` requires special consideration at liquid-liquid and solid-liquid interfaces. 
:math:`V` is not continuous across these boundaries due to the shear modulus being zero in liquid regions. 
Hence, at liquid interfaces, :math:`V` is allowed to be discontinuous, a behavior that is incorporated into the computational framework through the gather-scatter operator. 
The operator decouples the nodes of :math:`V` at the solid-liquid and liquid-liquid boundaries.

One important detail of the spheroidal full gravity equation as shown above is that the mass matrix (the left-hand side) does not include an eigenfunction for the gravity perturbation :math:`P` making the matrix positive semi-definite, which has a strong influence on the type of solver that can be used to solve the general eigenvalue problem hiding behind these equations. 

======
Solver
======

For each of the types mentioned above, we are solving a general eigenvalue problem:

.. math::
   :label: eq.eigenvalueproblem

    \omega^2 M \vec{v} = K \vec{v},

in which, :math:`M` is the mass matrix and :math:`K` is the stiffness matrix corresponding to the left-hand side and right-hand side of the equations :eq:`eq.toroidal`, :eq:`eq.radial`, and :eq:`eq.spheroidal`, respectively. 
Most important is the eigenpair given by :math:`(\omega=2 \pi f, \vec{v})`, which consists of the eigenvalue :math:`\omega` that is the angular frequency, 
and the corresponding eigenvector :math:`\vec{v}`, commonly refered to as eigenfunctions in the normal mode literature. 
The eigenfunctions represent dimensionless displacements in depth for each mode.  
For each angular order :math:`l` we need to solve one of these eigenvalue problems :eq:eq.eigenvalueproblem. 
We use a shift-invert preconditioner and the Krylov-Schur form of the matrices in the Arnoldi algorithm to solve the general elastic eigenvalue problem. 

Elastic problem
===============

Specnm solves the elastic eigenvalue problem by employing `slepc4py <https://pypi.org/project/slepc4py/>`_.
An important fact to note is that the relative error in eigensolution increases with distance from the shift, 
hence we bracket the frequency interval :math:`f=\{f_\text{min}, f_\text{max}\}` into subintervals with new shifts to keep the error to a minimum (the parameter **n_per_shift** is the number of modes within each interval). 
To collect the solutions for each bracket we double the first and last eigenvalue and match the brackets by using the fact that the eigenvectors of these identical modes are parallel :math:`\vec{v}_1 \cdot \vec{v}_2 \approx 1` due to the orthonormality of the eigenvectors. 
The parameter **max_similarity** gives the user access to the cutoff value over which the eigenvectors are considered parallel.  

The elastic problem is solved with the following function:

.. autofunction:: specnm.multishift_solver.multishift_slepc

Anelastic problem
=================

Anelasticity makes the eigenvalue problem weakly nonlinear (in the stiffnes matrix :math:`K(\omega)`), as it accounts for mode dispersion by attenuation:

.. math::

    \omega^2 M \vec{v} = K(\omega) \vec{v}.

Here, we employ two different techniques to solve the problem. 
First-order perturbation and eigenvector continuation :cite:p:`Frame_etal2018` together with logarithmic interpolation based on the elastic solution. 

The *first-order* perturbation (in frequency) relies on the fact that the angular frequency :math:`\omega` affected by attenuation :math:`\omega_\mathrm{at.}` can be approximated by:

.. math::
    \omega_\mathrm{at.} \approx \omega_{el.} + \frac{1}{\pi} Q^{-1} \mathrm{ln}(\omega_{el.} / \omega_0),

where :math:`Q` is the global quality factor of the mode and :math:`\omega_0` is the model reference frequency (usually 1 Hz), and :math:`\omega_\mathrm{el.}` is the *elastic* frequency. 
This approach ignores effects on eigenfunctions and does not allow for branch switching due to attenuation. 
Since all the parameters are readily available within our code this approach is highly efficient. 

The *eigenvector contiuantion* and the *stepwise* variant makes use of the calculated orthonormal system of the elastic spectrum to approximate the anelastic spectrum. 
Due to physical dispersion each mode sees the model logarithmically shifted to the mode frequency, which ends up in the stiffness matrix :math:`K(\omega)`. 
Hence, we use eigenvector contiuation as it is a robust technique for solving (Hermitian) eigenvalue problems with matrices having a functional dependency on parameters (here our stiffness matrix varies with eigenfrequency). 
Our function is adapted to efficiently compute eigenpairs across a predefined frequency range. 
Beginning with an initial solution for the lowest frequency, it iteratively refines solutions by solving for increasingly higher frequencies, ensuring alignment between left and right eigenvectors. 
Through eigenvector matching via dot products together with boundary filtering over overlapping eigenpairs, and log-linear interpolation for frequency estimation, it enhances solution accuracy. 
Further refinement is achieved by solving a reduced eigenvalue problem, culminating in sorted eigenvalues within the specified frequency bounds. 
    

Spurious modes
==============

Our formulation of the spheroidal eigenvalue problem has unconstrained degrees of freedom in fluid layers of the modeled object as the material parameter :math:`\mu` is zero, which directly influences the :math:`V` eigenfunction. 
We encounter two types of spurious modes associated with this: numerical spurious modes that stem from underresolving parts of the infinite spectrum, and undertones that are essentially gravity waves in the fluid layers of the planet. 
There is also the possibility of numerical spurious modes mixing with very similar (in eigenfunction) physical modes (quasi-degenerate coupling) :cite:p:`Buland_etal1984`. 
This can be mitigated by just changing the mesh frequency slightly. 
The spurious modes are identified by the code, and it throws/produces a *RuntimeError* displaying a short description.
For more details on the spurious modes, the reader is refered to our accompanying paper :cite:p:`Kemper_etal2021`.

Bibliography
============

.. bibliography::
   :filter: docname in docnames
