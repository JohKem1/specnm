*************
Visualization
*************

Here you will find some basic visualization tricks using **plotter.py** of specnm:

.. contents:: Table of Contents
   :local:

Simple eigenfunction plot
=========================
A simple example of how to use the structured eigenfunction plot function.

.. note::
   All the different specnm base classes (spheroidal, radial and toroidal)
   can be displaced by the same function **eigenfunc_struct**.

.. autofunction:: specnm.plotter.eigenfunc_struct

**Code example**

.. literalinclude:: ../specnm/examples/simple_plot.py
  :language: python
  :linenos:

.. plot:: ../specnm/examples/simple_plot.py

There are also other options to plot eigenfunctions based on unstructured output, that are mainly used for debugging. 

.. autofunction:: specnm.plotter.eigenfunc_unstructured

.. autofunction:: specnm.plotter.eigenfunc_simple

Interactive spectrum plot
=========================
This is a good educational tool for showing the eigenfunctions in depth 
corresponding to the eigenfrequencies as shown in the spectrum. This is especially 
*cool* for models of objects that have *unknown* or new spectra, such as the Galilean moon Europa 
that is shown in the following example.

Using this matplotlib window one can interactively select the modes by double clicking.

.. autofunction:: specnm.plotter.spectrum

**Code example**

.. literalinclude:: ../specnm/examples/interactive_spectrum.py
  :language: python
  :linenos:

.. plot:: ../specnm/examples/interactive_spectrum.py
