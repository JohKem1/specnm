***********
Basic usage
***********

Here you will find some basic use cases for specnm.

.. contents:: Table of Contents
   :local:

Basics
======

In general, normal modes exist in two different cases: spheroidal, related to **rayleigh** surface waves, and toroidal modes, related to **love** surface waves. 
In the spherical symmetric case the modes follow the following naming convention: :math:`_{n}\mathrm{S}_{l}` and :math:`_{n}\mathrm{T}_{l}`, 
in which :math:`n` is the overtone number and :math:`l` is the angular order. 
Radial modes are spheroidal modes with angular order :math:`l` equal to zero.
There are different options for which **specnm** class to use with different approximations related to gravity (for spheroidal calculations): 

#. **rayleigh_fg** :ref:`Spheroidal modes <spheroidal>` with *full* description of gravity 
   (gravest modes <30 mHz for Earth and *heavy* planets)
#. **rayleigh** :ref:`Spheroidal modes <spheroidal>` with *Cowling* approximation of gravity or no gravity at all
#. **radial** :ref:`Radial modes <radial>` with *full* gravity, but is highly efficient
#. **love** :ref:`Toroidal modes <toroidal>` are unaffected by gravity as the displacement is purely horizontal

.. _spheroidal:

Spheroidal modes
================

.. autoclass:: specnm.spheroidal_base.sph_base
   :members: rayleigh_problem

**Code example: Full gravity**

.. code-block:: python
    :linenos:

    from specnm import rayleigh_fg

    # setup spheroidal base class for anisotropic prem model
    ray = rayleigh_fg(model='prem_ani', fmax=0.01)
    # run the eigenvalue problem solver to get solutions
    ray_out = ray.rayleigh_problem(attenuation_mode='full', fmax=0.005)

**Code example: Cowling approximation**

.. code-block:: python
    :linenos:

    from specnm import rayleigh

    # setup spheroidal base class for anisotropic prem model
    ray = rayleigh(model='prem_ani', fmax=0.01)
    # run the eigenvalue problem solver to get solutions
    ray_out = ray.rayleigh_problem(attenuation_mode='full', fmax=0.005)

.. _radial:

Radial modes
============

.. autoclass:: specnm.radial
   :members: radial_problem

**Code example**

.. code-block:: python
    :linenos:

    from specnm import radial

    # setup radial base class for anisotropic prem model
    rad = radial(model='prem_ani', fmax=0.01)
    # run the eigenvalue problem solver to get solutions
    rad_out = rad.radial_problem(attenuation_mode='full', fmax=0.005)

.. _toroidal:

Toroidal modes
==============

.. autoclass:: specnm.love
   :members: love_problem

**Code example**

.. code-block:: python
    :linenos:

    from specnm import love

    # setup toroidal base class for anisotropic prem model
    lov = love(model='prem_ani', fmax=0.01)
    # run the eigenvalue problem solver to get solutions
    lov_out = lov.love_problem(attenuation_mode='full', fmax=0.005)

Within each **specnm** class **rayleigh_fg**, **rayleigh**, **radial** and **love** one can select 4 different types of *attenuation*: *no* | *elastic*, *first order*, *eigenvector continuation* or *full* | *eigenvector continuation stepwise*. By default the code selects 'elastic' if the model has no attenuation information (no quality factors in depth) or 'full' if the model provides such information. 
If high accuracy is not so important one can also select 'first order' which increases efficiency.
It employs a first order perturbation in the eigenfrequencies, but does not allow for branch switching of the overtones. 
In addition, this type of attenuation has no effect on the eigenfunctions. 
The option *eigenvector continuation* should only be used for calculations with small number of overtone as the error increases with interval distance in frequency between the first and last overtone, this has been overcome by bracketing in the *full* mode, which uses this method in a stepwise fashion. For an explanation of this please see the :doc:`method_and_solver` chapter. 

Input files
===========

Possible input file formats are: `AxiSEM <https://github.com/geodynamics/axisem>`_ *polynomial* or *layered* file format, `Mineos <https://github.com/geodynamics/mineos/tree/master>`_ *deck* file format.
Some examples are in: **specnm/models/**.

.. note::
   Possible extension to include `TauP <https://docs.obspy.org/packages/obspy.taup.html#building-custom-models>`_ *nd* file format.

Calculate PREM modes
====================
Calculate PREM spheroidal and toroidal modes and plot some of them.

Be aware that for spheroidal modes and angular order *1* the overtone number starts at *2*.
:math:`_{0}\mathrm{S}_{1}` is the *trivial* translation mode, while :math:`_{1}\mathrm{S}_{1}` is the Slichter mode (citation). 
Hence if you look for :math:`_{2}\mathrm{S}_{1}` it is the first mode in the 
spectrum for angular order :math:`l=0` (index=0) and this needs to be corrected for 
association to observed data.

The same applies to toroidal modes as :math:`_{0}\mathrm{T}_{1}` is the *trivial* rotational mode 
meaning that the spectrum for angular order :math:`l=1` starts at overtone number *1*: :math:`_{1}\mathrm{T}_{1}`.

.. note::
   Possible extension to include a *mode counter*.

**Code example**

.. literalinclude:: ../specnm/examples/simple_output.py
  :language: python
  :linenos:

**Output**

.. code-block:: console 

    $ python simple_output.py
    Printing the eigenfrequencies for toroidal run:
    mode: frequency in Mhz
    1T1: 1.23528
    0T2: 0.37862
    1T2: 1.31917
    0T3: 0.58514
    1T3: 1.43799
    0T4: 0.76406
    1T4: 1.58418
    0T5: 0.92592
    1T5: 1.74896

    Printing the eigenfrequencies for spheroidal run:
    mode: frequency in Mhz
    2S1: 0.38148
    3S1: 0.95613
    4S1: 1.42378
    5S1: 1.68960
    6S1: 1.98770
    0S2: 0.24212
    1S2: 0.68493
    2S2: 0.86372
    3S2: 1.10303
    4S2: 1.73643
    0S3: 0.39877
    1S3: 0.94717
    2S3: 1.23942
    3S3: 1.35785
    0S4: 0.58993
    1S4: 1.17614
    2S4: 1.37347
    3S4: 1.78491
    0S5: 0.79429
    1S5: 1.36507
    2S5: 1.51231
    0S6: 1.00008
    1S6: 1.50904
    2S6: 1.68225
    0S7: 1.19941
    1S7: 1.63975
    2S7: 1.86654
    0S8: 1.38532
    1S8: 1.78279

Phase or group velocity diagram
===============================
Calculate PREM spheroidal phase velocities and plot the dispersion diagram.
The keyword **dispersion** should only be used (set to **True**), when used in conjunction with **rayleigh** or **love** class. 
Since for dispersion we are only interested in rather *high frequency* modes it is reasonable use Cowling approximation and to cutoff the spheroidal eigenfunctions at the core-mantle boundary.
This makes the calculation much more efficient as no filtering for spurious fluid modes is needed. 
It is an approximation in that it assumes that the modes are not affected by gravity much and do not touch the core-mantle boundary with their displacement. 
If full accuracy is needed just remove the dispersion keyword.

The same code can be used for producing a group velocities diagram using 'group velocities' instead of 'phase velocities' as key.

**Code example**

.. literalinclude:: ../specnm/examples/simple_phasevelocity.py
  :language: python
  :linenos:

.. plot:: ../specnm/examples/simple_phasevelocity.py
   :context: reset
