#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A class to handle toroidal eigenvalues, eigenfunctions and dispersion curves.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2016
    Federico D. Munch (federico.munch@erdw.ethz.ch), 2016
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2019-2024
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np  # numerical python
from scipy.sparse import lil_matrix, block_diag, identity  # sparse matrices
from .specnm_base import spc_base  # god class
from .sensitivity import sensitivity  # sensitivity kernels
from itertools import cycle


class love(spc_base, sensitivity):
    def __init__(self, *args, **kwargs):
        # pass all arguments and keyword-argument tuples to base class
        spc_base.__init__(self, *args, **kwargs,
                          gravity=False, mode='toroidal')

    def love_problem(self, l0=1, lmax=None, fmin=0.5/3600., fmax=None,
                     attenuation_mode=None, logskip=False, dlog10l=0.1,
                     llist=None, sensitivity_kernels='none',
                     **multishift_kwargs):
        """
        Produces the toroidal spectrum of eigenvalues and eigenfunctions
        until the max frequency or max angular order is reached

        :type l0: int
        :param l0: minimum angular order (should be l0>0)
        :type lmax: int
        :param lmax: maximum angular order (output only till fmax of mesh)
        :type fmin: float
        :param fmin: minimum output frequency (cutoff for undertones)
                     defaults to 1/120 min based on minimum earth frequency
        :type fmax: float
        :param fmax: maximum output frequency (freq<=fmax)
        :type attenuation_mode: str
        :param attenuation_mode: attenuation mode for calculation
                                 {'no' or 'elastic',
                                 'first order',
                                 'eigenvector continuation',
                                 'full' or
                                 'eigenvector continuation stepwise'}
        :type logskip: bool
        :param logskip: logarithmic steps in angular order
        :type dlog10l: float
        :param dlog10l: declogarithmic stepsize according to
                        l = int(10^(log10(l-1) + dlog10l))
        :type llist: list (int) or ndarray (int)
        :param llist: list of angular orders to use in calculation
                      (default=None)
        :type sensitivity_kernels: str
        :param sensitivity_kernels: Produce sensitivity kernels (default none)
                                    {'none', 'isotropic', 'anisotropic'}
        :type multishift_kwargs: dict
        :param multishift_kwargs: additional arguments for the multishift
                                  solver
        :returns: Structured output with keys: angular orders, wave numbers,
                  frequencies, angular frequencies,
                  eigenfunctions, eigenfunctions_dr,
                  phase velocities, group velocities, gamma, Q, epsilons,
                  radius, polynomial order
        :rtype: dict
        """
        if llist is not None:
            llist = np.copy(sorted(llist)).tolist()
            lmax = llist[-1]
            llist.append(lmax + 1)
            lcycle = cycle(llist)
            l0 = next(lcycle)

        if (l0 <= 0):
            raise ValueError('l0 has to be positive.')
        if (lmax is not None and l0 > lmax):
            raise ValueError('l0 has to be smaller than lmax.')

        attenuation = (self.model_attenuation and
                       attenuation_mode not in ['elastic', 'first order'])

        if (self.verbose):
            print('Initialization of solver and start of calculation.')

        # initialize eigenvalue problem
        self._eigenvalue_problem_init(attenuation_mode)

        if self.dispersion:
            _ = multishift_kwargs.setdefault('n_total', 10)

        if (fmax is None):
            fmax = self.fmesh
        elif (fmax > self.fmesh):
            raise ValueError(
                'The maximum input frequency was \
                 higher than maximum frequency of the mesh.')

        if attenuation_mode == 'first order':
            f0 = fmax
            self._update_physical_properties(2 * np.pi * f0)

        # initialize variables
        fs = []
        Ws = []
        ls = []

        # compute eigenfrequency and eigenfunctions
        if logskip:
            # since the dispersion curves are interpolated we do not need
            # each l
            lfunc = lambda l: int(10**(np.log10(l) + dlog10l))  # NOQA
        else:
            # in general need all angular orders
            lfunc = lambda l: l + 1  # NOQA

        cur_l = l0

        while lmax is None or cur_l <= lmax:
            if (self.verbose):
                print(f'Current angular order:{cur_l:5d}')

            # solve the eigenvalue problem in base class
            ef, ev = self._eigenvalue_problem(cur_l, fmin, fmax,
                                              **multishift_kwargs)

            # maximum l reached? If not provided by the user, stop if at this
            # frequency the interval contains no modes
            if len(ef) == 0:
                if cur_l > 1 and lmax is None:
                    break
            else:
                fs += [_ef for _ef in ef]

                if (self.verbose):
                    print(f'number of overtones  : {len(ef)}')

                # get eigenfunctions according to Dahlen&Tromp
                ev *= self._sign(ev[:, -1])[:, np.newaxis]
                Ws += [self.scatter.dot(_ev) for _ev in ev]

                # update current minimum frequency
                ls += [cur_l] * len(ef)

            if llist is None:
                cur_l = lfunc(cur_l)
            else:
                cur_l = next(lcycle)

        if len(fs) == 0:
            raise ValueError('No mode found!')

        fs = np.array(fs)
        ls = np.array(ls)
        Ws = np.array(Ws)
        ws = 2 * np.pi * fs
        ks = (ls * (ls + 1)) ** 0.5

        # compute phase&group velocity for Love waves
        cp = ws / ks
        cg = self._group_vel(Ws, cp, ws, attenuation=attenuation)

        # normalize eigenfunctions
        Ws /= self._normalize(Ws)[:, np.newaxis]

        if self.model_attenuation:
            gammas = self._calculate_gammas(ks, ws, Ws)
            Qs = ws / gammas / 2
            if attenuation:
                # correct group velocity for physical dispersion
                # seem to go the right direction, but still higher error than
                # for elastic
                cg *= 1 + 2 * gammas / (np.pi * ws)
        else:
            gammas = np.zeros_like(fs)
            Qs = np.zeros_like(fs) + np.inf

        if attenuation_mode == 'first order':
            # D&T 9.55
            fs += fs / np.pi / Qs * np.log(fs / f0)

        epsilons = self._epsilon(Ws, ks, ws, attenuation=attenuation)

        # derivative of eigenfunction
        jop = self.jacobian_vec[np.newaxis, :]
        derop = self.l_prime_mat
        Wdot = derop.dot(Ws.T).T / jop

        # reset physical properties if next run is elastic
        if attenuation_mode != 'elastic':
            self._reset_physical_properties()

        structured_output = {
            'angular orders': ls,
            'wave numbers': ks,
            'frequencies': fs,
            'angular frequencies': ws,
            'eigenfunctions': Ws,
            'eigenfunctions_dr': Wdot,
            'phase velocities': cp * self.radius,
            'group velocities': cg * self.radius,
            'gamma': gammas,
            'Q': Qs,
            'epsilons': epsilons,
            'radius': self.r,
            'polynomial order': self.n,
        }

        # related to sensitivity kernel output
        sensitivities = {'isotropic': self.isotropic_kernels,
                         'anisotropic': self.anisotropic_kernels}

        if sensitivity_kernels in sensitivities:
            structured_output['sensitivity kernels'] =\
                sensitivities[sensitivity_kernels](structured_output)

        if (self.verbose):
            print('Done.')

        return structured_output

    def _update_physical_properties(self, wfreq):
        """
        Computes perturbation of L & N according to
        eq. (63)&(64) from documentation
        :type wfreq: float
        :param wfreq: angular frequency for update
        """
        lnfrq = np.log(wfreq / self.wref)

        self.L = self.L0 + self.factorT * lnfrq
        self.N = self.N0 + self.factorT * lnfrq

        # build updated medium matrices
        self._build_medium_matrices()

    def _precompute_stiffness_mass_matrices(self):
        """
        Precomputes the stiffness and mass matrices
        also gets norms, weights and l' etc.
        """
        # Build gather and scatter operators
        self.gather, self.scatter = \
            self._gather_scatter_operator(self.fluid_elements)

        # Precompute patterns for medium matrices
        mini_blocks = [np.ones((self.n + 1, self.n + 1))] * self.Ne
        self.Lpattern = block_diag(mini_blocks, format='csr')
        self.Idiag = identity(self.Ne * (self.n + 1), format='csr')

        # GLL points (estimated by build_structure_and_load_model)
        self.r_ass = self.gather.dot(self.r.flatten())

        # Mass matrix (Eq. 14 - Documentation )
        self.M = self.__global_mass()

        # L matrix (Eq. 15 - Documentation) - indepedent of material properties
        self.stiffnessL = self.__global_stiffnessL()

        # K matrix (Eq. 16 - Documentation)
        self.stiffnessK = self.__global_stiffnessK()

        # build medium matrices for linear solver with reference frequency
        self._build_medium_matrices()

        # save medium matrices without anelasticity for later reset
        self.Lmatrix_block0 = self.Lmatrix_block.copy()

    def _precompute_mass_matrix(self):
        self.M = self.__global_mass()

    def _build_medium_matrices(self):
        """
        From updated anelastic properties
        we create matrices to incorporate the medium properties
        into the stiffness matrix.
        """
        # create matrices to incorporate
        # the medium properties into the stiffness matrix
        self.Lmatrix_block = []

        for k in np.arange(self.n + 1):
            Lnode = np.repeat(self.L[k::self.n + 1], (self.n + 1))
            self.Idiag.data = Lnode
            Lmatrixnode = self.Lpattern.dot(self.Idiag)
            self.Lmatrix_block.append(Lmatrixnode)

    def _reset_physical_properties(self):
        """
        Resets material matrices to reference frequency ones.
        """
        self.L = self.L0.copy()
        self.N = self.N0.copy()
        self._build_medium_matrices()

    def global_matrices(self, l):
        """
        This function combines pre-computed stiffness matrices and assembles
        the stiffness and mass matrices for a given angular order l

        :type l: int
        :param l: angular order
        :returns: assembled sparse Mass matrix,
                  assembled sparse stiffness matrix
        :rtype: Tuple(scipy.sparse.lil_matrix, scipy.sparse.lil_matrix)
        """
        kn = (l * (l + 1)) ** 0.5

        # build stiffness matrix
        self.Idiag.data = self.N
        K = self.stiffnessL.dot(self.Idiag) * (kn ** 2 - 2)
        for k in np.arange(self.n + 1):
            K += self.stiffnessK[k].multiply(self.Lmatrix_block[k])

        # assemble stiffness matrix
        K_ass = self.gather.dot(K).dot(self.scatter)

        # build mass matrix
        M_ass = self.gather.dot(self.M).dot(self.scatter)

        return M_ass, K_ass

    def _group_vel(self, W, c, omega, attenuation=True):
        """
        Compute group velocity according to D&T1998 (11.56, 11.83)
        :type W: float array
        :param W: eigenfunction W within domain
        :type c: float
        :param c: phase velocity
        """
        # setup operators & localize variables
        jopwop = self.jacobian_vec * self.weights_vec
        rho = self.rho
        r = self.r

        if attenuation:
            lnfrq = np.log(omega / self.wref)
            N = self.N0[None, :] + self.factorT[None, :] * lnfrq[:, None]
        else:
            N = self.N

        I1 = (rho * W ** 2 * r ** 2).dot(jopwop)
        I2 = (N * W ** 2).dot(jopwop)

        return I2 / (c * I1)

    def _epsilon(self, W, k, omega, attenuation=True):
        """
        compute energy partitioning error as suggested by Takeuchi & Saito 1972

        with an additional factor 2, this gives the relative error in omega,
        see also Zabranova et all 2017

        Compute everything at polynomial order n+1, as otherwise the error
        estimate is zero, because the discrete solutions fulfill the discrete
        rayleigh quotient.
        """
        jop = self.jacobian_vec_np1[np.newaxis, :]
        jopwop = self.jacobian_vec_np1 * self.weights_vec_np1
        derop = self.l_prime_mat_np1

        # TODO: evaluate moduli at mode frequency?
        rho = self.P_n_to_np1.dot(self.rho)
        r = self.P_n_to_np1.dot(self.r)

        if attenuation:
            factorT = self.P_n_to_np1.dot(self.factorT)
            lnfrq = np.log(omega / self.wref)

            L = self.P_n_to_np1.dot(self.L0)
            N = self.P_n_to_np1.dot(self.N0)

            L = L[None, :] + factorT[None, :] * lnfrq[:, None]
            N = N[None, :] + factorT[None, :] * lnfrq[:, None]
        else:
            N = self.P_n_to_np1.dot(self.N)
            L = self.P_n_to_np1.dot(self.L)

        W = self.P_n_to_np1.dot(W.T).T

        Wdot = derop.dot(W.T).T / jop

        # D&T 8.111
        T = (rho * W ** 2 * r ** 2).dot(jopwop)

        # D&T 8.197
        Ve = (L * (Wdot * r - W) ** 2 +
              (k[:, None] ** 2 - 2) * N * W ** 2).dot(jopwop)

        epsilon = np.abs(Ve / (omega ** 2 * T) / 2 - 0.5)

        return epsilon

    def _calculate_gammas(self, k, omega, W):
        """
        Calculates the integral for the decay rate depending on
        the material property mu (shear)
        :type k: float array
        :param k: wavenumber
        :type omega: float array
        :param omega: angular frequency
        :type W: float array
        :param W: toroidal eigenfunction
        """
        r = self.r[np.newaxis, :]
        jop = self.jacobian_vec[np.newaxis, :]
        wop = self.weights_vec[np.newaxis, :]
        derop = self.l_prime_mat

        muw = self.mu0[np.newaxis, :] + \
            2. / np.pi * self.mu0[np.newaxis, :] * \
            self.QMUinv[np.newaxis, :] * \
            np.log(omega / self.wref)[:, np.newaxis]

        Wdot = derop.dot(W.T).T / jop

        K_mu = (((r * Wdot - W) ** 2 + (k[:, np.newaxis] ** 2 - 2.) * W ** 2) /
                (2. * omega[:, np.newaxis]))
        gamma_mu = (
            K_mu * wop * jop * muw * self.QMUinv[np.newaxis, :]).sum(axis=1)

        return gamma_mu

    #
    # Mass & Stiffness Matrix declarations
    #
    def __global_mass(self):
        """
        build the unassembled global mass matrix
        according to documentation
        """
        n = self.n
        Ne = self.Ne
        rho = self.rho
        r = self.r
        jac_ind = self.jacobian_indices

        # setting up matrix
        nldof = Ne * (n + 1)
        M = lil_matrix((nldof, nldof))

        # loop over elements
        for j in jac_ind:
            M[j[1]:j[2], j[1]:j[2]] = \
                self.__elemental_mass(rho[j[1]:j[2]], r[j[1]:j[2]], j[0], n)

        # return in compressed sparse row format
        return M.tocsr()

    def __elemental_mass(self, rho, r, J, n):
        """
        compute elemental mass matrix for a single element
        see documentation
        :type rho: float array
        :param rho: density for each point within element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        """
        weights = self.weights
        Ml = lil_matrix((n + 1, n + 1))

        for k in np.arange(n + 1):
            Ml[k, k] += weights[k] * rho[k] * r[k] ** 2 * J[k]

        # return in compressed sparse row format
        return Ml.tocsr()

    def __global_stiffnessK(self):
        """
        build up unassembled K matrix (without medium information).
        """
        jac_ind = self.jacobian_indices
        n = self.n
        Ne = self.Ne
        r = self.r

        # matrix dimensions
        nldof = Ne * (n + 1)

        Klist = []
        for k in np.arange(n + 1):
            Ktmp = lil_matrix((nldof, nldof))
            for j in jac_ind:
                Ktmp[j[1]:j[2], j[1]:j[2]] = \
                    self.__elemental_stiffnessK(r[j[1]:j[2]], j[0], k, n)

            # append matrix to list
            Klist.append(Ktmp.tocsr())

        return Klist

    def __elemental_stiffnessK(self, r, J, k, n):
        """
        compute elemental K matrix for a single element
        (without medium information) see documentation
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: int
        :param k: outter loop index
        """
        weights = self.weights
        l_prime = self.l_prime
        Kl = np.zeros((n + 1, n + 1))

        for i, j in np.ndindex(Kl.shape):
            Kl[j, i] += (r[k] ** 2 * weights[k] * l_prime[k, i] *
                         l_prime[k, j] / J[k])

            if (k == i):
                Kl[j, i] -= weights[i] * l_prime[i, j] * r[i]

            if (k == j):
                Kl[j, i] -= weights[j] * l_prime[j, i] * r[j]

        Kl[k, k] += weights[k] * J[k]

        return Kl

    def __global_stiffnessL(self):
        """
        build up unassembled L matrix.
        """
        Ne = self.Ne
        n = self.n
        jac_ind = self.jacobian_indices
        r = self.r

        # setting up matrix
        nldof = Ne * (n + 1)
        K = lil_matrix((nldof, nldof))

        # loop over blocks on the diagonal
        for j in jac_ind:
            K[j[1]:j[2], j[1]:j[2]] = self.__elemental_stiffnessL(
                r[j[1]:j[2]], j[0], n)

        # return in compressed sparse row format
        return K.tocsr()

    def __elemental_stiffnessL(self, r, J, n):
        """
        compute elemental L matrix for a single element
        (without medium information) see documentation
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        """
        weights = self.weights

        Kl = lil_matrix((n + 1, n + 1))

        for k in np.arange(n + 1):
            Kl[k, k] += weights[k] * J[k]

        # return in compressed sparse row format
        return Kl.tocsr()
