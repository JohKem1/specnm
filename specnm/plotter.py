#!/usr/bin/env python
"""
Plotting functions for specnm.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2020-2024
:license:
    | GNU Lesser General Public License, Version 3
    | (http://www.gnu.org/copyleft/lgpl.html)
"""
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from .spheroidal_base import _UVP_to_U_V_P as UVP
import numpy as np


def eigenfunc_struct(cls, cls_out, el, en, plottitle=True,
                     fig=None, ax=None):
    """
    Plot specific eigenfunctions of either love or rayleigh type

    :type cls: specnm class
    :param cls: either love, rayleigh or radial object
    :type cls_out: dict
    :param cls_out: output array of specnm calculation function
    :type el: int
    :param el: angular order of mode
    :type en: int
    :param en: mode number of mode (fund + overtone)
    :type plottitle: bool
    :param plottitle: whether to display frequency of the mode as title of plot
                      (default=True)
    :type fig: matplotlib figure object
    :param fig: matplotlib figure to draw axes on (default=None)
    :type ax: matplotlib axis object
    :param ax: axis object of matplotlib figure object to draw on
               (default=None)
    """
    if (fig and ax):
        pass
    elif (not fig and not ax):
        fig, ax = plt.subplots()
    elif (not ax):
        ax = fig.add_subplot(1, 1, 1)
    elif (not fig):
        fig = ax.figure

    if (el not in cls_out['angular orders']):
        raise ValueError('No such angular order in given structured array.' +
                         '\nList of possible angular orders: ' +
                         str(np.unique(cls_out['angular orders'])))

    # get index
    idx = np.where(cls_out['angular orders'] == el)[0][en]

    # plotting
    plt.xlabel(r'radius in km')
    plt.ylabel(r'eigenfunction in a.u.')
    if (plottitle):
        frq = cls_out['frequencies'][idx] * 1000.
        plt.title(f'frequency: {frq:.5f} in mHz')

    lstyles = ['k-']
    if (cls.mode == 'spheroidal'):
        U, V, P = UVP(cls_out['eigenfunctions'], cls.sph_type)
        labels = ['U', 'V', 'P']
        lstyles += ['k--', 'k-.']
        efs = [U[idx], V[idx], P[idx]]
    elif (cls.mode == 'radial'):
        efs = [cls_out['eigenfunctions'][idx]]
        labels = ['U']
    elif (cls.mode == 'toroidal'):
        efs = [cls_out['eigenfunctions'][idx]]
        labels = ['W']
    else:
        raise NotImplementedError(
            'Only classes spheroidal, radial and toroidal implemented.')

    for e, f in enumerate(efs):
        if np.all(f) == 0.0:
            continue
        ax.plot(cls.r / 1000., f, lstyles[e], label=labels[e])
    for fb in cls.fluid_boundaries:
        ax.axvline(x=fb / 1000., ls='dashed')
    ax.set_xlim(cls.r[0] / 1000., cls.r[-1] / 1000.)
    ax.legend()

    if ((ax is not None) or (fig is not None)):
        plt.tight_layout()
        plt.show()

    return


def eigenfunc_unstructured(r, ef, fluid_boundaries=None,
                           fig=None, ax=None):
    """
    Plot specific eigenfunctions of either love or rayleigh type
    from an unstructured eigenfunction vector

    :type r: ndarray (float)
    :param r: radial grid
    :type ef: ndarray (float)
    :param ef: unstructured eigenfunction vector directly from solver
    :type fluid_boundaries: ndarray (float)
    :param fluid_boundaries: fluid boundaries (None = No plotting)
    :type fig: matplotlib figure object
    :param fig: matplotlib figure to draw axes on
    :type ax: matplotlib axis object
    :param ax: axis object of matplotlib figure object to draw on
    """
    if (fig and ax):
        pass
    elif (not fig and not ax):
        fig, ax = plt.subplots()
    elif (not ax):
        ax = fig.add_subplot(1, 1, 1)
    elif (not fig):
        fig = ax.figure

    ty = int(len(ef) / len(r))

    if (ty >= 2):
        labels = ['U', 'V', 'P']
        lstyles = ['k-', 'k--', 'k-.']
    else:
        labels = ['W or U']
        lstyles = ['k-']

    efs = [ef[i::ty] * np.sign(ef[-1]) for i in range(ty)]

    plt.xlabel(r'radius in km')
    plt.ylabel(r'eigenfunction in a.u.')
    if (fluid_boundaries is not None):
        for fb in fluid_boundaries:
            plt.axvline(fb / 1000., ls='dashed')
    for e, f in enumerate(efs):
        plt.plot(r / 1000., f, lstyles[e], label=labels[e])
    plt.legend()
    plt.tight_layout()
    plt.show()

    return


def eigenfunc_simple(r, efs, fluid_boundaries=None, freq=None,
                     fig=None, ax=None):
    """
    Plot specific eigenfunctions of either love or rayleigh type
    from an unstructured eigenfunction vector

    :type r: ndarray (float)
    :param r: radial grid
    :type efs: ndarray (float)
    :param efs: eigenfunction vectors [U, V(, P)] or [U or W]
    :type fluid_boundaries: ndarray (float)
    :param fluid_boundaries: fluid boundaries (None = No plotting)
    :type freq: float
    :param freq: frequency in Hz for plot title (None = no title)
    :type fig: matplotlib figure object
    :param fig: matplotlib figure to draw axes on
    :type ax: matplotlib axis object
    :param ax: axis object of matplotlib figure object to draw on
    """
    if (fig and ax):
        pass
    elif (not fig and not ax):
        fig, ax = plt.subplots()
    elif (not ax):
        ax = fig.add_subplot(1, 1, 1)
    elif (not fig):
        fig = ax.figure

    if (len(efs) >= 2):
        labels = ['U', 'V', 'P']
        lstyles = ['k-', 'k--', 'k-.']
    else:
        labels = ['W or U']
        lstyles = ['k-']

    plt.figure(figsize=(8, 6))
    plt.xlabel(r'radius in km')
    plt.ylabel(r'eigenfunction in a.u.')
    if (fluid_boundaries is not None):
        for fb in fluid_boundaries:
            plt.axvline(fb / 1000., ls='dashed')
    if (freq is not None):
        plt.title(f'frequency in mHz: {freq * 1000.}')
    for e, f in enumerate(efs):
        plt.plot(r / 1000., f, lstyles[e], label=labels[e])
    plt.legend()
    plt.tight_layout()
    plt.show()

    return


def spectrum(cls, cls_out,
             plot_title=True, plot_efs=True, exclude_slichter=False,
             plot_wavenumber=False, plot_phasevelocity=False,
             init_mode=None):
    """
    Plot specific interactive spectrum of either love or rayleigh type
    Provides an educational way of viewing the spectrum with clickable
    modes to display eigenfunctions and respective eigenfrequency

    :type cls: initialized specnm class
    :param cls: either love, rayleigh or radial object
    :type cls_out: dict
    :param cls_out: output array of specnm calculation function
    :type plot_title: bool
    :param plot_title: whether to display a plot title
    :type plot_efs: bool
    :param plot_efs: whether to display the eigenfunctions next to plot and
                     make the plot clickable
    :type exclude_slichter: bool
    :param exclude_slichter: whether to exclude the Slichter number from
                             overtones starts S at overtone number 2
                             instead of 1
    :type plot_wavenumber: bool
    :param plot_wavenumber: whether to have the x axis as wavenumber
    :type plot_phasevelocity: bool
    :param plot_phasevelocity: whether to have the y axis as phase velocity
    :type init_mode: tuple
    :param init_mode: initial mode number as (n, l) (default=None)
    """
    oplotcolors = ['#e31a1c',
                   '#33a02c',
                   '#ff7f00',
                   '#1f78b4']

    addt = 1 + int(exclude_slichter) * int(cls.sph_type > 1)

    ls = cls_out['angular orders']
    if (plot_wavenumber):
        xlabel = r'wave number $k$ in km'
        xs = cls_out['wave numbers'] * cls_out['radius'][-1] / 1000.0
    else:
        xlabel = r'angular degree $l$'
        xs = cls_out['angular orders']

    if (plot_phasevelocity):
        ylabel = r'phase velocity in km/s'
        fcp = cls_out['phase velocities'] / 1000.

    else:
        ylabel = r'frequency in mHz'
        fcp = cls_out['frequencies'] * 1000.

    if (plot_efs):
        if (cls.mode == 'spheroidal'):
            if (cls.sph_type == 3):
                plabels = ['U', 'V', 'P']
                efs = [cls_out['eigenfunctions'][:, 0::3],
                       cls_out['eigenfunctions'][:, 1::3],
                       cls_out['eigenfunctions'][:, 2::3]]
            else:
                plabels = ['U', 'V']
                efs = [cls_out['eigenfunctions'][:, 0::2],
                       cls_out['eigenfunctions'][:, 1::2]]
        elif (cls.mode == 'radial'):
            plabels = ['U']
            efs = [cls_out['eigenfunctions']]
        elif (cls.mode == 'toroidal'):
            plabels = ['W']
            efs = [cls_out['eigenfunctions']]
        else:
            raise ValueError(f'Not implemented for type {cls.mode}')

        fig = plt.figure(figsize=(12, 8))
        gspec = gridspec.GridSpec(ncols=2,
                                  nrows=1,
                                  figure=fig,
                                  width_ratios=[4, 1])
        gs0 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gspec[0])
        gs1 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gspec[1])

        if init_mode is None:
            mode = (0, 0) if cls.sph_type == 1 else (0, 1)
        else:
            mode = init_mode
        dataid = np.where(cls_out['angular orders'] == mode[1])[0][mode[0]]

        # spectrum plot
        ax1 = fig.add_subplot(gs0[0, 0])
        ax1.set_xlabel(xlabel)
        ax1.set_ylabel(ylabel)
        if (plot_title):
            ax1.set_title('Type: ' +
                          cls.mode +
                          ', Modelname: ' +
                          cls.modelname)
        ax1.scatter(xs,
                    fcp,
                    marker='.',
                    s=4,
                    c='black')
        ax1.plot(xs[dataid],
                 fcp[dataid],
                 marker='o',
                 mfc='none',
                 mec='red')
        ax1.set_xlim(xmin=0.0)
        ax1.set_ylim(ymin=0.0)

        # eigenfunction plot
        ax2 = fig.add_subplot(gs1[0, 0])
        ax2.set_xlabel(r'eigenfunction in a.u.')
        ax2.set_ylabel(r'radius in km')
        ax2.set_ylim(ymin=cls.r[0] / 1000.,
                     ymax=cls.r[-1] / 1000.)
        ax2.set_title('f in mHz: ' +
                      '{:.3f}'.format(fcp[dataid]) +
                      '\nl/k=' + '{:.3f}'.format(xs[dataid]) +
                      ', n=' + str(mode[0]
                                   if mode[1] != 1 else
                                   mode[0] + addt))
        for e, f in enumerate(efs):
            ax2.plot(f[dataid],
                     cls.r / 1000.,
                     color=oplotcolors[e],
                     label=plabels[e])
        for fb in cls.fluid_boundaries:
            ax2.axhline(fb / 1000., ls='dashed', c='tab:brown', alpha=0.5)
        for fb in cls.fluid_fluid_boundaries:
            ax2.axhline(fb / 1000., ls='dashed', c='tab:blue', alpha=0.5)
        ax2.axvline(0., ls='dashed', c='gray', alpha=0.5)
        ax2.legend()

        # onclick
        def onclick(event):
            if (event.dblclick):
                ccoord = np.array([event.xdata, event.ydata])
                dists = [np.linalg.norm(ccoord - np.array([ll, fq]))
                         for ll, fq in zip(xs, fcp)]
                dataid = np.argmin(dists)

                # update data for eigenfunction plot
                lnelements = ax2.lines
                for i in np.arange(len(efs)):
                    lnelements[i].set_xdata(efs[i][dataid])

                # rescale x axis
                ax2.relim()
                ax2.autoscale_view()

                lsd = ls[dataid]
                overtone_number = np.where(np.where(ls == lsd)[0]
                                           == dataid)[0][0]
                overtone_number += 0 if lsd != 1 else addt

                # reset title
                ax2.set_title('f in mHz: ' +
                              '{:.3f}'.format(fcp[dataid]) +
                              '\nl/k=' + '{:.3f}'.format(xs[dataid]) +
                              ', n=' + str(overtone_number))

                # reset red point
                lnelements2 = ax1.lines
                lnelements2[-1].set_xdata(xs[dataid])
                lnelements2[-1].set_ydata(fcp[dataid])

                # redraw
                fig.canvas.draw()
                fig.canvas.flush_events()

        fig.canvas.mpl_connect('button_press_event', onclick)

        plt.tight_layout()
        plt.show()
    else:
        plt.figure(figsize=(8, 6))
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        if (plot_title):
            plt.title('Type: ' +
                      cls.mode +
                      ', Modelname:' +
                      cls.modelname)
        plt.scatter(ls,
                    fcp,
                    marker='.',
                    s=4,
                    c='black')
        plt.tight_layout()
        plt.show()

    return
