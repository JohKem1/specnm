#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Tests for dispersion.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2023
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np
import pytest

from .. import love
from .. import rayleigh

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


@pytest.fixture(
    params=[
        love(model=os.path.join(MODEL_DIR, 'prem_ani'),
             fmax=0.01, dispersion=True),
        rayleigh(model=os.path.join(MODEL_DIR, 'prem_ani'),
                 fmax=0.01, dispersion=True),
    ],
    ids=[
        "dispersion, toroidal",
        "dispersion, gravity",
    ],
    scope="module",
)
def dispersion_test_object(request):
    return request.param


def test_dispersion_att(dispersion_test_object):
    ra = dispersion_test_object
    att_mode = 'eigenvector continuation'
    llist = [5, 15, 27]
    refsfg = [1083.37513717, 1477.77691129, 2173.18885537, 2874.11553724,
              4298.99355045, 2344.17878607, 3189.16988269, 3773.63960142,
              4481.68011295, 8441.35926284, 3538.14803878, 4951.30705358,
              5734.89830058, 6532.62321692, 7276.44266581]
    refsph = [844.75788071, 1417.10611222, 1876.09652627, 2678.61208253,
              2946.79446423, 2330.4746624, 3187.44404362, 3728.13623863,
              4177.24570707, 4839.15266492, 3530.02650969, 4947.88190697,
              5732.33467852, 6526.83110067, 7248.74314528]
    reftor = [927.2864558, 1748.97233142, 2482.10742245, 3411.54890312,
              4453.92495547, 2206.22435129, 3405.2041039, 4193.03834559,
              4862.04651433, 5539.88390545, 3543.67772301, 5124.20885357,
              6153.7613704, 6950.7340028, 7691.62468173]
    if ra.mode == 'toroidal':
        frq_out = ra.love_problem(llist=llist,
                                  attenuation_mode=att_mode,
                                  n_total=5)['frequencies']
        frq_ref = reftor
    else:
        frq_out = ra.rayleigh_problem(llist=llist,
                                      attenuation_mode=att_mode,
                                      n_total=5)['frequencies']
        if ra.sph_type == 2:
            frq_ref = refsph
        else:
            frq_ref = refsfg

    np.testing.assert_allclose(frq_ref, frq_out * 1e6, rtol=1e-5)


def test_dispersion_noatt(dispersion_test_object):
    ra = dispersion_test_object
    att_mode = 'elastic'
    llist = [5, 15, 27]
    refsfg = [1089.94606336, 1491.80234794, 2185.13206309, 2891.98152677,
              4326.77490693, 2360.03972111, 3226.68402408, 3802.99012026,
              4511.58329541, 9779.27876521, 3571.21560456, 5005.57970424,
              5785.86661897, 6573.61057794, 7321.14571886]
    refsph = [851.53709215, 1429.67692234, 1889.83139743, 2689.04027931,
              2971.88928861, 2346.45484689, 3224.74768726, 3756.19717893,
              4204.33135807, 4869.93957142, 3563.17388878, 5002.10238598,
              5783.4010247, 6567.83789938, 7292.39611583]
    reftor = [937.01543105, 1763.52475083, 2504.69438224, 3439.73365711,
              4489.14800742, 2235.15024795, 3434.80414326, 4223.9909573,
              4896.15772491, 5581.27527351, 3591.90791544, 5172.48291535,
              6200.56176798, 7001.44467053, 7743.78727401]
    if ra.mode == 'toroidal':
        frq_out = ra.love_problem(llist=llist,
                                  attenuation_mode=att_mode,
                                  n_total=5)['frequencies']
        frq_ref = reftor
    else:
        frq_out = ra.rayleigh_problem(llist=llist,
                                      attenuation_mode=att_mode,
                                      n_total=5)['frequencies']
        if ra.sph_type == 2:
            frq_ref = refsph
        else:
            frq_ref = refsfg

    np.testing.assert_allclose(frq_ref, frq_out * 1e6, rtol=1e-5)
