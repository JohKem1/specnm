#!/usr/bin/env python
"""
Functions from pymodes package by Martin van Driel!
Adapted slightly to incorporate attenuation and get eigenfunctions W, U, V, P
like Dahlen & Tromp (1998).
:copyright:
    Martin van Driel
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2020
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
import warnings
from scipy.special import spherical_jn
from scipy.optimize import brentq
from .. import models_1D


def analytical_toroidal_eigenfunction(r, l, vs, omega):
    """
    D&T eq 8.118, assume omega known
    """
    return spherical_jn(l, omega * r / vs)


def analytical_radial_eigenfunction(r, vp, rho, omega, gravity=True):
    """
    D&T eq 8.167, assume omega known
    """
    G = models_1D.GRAVITY_G if gravity else 0.
    gamma = (omega ** 2 + 16. / 3. * np.pi * G * rho) ** 0.5 / vp
    return spherical_jn(1, gamma * r)


def analytical_spheroidal_eigenfunctions(omega, ll, rho, vs, vp, r,
                                         gravity=True, independent=False):
    """
    Compute the Matrix in the 'characteristic' or 'Rayleigh' function according
    to: Takeuchi & Saito (1972), in 'Methods in computational
    physics, Volume 11'. Eq (98)-(100) for including gravity, Eq (104) - (105)
    for neglecting gravity.

    :type omega: float
    :param omega: frequency at which to compute the matrix
    :type l: int
    :param l: angular order
    :type rho: float
    :param rho: density in kg / m ** 3
    :type vs: float
    :param float: s-wave velocity in m / s
    :type vp: float
    :param float: p-wave velocity in m / s
    :type r: float
    :param r: radius
    :type gravity: bool
    :param gravity: enable gravity in the calculation
    :type independent: bool
    :param independent: enable or disable independent solution for full gravity
    """

    # Auxiliar variables
    mu = rho * vs ** 2
    lam = rho * vp ** 2 - 2 * mu
    w2 = omega ** 2

    if gravity:
        gamma = 4. * np.pi * models_1D.GRAVITY_G * rho / 3.

        a = (w2 + 4. * gamma) / vp ** 2 + w2 / vs ** 2
        b = (w2 / vs ** 2) - (w2 + 4. * gamma) / vp ** 2
        c = 4. * ll * (ll + 1) * gamma ** 2 / (vp ** 2 * vs ** 2)
        d = (b ** 2 + c) ** 0.5

        # - root of k ** 2 - Eq (99) Takeuchi & Saito (1972)
        k1 = np.sqrt((a - d) / 2)
        f1 = vs ** 2 / gamma * (k1 ** 2 - w2 / vs ** 2)
        h1 = f1 - (ll + 1)
        x1 = k1 * r

        # + root of k ** 2  - Eq (99) Takeuchi & Saito (1972)
        k2 = np.sqrt((a + d) / 2)
        f2 = vs ** 2 / gamma * (k2 ** 2 - w2 / vs ** 2)
        h2 = f2 - (ll + 1)
        x2 = k2 * r

        # precompute bessel functions
        j_n_x1 = spherical_jn(ll, x1)
        j_np1_x1 = spherical_jn(ll + 1, x1)
        j_n_x2 = spherical_jn(ll, x2)
        j_np1_x2 = spherical_jn(ll + 1, x2)

        # Compute matrix's elements  - Eq (98 & 100) Takeuchi & Saito 1972
        # Solutions y1i (named as U by Dahlen & Tromp book)
        y11 = (ll * h1 * j_n_x1 - f1 * x1 * j_np1_x1) / r
        y12 = (ll * h2 * j_n_x2 - f2 * x2 * j_np1_x2) / r
        y13 = ll * r ** (ll - 1)

        # Solutions y2i (named as R by Dahlen & Tromp book)
        y21 = (-(lam + 2 * mu) * f1 * x1 ** 2 * j_n_x1 +
               2 * mu * ll * (ll - 1) * h1 * j_n_x1 +
               2 * mu * (2 * f1 + ll * (ll + 1)) * x1 * j_np1_x1) / r ** 2
        y22 = (-(lam + 2 * mu) * f2 * x2 ** 2 * j_n_x2 +
               2 * mu * ll * (ll - 1) * h2 * j_n_x2 +
               2 * mu * (2 * f2 + ll * (ll + 1)) * x2 * j_np1_x2) / r ** 2
        y23 = 2 * mu * ll * (ll - 1) * r ** (ll - 2)

        # Solutions y3i (named as V by Dahlen & Tromp book)
        y31 = (h1 * j_n_x1 + x1 * j_np1_x1) / r
        y32 = (h2 * j_n_x2 + x2 * j_np1_x2) / r
        y33 = r ** (ll - 1)

        # Solutions y4i (named as S by Dahlen & Tromp book)
        y41 = (mu * x1 ** 2 * j_n_x1 + mu * 2 * (ll - 1) * h1 * j_n_x1 -
               mu * 2 * (f1 + 1) * x1 * j_np1_x1) / r ** 2
        y42 = (mu * x2 ** 2 * j_n_x2 + mu * 2 * (ll - 1) * h2 * j_n_x2 -
               mu * 2 * (f2 + 1) * x2 * j_np1_x2) / r ** 2
        y43 = 2 * mu * (ll - 1) * r ** (ll - 2)

        # Solutions y5i (named as P by Dahlen & Tromp book) - Necessary to
        # compute y6i (B)
        y51 = 3 * gamma * f1 * j_n_x1
        y52 = 3 * gamma * f2 * j_n_x2
        y53 = (ll * gamma - w2) * r ** ll

        # Solutions y6i (named as B by Dahlen & Tromp book)
        y61 = ((2 * ll + 1) * y51 - 3 * ll * gamma * h1 * j_n_x1) / r
        y62 = ((2 * ll + 1) * y52 - 3 * ll * gamma * h2 * j_n_x2) / r
        y63 = ((2 * ll + 1) * y53 - 3 * ll * gamma * r ** ll) / r

        # Build matrix
        return \
            np.array([[y11, y12, y13], [y21, y22, y23], [y31, y32, y33],
                      [y41, y42, y43], [y51, y52, y53], [y61, y62, y63]])

    else:
        x1 = omega / vp * r

        j_n_x1 = spherical_jn(ll, x1)
        j_np1_x1 = spherical_jn(ll + 1, x1)

        y11 = (ll * j_n_x1 - x1 * j_np1_x1) / r
        y21 = (-(lam + 2 * mu) * x1 ** 2 * j_n_x1 +
               2 * mu * (ll * (ll - 1) * j_n_x1 + 2 * x1 * j_np1_x1)) / r ** 2
        y31 = j_n_x1 / r
        y41 = 2 * mu * ((ll - 1) * j_n_x1 - x1 * j_np1_x1) / r ** 2

        x2 = omega / vs * r

        j_n_x2 = spherical_jn(ll, x2)
        j_np1_x2 = spherical_jn(ll + 1, x2)

        y12 = -ll * (ll + 1) * j_n_x2 / r
        y22 = 2 * mu * (
            -ll * (ll ** 2 - 1) * j_n_x2 + ll * (ll + 1) * x2 * j_np1_x2)\
            / r ** 2
        y32 = (-(ll + 1) * j_n_x2 + x2 * j_np1_x2) / r
        y42 = mu * (x2 ** 2 * j_n_x2 - 2 * (ll ** 2 - 1) * j_n_x2 -
                    2 * x2 * j_np1_x2) / r ** 2

        return np.array([[y11, y12], [y21, y22], [y31, y32], [y41, y42]])


def analytical_bc_t(omega, ll, rho, vs, vp, R):
    """
    Compute the the 'characteristic' function according
    to: Takeuchi & Saito (1972), in 'Methods in computational
    physics, Volume 11', Eq (95)

    :type omega: float, scalar or array like
    :param omega: frequency at which to compute the matrix
    :type l: int
    :param l: angular order
    :type rho: float
    :param rho: density in kg / m ** 3
    :type vs: float
    :param float: s-wave velocity in m / s
    :type R: float
    :param R: radius of the sphere
    """
    mu = rho * vs ** 2
    omega = np.array(omega)
    k = omega / vs

    j_n = spherical_jn(ll, k * R)
    j_np1 = spherical_jn(ll + 1, k * R)

    y2 = mu / R * ((ll - 1) * j_n - k * R * j_np1)

    return y2


def analytical_characteristic_function_t(
        omega, ll, rho, vs, vp, R, gravity=False):
    """
    Compute the 'characteristic' or function - for toroidal modes this is just
    a wrapper to analytical_bc, see Takeuchi & Saito Eq (77)

    for parameters see analytical_bc
    """

    return analytical_bc_t(omega=omega, ll=ll, rho=rho, vs=vs, vp=vp, R=R)


def analytical_characteristic_function_r(
        omega, rho, vs, vp, R, gravity=True):
    """
    D&T 8.168
    """
    G = models_1D.GRAVITY_G if gravity else 0.
    gamma = (omega ** 2 + 16. / 3. * np.pi * G * rho) ** 0.5 / vp

    return (rho * vp ** 2 * gamma * spherical_jn(0, gamma * R) -
            4 * rho * vs ** 2 / R * spherical_jn(1, gamma * R))


def analytical_bc_s(omega, ll, rho, vs, vp, R, gravity=True):
    """
    Compute the Matrix in the 'characteristic' or 'Rayleigh' function according
    to: Takeuchi & Saito (1972), in 'Methods in computational
    physics, Volume 11'. Eq (98)-(100) for including gravity, Eq (104) - (105)
    for neglecting gravity.

    :type omega: float, scalar or array like
    :param omega: frequency at which to compute the matrix
    :type l: int
    :param l: angular order
    :type rho: float
    :param rho: density in kg / m ** 3
    :type vs: float
    :param float: s-wave velocity in m / s
    :type vp: float
    :param float: p-wave velocity in m / s
    :type R: float
    :param R: radius of the sphere
    :type gravity: bool
    :param gravity: enable gravity in the calculation
    """

    # Auxiliar variables
    mu = rho * vs ** 2
    lam = rho * vp ** 2 - 2 * mu
    omega = np.array(omega)
    w2 = omega ** 2

    if gravity:
        gamma = 4. * np.pi * models_1D.GRAVITY_G * rho / 3.

        a = (w2 + 4. * gamma) / vp ** 2 + w2 / vs ** 2
        b = (w2 / vs ** 2) - (w2 + 4. * gamma) / vp ** 2
        c = 4. * ll * (ll + 1) * gamma ** 2 / (vp ** 2 * vs ** 2)
        d = (b ** 2 + c) ** 0.5

        # - root of k ** 2 - Eq (99) Takeuchi & Saito (1972)
        # for long periods, d > a, which raises a NaN warning that can safely
        # be ignored
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            k1 = np.sqrt((a - d) / 2)
        f1 = vs ** 2 / gamma * (k1 ** 2 - w2 / vs ** 2)
        h1 = f1 - (ll + 1)
        x1 = k1 * R

        # + root of k ** 2  - Eq (99) Takeuchi & Saito (1972)
        k2 = np.sqrt((a + d) / 2)
        f2 = vs ** 2 / gamma * (k2 ** 2 - w2 / vs ** 2)
        h2 = f2 - (ll + 1)
        x2 = k2 * R

        # precompute bessel functions
        j_n_x1 = spherical_jn(ll, x1)
        j_np1_x1 = spherical_jn(ll + 1, x1)
        j_n_x2 = spherical_jn(ll, x2)
        j_np1_x2 = spherical_jn(ll + 1, x2)

        # Compute matrix's elements  - Eq (98 & 100) Takeuchi & Saito (1972)
        # Solutions y2i (named as R by Dahlen & Tromp book)
        y21 = (-vp ** 2 * rho * f1 * x1 ** 2 * j_n_x1 +
               2 * mu * ll * (ll - 1) * h1 * j_n_x1 +
               2 * mu * (2 * f1 + ll * (ll + 1)) * x1 * j_np1_x1) / R ** 2

        y22 = (-vp ** 2 * rho * f2 * x2 ** 2 * j_n_x2 +
               2 * mu * ll * (ll - 1) * h2 * j_n_x2 +
               2 * mu * (2 * f2 + ll * (ll + 1)) * x2 * j_np1_x2) / R ** 2

        y23 = 2 * mu * ll * (ll - 1) / R ** 2

        # Solutions y4i (named as S by Dahlen & Tromp book)
        y41 = (mu * x1 ** 2 * j_n_x1 + mu * 2 * (ll - 1) * h1 * j_n_x1 -
               mu * 2 * (f1 + 1) * x1 * j_np1_x1) / R ** 2

        y42 = (mu * x2 ** 2 * j_n_x2 + mu * 2 * (ll - 1) * h2 * j_n_x2 -
               mu * 2 * (f2 + 1) * x2 * j_np1_x2) / R ** 2

        y43 = 2 * mu * (ll - 1) / R ** 2

        # Solutions y5i (named as P by Dahlen & Tromp book) - Necessary to
        # compute y6i (B)
        y51 = 3 * gamma * f1 * j_n_x1
        y52 = 3 * gamma * f2 * j_n_x2
        y53 = (ll * gamma - w2)

        # Solutions y6i (named as B by Dahlen & Tromp book)
        y61 = ((2 * ll + 1) * y51 - 3 * ll * gamma * h1 * j_n_x1) / R
        y62 = ((2 * ll + 1) * y52 - 3 * ll * gamma * h2 * j_n_x2) / R
        y63 = ((2 * ll + 1) * y53 - 3 * ll * gamma) / R

        # make sure also frequency independent terms are vectorized in the same
        # way as omega
        y23 *= np.ones_like(omega)
        y43 *= np.ones_like(omega)

        # Build matrix
        return np.array([[y21, y22, y23], [y41, y42, y43], [y61, y62, y63]])

    else:
        x1 = omega / vp * R

        j_n_x1 = spherical_jn(ll, x1)
        j_np1_x1 = spherical_jn(ll + 1, x1)

        y21 = (-(lam + 2 * mu) * x1 ** 2 * j_n_x1 +
               2 * mu * (ll * (ll - 1) * j_n_x1 + 2 * x1 * j_np1_x1)) / R ** 2

        y41 = 2 * mu * ((ll - 1) * j_n_x1 - x1 * j_np1_x1) / R ** 2

        x2 = omega / vs * R

        j_n_x2 = spherical_jn(ll, x2)
        j_np1_x2 = spherical_jn(ll + 1, x2)

        y22 = 2 * mu * (
            -ll * (ll ** 2 - 1) * j_n_x2 + ll * (ll + 1) * x2 * j_np1_x2)\
            / R ** 2

        y42 = mu * (x2 ** 2 * j_n_x2 - 2 * (ll ** 2 - 1) * j_n_x2 -
                    2 * x2 * j_np1_x2) / R ** 2

        return np.array([[y21, y22], [y41, y42]])


def analytical_characteristic_function_s(
        omega, ll, rho, vs, vp, R, gravity=True):
    """
    Compute the determinant of the Matrix in the 'characteristic' or 'Rayleigh'
    function according to: Takeuchi & Saito (1972) - Eq (98)-(100), in 'Methods
    in computational physics, Volume 11'

    for paramters see analytical_bc
    """

    m = analytical_bc_s(omega=omega, ll=ll, rho=rho, vs=vs, vp=vp, R=R,
                        gravity=gravity)

    # np.linalg.det expects the last two axis to be the matrizes
    # analytical_bc returns the frequency on the last axis, so need to roll it
    # in case
    if m.ndim == 3:
        m = np.rollaxis(m, 2)

    # catch NaN warnings
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        return np.linalg.det(m)


def analytical_eigen_frequencies(omega_max, omega_delta, ll, rho, vs, vp,
                                 R, tol=1e-8, maxiter=100, mode='T',
                                 gravity=True, omega_min=0.):
    """
    Find the zeros of the determinant of the Matrix in the 'characteristic' or
    'Rayleigh' function according to: Takeuchi & Saito (1972), Eq (98)-(100)
    for spheroidal and Eq (95) for toroidal in 'Methods in computational
    physics, Volume 11'. Uses sampling first to find zero crossings and then
    Brent's method within the resulting intervals.

    :type omega_max: float
    :param omega_max: maximum frequency to look for zeros
    :type omega_delta: float
    :param omega_delta: spacing for initial sampling in finding zero crossings

    for other paramters see analytical_bc
    """

    # intial sampling of the characteristic function
    omega = np.arange(omega_min, omega_max, omega_delta)

    if (mode == 'T'):
        det = analytical_characteristic_function_t(
            omega=omega, ll=ll, rho=rho, vs=vs, vp=vp, R=R)
    elif (mode == 'S'):
        det = analytical_characteristic_function_s(
            omega=omega, ll=ll, rho=rho, vs=vs, vp=vp, R=R, gravity=gravity)
    elif (mode == 'R'):
        det = analytical_characteristic_function_r(
            omega=omega, rho=rho, vs=vs, vp=vp, R=R, gravity=gravity)
    else:
        raise ValueError

    # find intervals with zero crossing
    # catch NaN warnings
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        idx_zero_crossing = det[1:] * det[:-1] < 0
        # TODO: add a check that we are not in numerical noise

    nf = idx_zero_crossing.sum()

    omega_a = omega[:-1][idx_zero_crossing]
    omega_b = omega[1:][idx_zero_crossing]

    eigen_frequencies = np.zeros(nf)

    # loop over intervals and converge to tolerance using Brent's method
    for i in np.arange(nf):
        if (mode == 'T'):
            eigen_frequencies[i] = brentq(
                f=analytical_characteristic_function_t, a=omega_a[i],
                b=omega_b[i], args=(ll, rho, vs, vp, R, gravity), xtol=tol,
                maxiter=maxiter, disp=True)
        elif (mode == 'S'):
            eigen_frequencies[i] = brentq(
                f=analytical_characteristic_function_s, a=omega_a[i],
                b=omega_b[i], args=(ll, rho, vs, vp, R, gravity),
                xtol=tol, maxiter=maxiter, disp=True)
        elif (mode == 'R'):
            eigen_frequencies[i] = brentq(
                f=analytical_characteristic_function_r, a=omega_a[i],
                b=omega_b[i], args=(rho, vs, vp, R, gravity),
                xtol=tol, maxiter=maxiter, disp=True)
        else:
            raise ValueError

    return eigen_frequencies


def analytic_eigen_frequencies_att(omega_max, omega_delta, ll, rho, vs, vp,
                                   R, Qkappa, Qmu, tol=1e-8,
                                   maxiter=100, mode='T', gravity=True):
    """
    Calculate eigenfrequencies with attenuation based
    on standard linear solid (see Dahlen & Tromp eqs. 9.59 - 9.62)

    :type omega_max: float
    :param omega_max: maximum frequency to look for zeros
    :type omega_delta: float
    :param omega_delta: spacing for initial sampling in finding zero crossings
    :type Qkappa: float
    :param Qkappa: constant quality factor for kappa
    :type Qmu: float
    :param Qmu: constant quality factor for mu

    for other paramters see analytical_bc
    """
    vs0 = vs
    vp0 = vp

    # get quality factors for p and s wave velocities
    Qp0, Qs0 = calc_velQs(Qkappa, Qmu, vp0, vs0)

    wfrqs = analytical_eigen_frequencies(omega_max, omega_delta, ll, rho,
                                         vs0, vp0, R, tol,
                                         maxiter, mode, gravity)

    for e, omega in enumerate(wfrqs):
        dvp, dvs = update_attenuation(vp0, vs0, Qp0, Qs0, omega)
        wfrqs[e] = analytical_eigen_frequencies(omega * 1.1, omega_delta, ll,
                                                rho, vs0 + dvs, vp0 + dvp, R,
                                                tol, maxiter, mode, gravity)[e]

    return wfrqs


def update_attenuation(vp, vs, Qp, Qs, omega):
    """
    Calculate attenuations effect on kappa and mu
    :type vp: float
    :param vp: p wave velocity
    :type vs: float
    :param vs: s wave velocity
    :type Qp: float
    :param Qp: quality factor for p wave
    :type Qs: float
    :param Qs: quality factor for s wave
    :type omega: float
    :param omega: angular frequency for elastic case
    """
    dvp = vp / (Qp * np.pi) * np.log(omega / (2. * np.pi))
    dvs = vs / (Qs * np.pi) * np.log(omega / (2. * np.pi))
    return dvp, dvs


def calc_velQs(Qkappa, Qmu, vp, vs):
    """
    Calculate Qp and Qs from vp & vs and Qkappa & Qmu
    :type Qkappa: float
    :param Qkappa: compressional quality factor
    :type Qmu: float
    :param Qmu: shear quality factor
    :type vp: float
    :param vp: p wave velocity
    :type vs: float
    :param vs: s wave velocity
    """
    Qp = (1. - 4. / 3. * (vs ** 2 / vp ** 2)) * Qkappa\
        + 4. / 3. * (vs ** 2 / vp ** 2) * Qmu
    Qs = Qmu
    return Qp, Qs
