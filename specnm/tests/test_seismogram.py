#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Tests for database generation and seismogram computation.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2020
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np
import pytest
from instaseis import Source, Receiver

from ..seismogram import mode_database

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")

MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))

DATA_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.absolute(),
        "data"))


def test_compute_mode_database(tmpdir):

    filename = os.path.join(tmpdir, 'test_database.h5')
    # filename = os.path.join(DATA_DIR, 'test_database4.h5')

    mdb = mode_database.compute(
        model=os.path.join(MODEL_DIR, 'prem_ani'),
        fmax=0.01,
        mode_types='RST',
        gravity_mode='full',
        init_kwargs={'verbose': True},
        problem_kwargs={'attenuation_mode': 'first order', 'lmax': 10}
    )

    mdb.write_h5(filename)
    assert os.path.exists(filename) and os.path.getsize(filename) > 0


def test_recreate_mode_database(tmpdir):

    filename = os.path.join(DATA_DIR, 'test_database.h5')
    mdb1 = mode_database.read(filename)

    # need to use the relative path to run it in the CI
    mdb1.arguments['model'] = os.path.join(MODEL_DIR, 'prem_ani')
    mdb2 = mode_database.compute(**mdb1.arguments)

    # do a regression test
    for mt in 'RST':
        for k in mdb1.modes[mt].keys():
            atol = np.abs(mdb1.modes[mt][k]).max() * 1e-7

            # epsilon has a bit higher relative error
            if k == 'epsilons':
                atol *= 2000

            np.testing.assert_allclose(mdb1.modes[mt][k],
                                       mdb2.modes[mt][k], atol=atol)


@pytest.mark.parametrize("theta", np.linspace(0., np.pi, 7))
def test_Plm_cos_theta(theta):

    Plm, dPlm = mode_database.seismogram.Plm_cos_theta(3, theta)

    x = np.cos(theta)
    dx = -np.sin(theta)

    P10_ref = x
    dP10_ref = dx

    np.testing.assert_allclose(Plm[1, 0], P10_ref, atol=1e-10)
    np.testing.assert_allclose(dPlm[1, 0], dP10_ref, atol=1e-10)

    P22_ref = 3 * (1 - x ** 2)
    dP22_ref = - 6 * x * dx

    np.testing.assert_allclose(Plm[2, 2], P22_ref, atol=1e-10)
    np.testing.assert_allclose(dPlm[2, 2], dP22_ref, atol=1e-10)


def test_nAl():

    filename = os.path.join(DATA_DIR, 'test_database.h5')
    mdb = mode_database.read(filename)

    # no physical test, just making sure shapes align

    M = (1., 1., 1., 1., 1., 1.)
    theta = 0.324
    phi = 1.34566

    s_idx = -1
    r_idx = -1

    # toroidal
    W_s = mdb.modes['T']['eigenfunctions'][:, s_idx]
    W_dot_s = mdb.modes['T']['eigenfunctions_dr'][:, s_idx]
    W_r = mdb.modes['T']['eigenfunctions'][:, r_idx]

    l = mdb.modes['T']['angular orders']
    k = mdb.modes['T']['wave numbers']
    r_s = mdb.modes['T']['radius'][s_idx]

    nAl_t = mode_database.seismogram.nAl_toroidal(*M,
                                                  W_r, W_s, W_dot_s,
                                                  theta, phi, r_s, l, k)

    assert nAl_t.shape == (len(l), 3)

    # spheroidal
    U_s = mdb.modes['S']['eigenfunctions'][:, 0::3][:, s_idx]
    V_s = mdb.modes['S']['eigenfunctions'][:, 1::3][:, s_idx]
    U_dot_s = mdb.modes['S']['eigenfunctions_dr'][:, 0::3][:, s_idx]
    V_dot_s = mdb.modes['S']['eigenfunctions_dr'][:, 1::3][:, s_idx]
    U_r = mdb.modes['S']['eigenfunctions'][:, 0::3][:, r_idx]
    V_r = mdb.modes['S']['eigenfunctions'][:, 1::3][:, r_idx]

    l = mdb.modes['S']['angular orders']
    k = mdb.modes['S']['wave numbers']
    r_s = mdb.modes['S']['radius'][s_idx]

    nAl_s = mode_database.seismogram.nAl_spheroidal(*M,
                                                    U_r, V_r, U_s, U_dot_s,
                                                    V_s, V_dot_s,
                                                    theta, phi, r_s, l, k)

    assert nAl_s.shape == (len(l), 3)

    # radial
    U_s = mdb.modes['R']['eigenfunctions'][:, s_idx]
    U_dot_s = mdb.modes['R']['eigenfunctions_dr'][:, s_idx]
    U_r = mdb.modes['R']['eigenfunctions'][:, r_idx]

    l = mdb.modes['R']['angular orders']
    k = mdb.modes['R']['wave numbers']
    r_s = mdb.modes['R']['radius'][s_idx]

    nAl_r = mode_database.seismogram.nAl_radial(*M, U_r, U_s, U_dot_s,
                                                theta, phi, r_s)

    assert nAl_r.shape == (len(l),)


def test_lagrange_interpolation():
    def f(r):
        return np.sin(r / 2e6)

    filename = os.path.join(DATA_DIR, 'test_database.h5')
    mdb = mode_database.read(filename)

    r = mdb.modes['T']['radius']
    n = mdb.modes['T']['polynomial order']

    x = np.linspace(4e6, 6e6, 20)
    fx = [mode_database.seismogram.lagrange_interpolation(f(r), r, _x, n)
          for _x in x]

    np.testing.assert_allclose(fx, f(x))


def test_time_function():

    filename = os.path.join(DATA_DIR, 'test_database.h5')
    mdb = mode_database.read(filename)

    t0 = 200.
    dt = 0.01
    args = {'nt': int(1000 / dt), 't0': t0, 'dt': dt}

    M = np.ones(6) * 1e24
    src = Source(0., 90., 10e3, m_rr=M[0], m_tt=M[1], m_pp=M[2], m_rt=M[3],
                 m_rp=M[4], m_tp=M[5])

    rec = Receiver(0., 0.)

    tr1 = mdb.get_seismograms(src, rec, kind='displacement',
                              **args).select(channel='XZ')[0]
    tr2 = mdb.get_seismograms(src, rec, kind='velocity',
                              **args).select(channel='XZ')[0]
    tr3 = mdb.get_seismograms(src, rec, kind='acceleration',
                              **args).select(channel='XZ')[0]

    tr1.differentiate()
    atol = np.abs(tr1.data).max() * 1e-4
    np.testing.assert_allclose(tr1.data, tr2.data, atol=atol)

    tr2.differentiate()
    atol = np.abs(tr2.data).max() * 3e-5
    np.testing.assert_allclose(tr2.data, tr3.data, atol=atol)
