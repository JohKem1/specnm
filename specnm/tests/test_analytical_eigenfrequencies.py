#!/usr/bin/env python
'''
Tests against analytical modes.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2020
    Johannes Kemper (johannes.kemper@erdw.ethz.ch, 2023)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np
import pytest
from .analyticalsolutions import analytical_eigen_frequencies
from .. import love
from .. import radial
from .. import rayleigh
from .. import rayleigh_fg

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def test_compare_analytical_freqs_love():
    model = os.path.join(MODEL_DIR, 'homo-full-sphere2')
    fmin = 1e-5
    fmax = 0.005
    l0 = 1
    lmax = 5

    # toroidal
    lo = love(model=model, fmax=fmax*2)
    out = lo.love_problem(fmin=fmin, fmax=fmax, l0=l0, lmax=lmax)

    ws = out['angular frequencies']

    ws_ana = np.array([])
    for ll in range(l0, lmax + 1):
        w_tmp = analytical_eigen_frequencies(
            omega_max=fmax * 2. * np.pi, omega_delta=1e-6, ll=ll,
            rho=8e3, vs=7e3, vp=10e3, R=6371e3,
            tol=1e-12, maxiter=100, mode='T', gravity=False)
        ws_ana = np.append(ws_ana, w_tmp)

    np.testing.assert_allclose(ws, ws_ana, 4e-10)


@pytest.mark.parametrize("gravity", [False, True])
def test_compare_analytical_freqs_radial(gravity):

    model = os.path.join(MODEL_DIR, 'homo-full-sphere2')
    fmin = 1e-5
    fmax = 0.01

    ra = radial(model=model, fmax=fmax*2, gravity=gravity)
    out = ra.radial_problem(fmin=fmin, fmax=fmax)
    ws = out['angular frequencies']

    ws_ana = analytical_eigen_frequencies(
        omega_max=fmax * 2. * np.pi, omega_delta=1e-6, ll=0,
        rho=8e3, vs=7e3, vp=10e3, R=6371e3,
        tol=1e-12, maxiter=100, mode='R', gravity=gravity)

    np.testing.assert_allclose(ws, ws_ana, 2e-11)


# there is no cowling approximation in the analytic solution?
@pytest.mark.parametrize("gravity", [False])
def test_compare_analytical_freqs_rayleigh(gravity):

    model = os.path.join(MODEL_DIR, 'homo-full-sphere2')
    fmin = 1e-5
    fmax = 0.005
    l0 = 1
    lmax = 5
    # spheroidal cowling
    ry = rayleigh(model=model, fmax=fmax*2, gravity=gravity)
    out = ry.rayleigh_problem(fmin=fmin, fmax=fmax, l0=l0, lmax=lmax)

    ws = out['angular frequencies']

    ws_ana = np.array([])
    for ll in range(l0, lmax + 1):
        w_tmp = analytical_eigen_frequencies(
            omega_max=fmax * 2. * np.pi, omega_delta=1e-6, ll=ll,
            rho=8e3, vs=7e3, vp=10e3, R=6371e3,
            tol=1e-12, maxiter=100, mode='S', gravity=gravity)
        ws_ana = np.append(ws_ana, w_tmp)

    np.testing.assert_allclose(ws, ws_ana, 5e-10)


def test_compare_analytical_freqs_rayleigh_fg():

    model = os.path.join(MODEL_DIR, 'homo-full-sphere2')
    fmin = 1e-5
    fmax = 0.005
    l0 = 1
    lmax = 10
    # spheroidal full gravity
    ry = rayleigh_fg(model=model, fmax=fmax*2)
    out = ry.rayleigh_problem(fmin=fmin, fmax=fmax, l0=l0, lmax=lmax)

    ws = out['angular frequencies']

    ws_ana = np.array([])
    for ll in range(l0, lmax + 1):
        w_tmp = analytical_eigen_frequencies(
            omega_max=fmax * 2. * np.pi, omega_delta=1e-6, ll=ll,
            rho=8e3, vs=7e3, vp=10e3, R=6371e3,
            tol=1e-12, maxiter=100, mode='S', gravity=True)
        ws_ana = np.append(ws_ana, w_tmp)

    np.testing.assert_allclose(ws, ws_ana, 5e-10)
