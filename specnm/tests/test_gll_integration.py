#!/usr/bin/env python
"""
Unit test for general gll integration

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2020
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
from .. import rayleigh

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def test_gll_integration():
    model = os.path.join(MODEL_DIR, 'homo-full-sphere')
    fmax = 0.01

    # get a mesh
    ray = rayleigh(fmax=fmax, model=model)
    r = ray.r / ray.radius
    n = ray.n

    # analytical functions
    ff = 3. * r + r ** n
    fi = 1.5 * r[-1] ** 2 + 1. / (n + 1) * r[-1] ** (n + 1)

    # get integration weights and jacobian vector
    wopjop = ray.weights_vec * ray.jacobian_vec / ray.radius

    np.testing.assert_allclose(fi, ff.dot(wopjop), atol=1e-20)
