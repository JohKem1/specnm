#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Tests for spheroidal modes without full gravity.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2020
    Johannes Kemper (johannes.kemper@erdw.ethz.ch, 2023)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np
import pytest

from .. import rayleigh
from ..multishift_solver import multishift_slepc

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def test_rayleigh_problem():
    ll = 3
    fmax = 0.0015
    fmin = 0.5 / 3600.
    polyorder = 5

    ry = rayleigh(model=os.path.join(MODEL_DIR, 'prem_ani'),
                  fmax=fmax, n=polyorder,
                  refine_center=False)

    # test if direct call to solver and member function give same frequencies
    M, K = ry.global_matrices(ll)

    ef, ev = multishift_slepc(K, M, fmin, fmax*2)

    ry._eigenvalue_problem_init('elastic')
    ef2, ev2 = ry._eigenvalue_problem(ll, fmin, fmax*2)

    np.testing.assert_allclose(ef, ef2)

    # test full catalogue
    modes = ry.rayleigh_problem(attenuation_mode='elastic', fmin=fmin,
                                fmax=fmax, lmax=20)

    fref = np.array([0.38416357, 0.95881154, 1.43148698, 0.24419498, 0.6901619,
                     0.88526982, 1.10863795, 0.40229198, 0.95475151,
                     1.24619498, 1.38262865, 0.59481517, 1.1854577, 1.38209837,
                     0.80050228, 1.37438309, 1.00761149, 1.20828509,
                     1.39555421])
    cpref = np.array([10873.98307294, 27139.74283415, 40519.10821681,
                      3990.69541659, 11278.79825471, 14467.30060304,
                      18117.63844925, 4648.77182569, 11032.83702458,
                      14400.67493793, 15977.26368283, 5324.20186972,
                      10611.05428416, 12371.18867063, 5850.45199626,
                      10044.6463094, 6223.8044513, 6463.42742773,
                      6583.66822247])
    cgref = np.array([12776.37564105, 5403.82678167, 11211.24703978,
                      5242.96507357, 11125.30781994, 21499.63158912,
                      6112.28700502, 7119.91641557, 9803.13325759,
                      5903.75047254, 17362.94514243, 8023.66077535,
                      8458.27456314, 5260.28084536, 8286.11103481,
                      6497.41791968, 8185.86938055, 7790.96376245,
                      7137.40375987])
    Qref = np.array([367.40486932, 783.360788, 373.96344227, 327.73877176,
                     321.37637406, 81.6459836, 430.75810306, 306.2820013,
                     294.70434928, 407.03207054, 88.23580716, 314.0041816,
                     284.21429148, 373.15922642, 322.38127457, 310.82795359,
                     327.88792893, 330.55037152, 330.82129087])

    np.testing.assert_allclose(modes['frequencies'] * 1e3, fref, rtol=2e-8)
    np.testing.assert_allclose(modes['phase velocities'],
                               cpref, rtol=1e-10)
    np.testing.assert_allclose(modes['group velocities'],
                               cgref, rtol=1e-10)
    np.testing.assert_allclose(modes['Q'], Qref, rtol=1e-10)


@pytest.fixture(
    params=[
        rayleigh(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=0.002, n=4,
                 gravity=False),
        rayleigh(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=0.002, n=4,
                 gravity=True),
        rayleigh(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=0.002, n=6,
                 gravity=True),
    ],
    ids=[
        "fmax=0.002, n=4, no gravity",
        "fmax=0.002, n=4, gravity",
        "fmax=0.002, n=6, gravity",
    ],
    scope="module",
)
def rayleigh_test_object(request):
    return request.param


@pytest.mark.parametrize("ll", [1, 3, 10, 15, 30])
def test_rayleigh_problem_att(rayleigh_test_object, ll):
    ry = rayleigh_test_object

    fmax = ry.fmesh
    fmin = 0.5 / 3600.
    np.random.seed(123)

    # elastic solution
    ry._update_physical_properties(2 * np.pi * (fmax*2 * fmin) ** 0.5)
    ry._eigenvalue_problem_init('elastic')
    ef_el, ev_el = ry._eigenvalue_problem(ll, fmin, fmax*2)

    # do a triple recursion to get a most accurate eigenvalue as reference
    ef_rec = np.zeros_like(ef_el)

    for i, f in enumerate(ef_el):
        # finding same mode based on frequency, only works for some ll and
        # max frequencies, but should be good enough for this test
        eps = f * 1e-2
        ry._update_physical_properties(2 * np.pi * f)
        f2, _ev = ry._eigenvalue_problem(ll, f-eps, f+eps)

        ry._update_physical_properties(2 * np.pi * f2)
        f2, _ev = ry._eigenvalue_problem(ll, f-eps, f+eps)

        ry._update_physical_properties(2 * np.pi * f2)
        ef_rec[i], _ev = ry._eigenvalue_problem(ll, f2-eps, f2+eps)

    # use eigenvector continuation
    ry._eigenvalue_problem_init('eigenvector continuation')

    # use eigenvector continuation with 2 eigenvectors
    ef_evc, ev = ry._eigenvalue_problem(ll, fmin, fmax*2)

    np.testing.assert_allclose(ef_rec, ef_evc, rtol=1e-7)

    # use eigenvector continuation with 3 eigenvectors
    ef_evc, ev = ry._eigenvalue_problem(ll, fmin, fmax*2,
                                        number_of_eigenvectors=3)
    np.testing.assert_allclose(ef_rec, ef_evc, rtol=1e-9)

    # use eigenvector continuation with 4 eigenvectors
    ef_evc, ev = ry._eigenvalue_problem(ll, fmin, fmax*2,
                                        number_of_eigenvectors=4)
    np.testing.assert_allclose(ef_rec, ef_evc, rtol=3e-6)

    # but significant difference for elastic
    assert np.max(np.abs((ef_rec - ef_el) / ef_el)) > 1e-3


@pytest.mark.parametrize("ll", [1, 3, 10, 15, 30])
def test_rayleigh_problem_att_new(rayleigh_test_object, ll):
    ry = rayleigh_test_object

    fmax = ry.fmesh
    fmin = 0.5 / 3600.
    np.random.seed(123)

    # use eigenvector continuation as reference
    ry._eigenvalue_problem_init('eigenvector continuation')
    ef_ref, ev_ref = ry._eigenvalue_problem(ll, fmin, fmax,
                                            number_of_eigenvectors=3)

    ry._eigenvalue_problem_init('eigenvector continuation stepwise')
    ef, ev = ry._eigenvalue_problem(ll, fmin, fmax)

    np.testing.assert_allclose(ef_ref, ef, rtol=1e-7)


@pytest.mark.parametrize("attenuation_mode, tol",
                         [('elastic', 5e-7),
                          ('first order', 5e-7),
                          ('eigenvector continuation stepwise', 2.5e-3)])
def test_rayleigh_group_velocity(rayleigh_test_object, attenuation_mode, tol):

    ry = rayleigh_test_object

    fmax = 0.002
    fmin = 0.5 / 3600.
    eps_l = 1e-5
    l0 = 1
    lmax = 20 + 2 * eps_l

    modes1 = ry.rayleigh_problem(attenuation_mode=attenuation_mode,
                                 l0=l0, lmax=lmax, fmin=fmin, fmax=fmax)
    modes2 = ry.rayleigh_problem(attenuation_mode=attenuation_mode,
                                 l0=l0-eps_l, lmax=lmax, fmin=fmin, fmax=fmax)
    modes3 = ry.rayleigh_problem(attenuation_mode=attenuation_mode,
                                 l0=l0+eps_l, lmax=lmax, fmin=fmin, fmax=fmax)

    cg = modes1['group velocities']
    w2 = modes2['angular frequencies']
    w3 = modes3['angular frequencies']
    k2 = modes2['wave numbers']
    k3 = modes3['wave numbers']

    # compare direct computation from eigenfunctions with finite difference of
    # cg = dw/dk
    cg_fd = (w3 - w2) / (k3 - k2)

    np.testing.assert_allclose(cg, cg_fd * ry.radius, rtol=tol)


def test_rayleigh_gamma(rayleigh_test_object):

    ry = rayleigh_test_object
    f0 = ry.fmesh

    # first compute elastic with moduli evaluated at f0
    ry._update_physical_properties(2 * np.pi * f0)
    modes_elastic = ry.rayleigh_problem(attenuation_mode='elastic', l0=1,
                                        lmax=5)
    f_elastic = modes_elastic['frequencies']

    # as a reference, use eigenvector continuation
    modes_evc = ry.rayleigh_problem(
        attenuation_mode='eigenvector continuation', l0=1, lmax=5)
    f_evc = modes_evc['frequencies']

    # compute linearized change in omega (first order)
    w = modes_elastic['angular frequencies']
    Q = modes_elastic['Q']

    # D&T 9.55
    delta_w = w / np.pi / Q * np.log(w / (2 * np.pi * f0))

    f_fo = f_elastic + delta_w / (2 * np.pi)

    # interestingly, the error is smaller for spurious modes
    np.testing.assert_allclose(f_fo, f_evc, rtol=2.2e-4)

    # make sure linearized version is better than elastic
    err_elastic = np.abs(f_elastic - f_evc)
    err_fo = np.abs(f_fo - f_evc)
    assert np.all(err_elastic > err_fo)

    # compare with direct appliction of first order shift in the problem
    modes_fo = ry.rayleigh_problem(attenuation_mode='first order', l0=1,
                                   lmax=5)
    f_fo2 = modes_fo['frequencies']

    np.testing.assert_allclose(f_fo2, f_fo, rtol=1e-10)


def test_rayleigh_epsilon(rayleigh_test_object):
    ry = rayleigh_test_object

    modes = ry.rayleigh_problem(attenuation_mode='elastic', lmax=20)
    np.testing.assert_equal(modes['epsilons'] > 1e-3,
                            modes['energy ratios'] > 0.999)

    modes = ry.rayleigh_problem(attenuation_mode='eigenvector continuation',
                                lmax=20)
    np.testing.assert_equal(modes['epsilons'] > 1e-3,
                            modes['energy ratios'] > 0.999)

    modes = ry.rayleigh_problem(
        attenuation_mode='eigenvector continuation stepwise', lmax=20)
    np.testing.assert_equal(modes['epsilons'] > 1e-3,
                            modes['energy ratios'] > 0.999)


def test_rayleigh_catalogue():
    fmax = 0.02
    lmax = 10

    ry1 = rayleigh(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=fmax*1.5,
                   n=4, gravity=True)
    ry2 = rayleigh(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=fmax*1.5,
                   n=5, gravity=True)

    # elastic
    modes1 = ry1.rayleigh_problem(attenuation_mode='elastic', l0=1, lmax=lmax,
                                  fmax=fmax)

    modes2 = ry2.rayleigh_problem(attenuation_mode='elastic', l0=1, lmax=lmax,
                                  fmax=fmax)

    np.testing.assert_allclose(modes1['frequencies'], modes2['frequencies'],
                               rtol=1.5e-5)

    # anelastic
    modes1 = ry1.rayleigh_problem(
        attenuation_mode='eigenvector continuation stepwise',
        l0=1, lmax=lmax, fmax=fmax)

    modes2 = ry2.rayleigh_problem(
        attenuation_mode='eigenvector continuation stepwise',
        l0=1, lmax=lmax, fmax=fmax)

    np.testing.assert_allclose(modes1['frequencies'], modes2['frequencies'],
                               rtol=1.5e-5)
