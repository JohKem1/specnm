#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Tests all Python files for code formatting.
This ensure the standard PEP8 style guide.

:copyright:
    Johannes Kemper, 2018
:license:
    GNU Lesser General Public License, Version 3 [non-commercial/academic use]
    (http://www.gnu.org/copyleft/lgpl.html)
"""

import inspect
import os
import logging
from flake8.api import legacy as flake8  # NOQS


def test_formatting():
    logging.getLogger('flake8').setLevel(logging.WARN)
    test_dir = os.path.dirname(os.path.abspath(inspect.getfile(
        inspect.currentframe())))
    specnm_dir = os.path.dirname(test_dir)

    # ignore old files that are not used anyway
    ignore_files = ["__init__.py"]

    files = []
    for dirpath, _, filenames in os.walk(specnm_dir):
        filenames = [_i for _i in filenames if
                     os.path.splitext(_i)[-1] == os.path.extsep + "py"]
        if not filenames:
            continue
        for python_file in filenames:
            if python_file not in ignore_files:
                files.append(os.path.join(dirpath, python_file))

    # Import the legacy API as flake8 3.0 currently has not official
    # public API - this has to be changed at some point.
    style_guide = flake8.get_style_guide(
        ignore=['E741', 'W504', 'E126', 'E24', 'E121', 'E123', 'E704', 'W503',
                'E226'])
    report = style_guide.check_files(files)

    assert report.total_errors == 0
