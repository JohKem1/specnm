#!/usr/bin/env python
"""
Test for spheroidal eigenfrequencies to mineos.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2020
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
from .. import rayleigh_fg
import sys

import warnings
import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))

DATA_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.absolute(),
        "benchmark_mineos"))


def test_spheroidal_prem_fg_noat():
    # disable warnings
    if not sys.warnoptions:
        warnings.simplefilter("ignore")

    # setting up specnm
    ray = rayleigh_fg(model=os.path.join(MODEL_DIR, 'prem_noocean_noat'),
                      fmax=0.01)

    # produce catalogue without attenuation
    ray_out = ray.rayleigh_problem(attenuation_mode='elastic', lmax=15)

    mineos = np.genfromtxt(os.path.join(DATA_DIR,
                                        'spheroidal_prem_noat_l1tol25'),
                           skip_header=1)

    l_vec = mineos[:, 2].astype(int)
    w_vec = mineos[:, 4]

    for l in range(1, 15):
        w_ref = w_vec[l_vec == l]
        w = ray_out['frequencies'][ray_out['angular orders'] == l]
        np.testing.assert_allclose(w_ref[:len(w)], w * 1e3, 6e-5)
