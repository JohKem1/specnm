#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Tests for toroidal modes.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2020
    Johannes Kemper (johannes.kemper@erdw.ethz.ch, 2023)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np
import pytest

from .. import love
from ..multishift_solver import multishift_slepc

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def test_love_problem():
    ll = 3
    fmax = 0.0015
    fmin = 0.5 / 3600.
    polyorder = 4

    lo = love(model=os.path.join(MODEL_DIR, 'prem_ani'),
              fmax=fmax, n=polyorder)

    # test if direct call to solver and member function give same frequencies
    M, K = lo.global_matrices(ll)

    ef, ev = multishift_slepc(K, M, fmin, fmax*2)

    lo._eigenvalue_problem_init('elastic')
    ef2, ev2 = lo._eigenvalue_problem(ll, fmin, fmax*2)

    np.testing.assert_allclose(ef, ef2)

    # test full catalogue
    modes = lo.love_problem(attenuation_mode='elastic', fmin=fmin, fmax=fmax)

    fref = np.array([1.24500725, 0.38278109, 1.32974168, 0.59159947,
                     1.44970046, 0.77278012, 0.93701566, 1.08925583, 1.2327801,
                     1.36984618])
    cpref = np.array([35240.6862774, 6255.5041991, 21730.97083104,
                      6836.3553554, 16752.32643026, 6917.16947568,
                      6848.15676202, 6728.10441457, 6594.45749808,
                      6462.38796618])
    cgref = np.array([2454.69439053, 9083.35844946, 4058.60782261, 7613.414788,
                      5354.3195604, 6829.48943469, 6282.13875437,
                      5884.24420036, 5591.2732335, 5370.60143315])
    Qref = np.array([265.68926614, 255.68450259, 262.08887677, 245.15807334,
                     258.55421974, 233.35364576, 221.53683167, 210.55487947,
                     200.75067315, 192.15952499])

    np.testing.assert_allclose(modes['frequencies'] * 1e3, fref, rtol=1e-8)
    np.testing.assert_allclose(modes['phase velocities'],
                               cpref, rtol=1e-10)
    np.testing.assert_allclose(modes['group velocities'],
                               cgref, rtol=1e-10)
    np.testing.assert_allclose(modes['Q'], Qref, rtol=1e-10)


@pytest.fixture(
    params=[
        love(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=0.01, n=4),
        love(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=0.01, n=6),
    ],
    ids=[
        "fmax=0.01, n=4",
        "fmax=0.01, n=6"
    ],
    scope="module",
)
def love_test_object(request):
    return request.param


@pytest.mark.parametrize("ll", [1, 3, 6, 10, 15, 20, 30])
def test_love_problem_att(love_test_object, ll):
    fmax = 0.01
    fmin = 0.5 / 3600.
    lo = love_test_object

    np.random.seed(1234)

    # elastic solution
    lo._update_physical_properties(2 * np.pi * (fmax*2 * fmin) ** 0.5)
    lo._eigenvalue_problem_init('elastic')
    ef_el, ev_el = lo._eigenvalue_problem(ll, fmin, fmax*2)

    # do a triple recursion to get a most accurate eigenvalue as reference
    ef_rec = np.zeros_like(ef_el)

    for i, f in enumerate(ef_el):
        eps = f / 30.
        lo._update_physical_properties(2 * np.pi * f)
        f2, _ev = lo._eigenvalue_problem(ll, f-eps, f+eps)

        lo._update_physical_properties(2 * np.pi * f2)
        f2, _ev = lo._eigenvalue_problem(ll, f-eps, f+eps)

        lo._update_physical_properties(2 * np.pi * f2)
        ef_rec[i], _ev = lo._eigenvalue_problem(ll, f2-eps, f2+eps)

    # use eigenvector continuation
    lo._eigenvalue_problem_init('eigenvector continuation')

    # use eigenvector continuation with 2 eigenvectors
    ef_evc, ev = lo._eigenvalue_problem(ll, fmin, fmax*2)

    np.testing.assert_allclose(ef_rec, ef_evc, rtol=6e-8)

    # use eigenvector continuation with 3 eigenvectors
    ef_evc, ev = lo._eigenvalue_problem(ll, fmin, fmax*2,
                                        number_of_eigenvectors=3)
    np.testing.assert_allclose(ef_rec, ef_evc, rtol=3e-10)

    # use eigenvector continuation with 4 eigenvectors
    ef_evc, ev = lo._eigenvalue_problem(ll, fmin, fmax*2,
                                        number_of_eigenvectors=4)
    np.testing.assert_allclose(ef_rec, ef_evc, rtol=1.7e-8)

    # but significant difference for elastic
    assert np.max(np.abs((ef_rec - ef_el) / ef_el)) > 1e-3


@pytest.mark.parametrize("ll", [1, 3, 6, 10, 15, 20, 30])
def test_love_problem_att_new(love_test_object, ll):
    fmax = love_test_object.fmesh
    fmin = 0.5 / 3600.
    lo = love_test_object

    # use eigenvector continuation as reference
    lo._eigenvalue_problem_init('eigenvector continuation')
    ef_ref, ev = lo._eigenvalue_problem(ll, fmin, fmax,
                                        number_of_eigenvectors=3)

    # test stepwise eigenvector continuation
    lo._eigenvalue_problem_init('eigenvector continuation stepwise')
    ef, ev = lo._eigenvalue_problem(ll, fmin, fmax)

    np.testing.assert_allclose(ef_ref, ef, rtol=1e-8)


@pytest.mark.parametrize("attenuation_mode, tol",
                         [('elastic', 3e-7),
                          ('first order', 3e-7),
                          ('eigenvector continuation stepwise', 1e-4)])
def test_love_group_velocity(love_test_object, attenuation_mode, tol):

    eps_l = 1e-4
    lmax = 10

    lo = love_test_object

    modes1 = lo.love_problem(attenuation_mode=attenuation_mode, l0=3.,
                             lmax=lmax)
    modes2 = lo.love_problem(attenuation_mode=attenuation_mode, l0=3.-eps_l,
                             lmax=lmax)
    modes3 = lo.love_problem(attenuation_mode=attenuation_mode, l0=3.+eps_l,
                             lmax=lmax+2*eps_l)

    cg = modes1['group velocities']
    w2 = modes2['angular frequencies']
    w3 = modes3['angular frequencies']
    k2 = modes2['wave numbers']
    k3 = modes3['wave numbers']

    # compare direct computation from eigenfunctions with finite difference of
    # cg = dw/dk
    cg_fd = (w3 - w2) / (k3 - k2)

    np.testing.assert_allclose(cg, cg_fd * lo.radius, rtol=tol)


def test_love_gamma(love_test_object):
    lo = love_test_object
    f0 = 0.01

    # first compute elastic with moduli evaluated at f0
    lo._update_physical_properties(2 * np.pi * f0)
    modes_elastic = lo.love_problem(attenuation_mode='elastic', l0=1, lmax=5)
    f_elastic = modes_elastic['frequencies']

    # as a reference, use eigenvector continuation
    modes_evc = lo.love_problem(attenuation_mode='eigenvector continuation',
                                l0=1, lmax=5)
    f_evc = modes_evc['frequencies']

    # compute linearized change in omega (first order)
    w = modes_elastic['angular frequencies']
    Q = modes_elastic['Q']

    # D&T 9.55
    delta_w = w / np.pi / Q * np.log(w / (2 * np.pi * f0))

    f_fo = f_elastic + delta_w / (2 * np.pi)
    np.testing.assert_allclose(f_fo, f_evc, rtol=2e-4)

    # make sure linearized version is better than elastic
    err_elastic = np.abs(f_elastic - f_evc)
    err_fo = np.abs(f_fo - f_evc)
    assert np.all(err_elastic > err_fo)

    # compare with direct appliction of first order shift in the problem
    modes_fo = lo.love_problem(attenuation_mode='first order', l0=1, lmax=5)
    f_fo2 = modes_fo['frequencies']

    np.testing.assert_allclose(f_fo2, f_fo, rtol=1e-10)


def test_love_epsilon(love_test_object):
    lo = love_test_object
    fmax = 0.005

    modes = lo.love_problem(attenuation_mode='elastic', l0=1, lmax=5,
                            fmax=fmax)
    np.testing.assert_allclose(modes['epsilons'], 0., atol=5e-8)

    modes = lo.love_problem(attenuation_mode='eigenvector continuation',
                            l0=1, lmax=5, fmax=fmax)
    np.testing.assert_allclose(modes['epsilons'], 0., atol=5e-8)

    modes = lo.love_problem(
        attenuation_mode='eigenvector continuation stepwise',
        l0=1, lmax=5, fmax=fmax)
    np.testing.assert_allclose(modes['epsilons'], 0., atol=6e-8)


def test_love_catalogue():
    fmax = 0.1
    lmax = 20

    lo1 = love(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=fmax, n=4)
    lo2 = love(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=fmax, n=5)

    # elastic
    modes1 = lo1.love_problem(attenuation_mode='elastic', l0=1, lmax=lmax,
                              fmax=fmax)

    modes2 = lo2.love_problem(attenuation_mode='elastic', l0=1, lmax=lmax,
                              fmax=fmax)

    np.testing.assert_allclose(modes1['frequencies'], modes2['frequencies'],
                               rtol=2.5e-5)

    # anelastic
    modes1 = lo1.love_problem(
        attenuation_mode='eigenvector continuation stepwise',
        l0=1, lmax=lmax, fmax=fmax)

    modes2 = lo2.love_problem(
        attenuation_mode='eigenvector continuation stepwise',
        l0=1, lmax=lmax, fmax=fmax)

    np.testing.assert_allclose(modes1['frequencies'], modes2['frequencies'],
                               rtol=2.5e-5)
