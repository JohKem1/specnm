# -*- coding: utf-8 -*-
'''
Tests against analytical eigenfunctions.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2020
    Johannes Kemper (johannes.kemper@erdw.ethz.ch, 2023)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np
import pytest
from .. import love, radial, rayleigh, rayleigh_fg
from .analyticalsolutions import (analytical_toroidal_eigenfunction,
                                  analytical_radial_eigenfunction,
                                  analytical_spheroidal_eigenfunctions)

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def renormalize(fn, fr):
    """
    Renormalize and make function fn the same as fr
    """
    fn /= np.linalg.norm(fn)
    fn *= np.linalg.norm(fr)
    # use crosscorrelation to determine sign, as last value could be zero
    fn *= np.sign((fn * fr).sum())
    return fn


def test_analytical_eigenfuncs_love():
    model = os.path.join(MODEL_DIR, 'homo-full-sphere2')
    fmin = 1e-5
    fmax = 0.002
    l0 = 1
    lmax = 5

    vs = 7e3

    lo = love(model=model, fmax=fmax*2)
    out = lo.love_problem(fmin=fmin, fmax=fmax, l0=l0, lmax=lmax)

    ws = out['angular frequencies']
    ls = out['angular orders']

    for i, (w, l) in enumerate(zip(ws, ls)):
        # assume that the eigenfrequencies from specnm are correct
        w_ref = analytical_toroidal_eigenfunction(lo.r[1:], l, vs, w)
        w_specnm = out['eigenfunctions'][i][1:]

        w_ref = renormalize(w_ref, w_specnm)

        atol = 1e-6 * np.abs(w_ref).max()
        np.testing.assert_allclose(w_ref, w_specnm, 1e-15, atol=atol)


@pytest.mark.parametrize("gravity", [False, True])
def test_analytical_eigenfuncs_radial(gravity):
    model = os.path.join(MODEL_DIR, 'homo-full-sphere2')
    fmin = 1e-5
    fmax = 0.002

    rho = 8e3
    vp = 10e3

    # toroidal
    ra = radial(model=model, fmax=fmax*2, gravity=gravity)
    out = ra.radial_problem(fmin=fmin, fmax=fmax)

    ws = out['angular frequencies']
    ls = out['angular orders']

    for i, (w, l) in enumerate(zip(ws, ls)):
        # assume that the eigenfrequencies from specnm are correct
        u_ref = analytical_radial_eigenfunction(ra.r[1:], vp, rho, w,
                                                gravity=gravity)
        u_specnm = out['eigenfunctions'][i][1:]

        u_ref = renormalize(u_ref, u_specnm)

        atol = 1e-7 * np.abs(u_ref).max()
        np.testing.assert_allclose(u_ref, u_specnm, 1e-15, atol=atol)


def test_analytical_eigenfuncs_rayleigh():
    model = os.path.join(MODEL_DIR, 'homo-full-sphere2')
    fmin = 1e-5
    fmax = 0.002
    l0 = 1
    lmax = 5

    rho = 8e3
    vs = 7e3
    vp = 10e3

    ry = rayleigh(model=model, fmax=fmax*2, gravity=False)
    out = ry.rayleigh_problem(fmin=fmin, fmax=fmax, l0=l0, lmax=lmax)

    ws = out['angular frequencies']
    ls = out['angular orders']

    for i, (w, l) in enumerate(zip(ws, ls)):
        # assume that the eigenfrequencies from specnm are correct
        [[U1, U2], [R1, R2], [V1, V2], [S1, S2]] = \
                analytical_spheroidal_eigenfunctions(w, l, rho, vs, vp,
                                                     ry.r[1:], gravity=False)

        BC_matrix = np.array([[R1[-1], R2[-1]], [S1[-1], S2[-1]]])
        cf = np.linalg.det(BC_matrix)

        # make sure the determinant is in fact 'zero'
        assert cf < 1e-9 * np.abs(BC_matrix).max()

        prefac = R1[-1] / R2[-1]

        u_ref = U1 - prefac * U2
        v_ref = V1 - prefac * V2

        u_specnm = out['eigenfunctions'][i, 0::2][1:]
        v_specnm = out['eigenfunctions'][i, 1::2][1:]

        u_ref = renormalize(u_ref, u_specnm)
        v_ref = renormalize(v_ref, v_specnm)

        atol = 1e-6 * np.abs(u_ref).max()
        np.testing.assert_allclose(u_ref, u_specnm, 1e-15, atol=atol)

        atol = 1e-6 * np.abs(v_ref).max()
        np.testing.assert_allclose(v_ref, v_specnm, 1e-15, atol=atol)


def test_analytical_eigenfuncs_rayleigh_fg():
    model = os.path.join(MODEL_DIR, 'homo-full-sphere2')
    fmin = 1e-5
    fmax = 0.002
    l0 = 1
    lmax = 5

    rho = 8e3
    vs = 7e3
    vp = 10e3

    ry = rayleigh_fg(model=model, fmax=fmax*2)
    out = ry.rayleigh_problem(fmin=fmin, fmax=fmax, l0=l0, lmax=lmax)

    ws = out['angular frequencies']
    ls = out['angular orders']

    for i, (w, l) in enumerate(zip(ws, ls)):
        # assume that the eigenfrequencies from specnm are correct
        [[U1, U2, U3], [R1, R2, R3], [V1, V2, V3],
         [S1, S2, S3], [P1, P2, P3], [B1, B2, B3]] = \
                analytical_spheroidal_eigenfunctions(w, l, rho, vs, vp,
                                                     ry.r[1:], gravity=True,
                                                     independent=False)

        # stress matrix at the free surface
        BC_matrix = np.array([[R1[-1], R2[-1], R3[-1]],
                              [S1[-1], S2[-1], S3[-1]],
                              [B1[-1], B2[-1], B3[-1]]])

        # renormalize the matrix columns
        BC_norm = (BC_matrix ** 2).sum(axis=0) ** 0.5
        BC_matrix = BC_matrix / BC_norm[np.newaxis, :]

        # characteristic function
        cf = np.linalg.det(BC_matrix)

        # make sure the determinant is in fact 'zero'
        assert cf < 1e-7 * np.abs(BC_matrix).max()

        # solve for the weights of the three independent solutions, using a
        # weight of 1 for the first and solving for the two others
        v1 = BC_matrix[:, 0]
        v23 = BC_matrix[:, 1:]
        a, b = np.linalg.pinv(v23, 1e-5).dot(-v1)

        # compute solution which fulfills the free surface BC
        u_ref = U1 / BC_norm[0] + a * U2 / BC_norm[1] + b * U3 / BC_norm[2]
        v_ref = V1 / BC_norm[0] + a * V2 / BC_norm[1] + b * V3 / BC_norm[2]
        p_ref = P1 / BC_norm[0] + a * P2 / BC_norm[1] + b * P3 / BC_norm[2]

        # check if surface is indeed stress free
        r_ref = R1 / BC_norm[0] + a * R2 / BC_norm[1] + b * R3 / BC_norm[2]
        s_ref = S1 / BC_norm[0] + a * S2 / BC_norm[1] + b * S3 / BC_norm[2]
        b_ref = B1 / BC_norm[0] + a * B2 / BC_norm[1] + b * B3 / BC_norm[2]

        assert r_ref[-1] < 1e-5 * np.abs(r_ref).max()
        assert s_ref[-1] < 1e-5 * np.abs(s_ref).max()
        assert b_ref[-1] < 1e-5 * np.abs(b_ref).max()

        # compare analytic to specnm eigenfunctions
        u_specnm = out['eigenfunctions'][i, 0::3][1:]
        v_specnm = out['eigenfunctions'][i, 1::3][1:]
        p_specnm = out['eigenfunctions'][i, 2::3][1:]

        u_ref = renormalize(u_ref, u_specnm)
        v_ref = renormalize(v_ref, v_specnm)
        p_ref = renormalize(p_ref, p_specnm)

        atol = 2e-6 * np.abs(u_ref).max()
        np.testing.assert_allclose(u_ref, u_specnm, 1e-15, atol=atol)

        atol = 3e-6 * np.abs(v_ref).max()
        np.testing.assert_allclose(v_ref, v_specnm, 1e-15, atol=atol)

        # seems like the error is a bit higher on P
        atol = 2e-5 * np.abs(p_ref).max()
        np.testing.assert_allclose(p_ref, p_specnm, 1e-15, atol=atol)
