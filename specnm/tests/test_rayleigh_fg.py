#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Tests for spheroidal mode with full gravity.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2020
    Johannes Kemper (johannes.kemper@erdw.ethz.ch, 2023)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np
import pytest

from .. import rayleigh_fg
from ..multishift_solver import multishift_slepc

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def test_rayleigh_problem_fg():
    ll = 3
    fmax = 0.0015
    fmin = 0.5 / 3600.
    polyorder = 4

    ry = rayleigh_fg(model=os.path.join(MODEL_DIR, 'prem_ani'),
                     fmax=fmax, n=polyorder,
                     refine_center=False)

    # test if direct call to solver and member function give same frequencies
    M, K = ry.global_matrices(ll)

    ef, ev = multishift_slepc(K, M, fmin, fmax*2)

    ry._eigenvalue_problem_init('elastic')
    ef2, ev2 = ry._eigenvalue_problem(ll, fmin, fmax*2)

    np.testing.assert_allclose(ef, ef2)

    # test full catalogue
    modes = ry.rayleigh_problem(attenuation_mode='elastic', fmin=fmin,
                                fmax=fmax, lmax=20)

    fref = np.array([0.40633184, 0.94585541, 1.41991793, 0.31081348,
                     0.68442423, 0.94334052, 1.11097704, 0.47129354,
                     0.94656349, 1.2467396, 1.41206904, 0.65116796, 1.18143932,
                     1.38547329, 0.84588065, 1.37955886, 1.04496207,
                     1.23964458, 1.42214242])

    cpref = np.array([11501.46934181, 26773.01157616, 40191.63929701,
                      5079.39160239, 11185.03176118, 15416.30657838,
                      18155.86452885, 5446.13413966, 10938.21860009,
                      14406.96838535, 16317.46849506, 5828.61676004,
                      10575.085677, 12401.39768229, 6182.09870909,
                      10082.47333089, 6454.51117292, 6631.17736408,
                      6709.10080919])

    cgref = np.array([12397.60587561, 5792.85469612, 10944.12723335,
                      6305.73620531, 10788.3341207, 19117.16187658,
                      6919.01743068, 6694.0915189, 9871.10083061,
                      6148.47609305, 16051.56858171, 7526.13729555,
                      8722.0776084, 5223.0898185, 7911.92529312, 6920.37528556,
                      7912.75631741, 7580.02199327, 6965.12677525])

    Qref = np.array([407.04399776, 865.89848121, 359.94376347, 518.84214177,
                     318.00948256, 102.29033486, 374.40016895, 426.9539548,
                     289.5495241, 424.87509338, 96.7223161, 381.93416506,
                     277.67513168, 392.71403589, 363.79684032, 299.85566566,
                     355.21626096, 349.6342585, 344.70335741])

    np.testing.assert_allclose(modes['frequencies'] * 1e3, fref, rtol=3e-8)
    np.testing.assert_allclose(modes['phase velocities'],
                               cpref, rtol=1e-10)
    np.testing.assert_allclose(modes['group velocities'],
                               cgref, rtol=1e-10)
    np.testing.assert_allclose(modes['Q'], Qref, rtol=5e-9)


@pytest.fixture(
    params=[
        rayleigh_fg(model=os.path.join(MODEL_DIR, 'prem_ani'),
                    fmax=0.002, n=4),
        rayleigh_fg(model=os.path.join(MODEL_DIR, 'prem_ani'),
                    fmax=0.002, n=6),
        # rayleigh_fg(model=os.path.join(MODEL_DIR, 'prem_ani'),
        #             fmax=0.01, n=6),
    ],
    ids=[
        "fmax=0.002, n=4",
        "fmax=0.002, n=6",
        # "fmax=0.01, n=6",
    ],
    scope="module",
)
def rayleigh_test_object(request):
    return request.param


@pytest.mark.parametrize("ll", [1, 3, 10, 15, 20, 30])
def test_rayleigh_problem_att_fg(rayleigh_test_object, ll):
    ry = rayleigh_test_object

    fmax = ry.fmesh
    fmin = 0.5 / 3600.

    np.random.seed(1234)

    # elastic solution
    ry._update_physical_properties(2 * np.pi * (fmax*2 * fmin) ** 0.5)
    ry._eigenvalue_problem_init('elastic')
    ef_el, ev_el = ry._eigenvalue_problem(ll, fmin, fmax*2)

    # do a triple recursion to get a most accurate eigenvalue as reference
    ef_rec = np.zeros_like(ef_el)

    for i, f in enumerate(ef_el):
        # finding same mode based on frequency, only works for some ll and
        # max frequencies, but should be good enough for this test
        eps = f * 1e-2
        ry._update_physical_properties(2 * np.pi * f)
        f2, _ev = ry._eigenvalue_problem(ll, f-eps, f+eps)

        ry._update_physical_properties(2 * np.pi * f2)
        f2, _ev = ry._eigenvalue_problem(ll, f-eps, f+eps)

        ry._update_physical_properties(2 * np.pi * f2)
        ef_rec[i], _ev = ry._eigenvalue_problem(ll, f2-eps, f2+eps)

    # use eigenvector continuation
    ry._eigenvalue_problem_init('eigenvector continuation')

    # use eigenvector continuation with 2 eigenvectors
    ef_evc, ev = ry._eigenvalue_problem(ll, fmin, fmax*2)

    np.testing.assert_allclose(ef_rec, ef_evc, rtol=2e-7)

    # use eigenvector continuation with 3 eigenvectors
    ef_evc, ev = ry._eigenvalue_problem(ll, fmin, fmax*2,
                                        number_of_eigenvectors=3)
    np.testing.assert_allclose(ef_rec, ef_evc, rtol=2e-9)

    # use eigenvector continuation with 4 eigenvectors
    ef_evc, ev = ry._eigenvalue_problem(ll, fmin, fmax*2,
                                        number_of_eigenvectors=4)
    np.testing.assert_allclose(ef_rec, ef_evc, rtol=2e-6)

    # but significant difference for elastic
    assert np.max(np.abs((ef_rec - ef_el) / ef_el)) > 1e-3


@pytest.mark.parametrize("ll", [1, 3, 10, 15, 20, 30])
def test_rayleigh_problem_att_new(rayleigh_test_object, ll):
    ry = rayleigh_test_object

    fmax = ry.fmesh
    fmin = 0.5 / 3600.
    np.random.seed(123)

    # use eigenvector continuation as reference
    ry._eigenvalue_problem_init('eigenvector continuation')
    ef_ref, ev_ref = ry._eigenvalue_problem(ll, fmin, fmax,
                                            number_of_eigenvectors=3)

    ry._eigenvalue_problem_init('eigenvector continuation stepwise')
    ef, ev = ry._eigenvalue_problem(ll, fmin, fmax)

    np.testing.assert_allclose(ef_ref, ef, rtol=1e-7)


@pytest.mark.parametrize("attenuation_mode, tol",
                         [('elastic', 1e-6),
                          ('first order', 1e-6),
                          ('eigenvector continuation stepwise', 5e-3)])
def test_rayleigh_group_velocity_fg(rayleigh_test_object, attenuation_mode,
                                    tol):

    ry = rayleigh_test_object
    fmax = ry.fmesh
    fmin = 0.5 / 3600.
    eps_l = 1e-5
    l0 = 1

    lmax = 20 + 2 * eps_l

    modes1 = ry.rayleigh_problem(attenuation_mode=attenuation_mode,
                                 l0=l0, lmax=lmax, fmin=fmin, fmax=fmax)
    modes2 = ry.rayleigh_problem(attenuation_mode=attenuation_mode,
                                 l0=l0-eps_l, lmax=lmax, fmin=fmin, fmax=fmax)
    modes3 = ry.rayleigh_problem(attenuation_mode=attenuation_mode,
                                 l0=l0+eps_l, lmax=lmax, fmin=fmin, fmax=fmax)

    cg = modes1['group velocities']
    w2 = modes2['angular frequencies']
    w3 = modes3['angular frequencies']
    k2 = modes2['wave numbers']
    k3 = modes3['wave numbers']

    # compare direct computation from eigenfunctions with finite difference of
    # cg = dw/dk
    cg_fd = (w3 - w2) / (k3 - k2)

    np.testing.assert_allclose(cg, cg_fd * ry.radius, rtol=tol)


def test_rayleigh_gamma_fg(rayleigh_test_object):

    ry = rayleigh_test_object
    fmin = 0.5 / 3600.
    f0 = ry.fmesh
    l0 = 2
    lmax = 2

    # first compute elastic with moduli evaluated at f0
    ry._update_physical_properties(2 * np.pi * f0)
    modes_elastic = ry.rayleigh_problem(attenuation_mode='elastic', l0=l0,
                                        lmax=lmax, fmin=fmin)
    f_elastic = modes_elastic['frequencies']

    # as a reference, use eigenvector continuation
    modes_evc = ry.rayleigh_problem(
        attenuation_mode='eigenvector continuation', l0=l0, lmax=lmax)
    f_evc = modes_evc['frequencies']

    # compute linearized change in omega (first order)
    w = modes_elastic['angular frequencies']
    Q = modes_elastic['Q']

    # D&T 9.55
    delta_w = w / np.pi / Q * np.log(w / (2 * np.pi * f0))

    f_fo = f_elastic + delta_w / (2 * np.pi)

    # interestingly, the error is smaller for spurious modes
    np.testing.assert_allclose(f_fo, f_evc, rtol=2.2e-4)
    # np.testing.assert_allclose(f_fo[:100], f_evc[:100], rtol=2.2e-4)
    # return

    # make sure linearized version is better than elastic
    err_elastic = np.abs(f_elastic - f_evc)
    err_fo = np.abs(f_fo - f_evc)
    assert np.all(err_elastic > err_fo)

    # compare with direct appliction of first order shift in the problem
    modes_fo = ry.rayleigh_problem(attenuation_mode='first order', l0=l0,
                                   lmax=lmax)
    f_fo2 = modes_fo['frequencies']

    np.testing.assert_allclose(f_fo2, f_fo, rtol=1e-10)


def test_rayleigh_epsilon_fg(rayleigh_test_object):
    ry = rayleigh_test_object

    modes = ry.rayleigh_problem(attenuation_mode='elastic', lmax=20)
    np.testing.assert_equal(modes['epsilons'] > 4e-4,
                            modes['energy ratios'] > 0.999)

    modes = ry.rayleigh_problem(attenuation_mode='eigenvector continuation',
                                lmax=20)
    np.testing.assert_equal(modes['epsilons'] > 4e-4,
                            modes['energy ratios'] > 0.999)

    modes = ry.rayleigh_problem(
        attenuation_mode='eigenvector continuation stepwise', lmax=20)
    np.testing.assert_equal(modes['epsilons'] > 4e-4,
                            modes['energy ratios'] > 0.999)


def test_rayleigh_catalogue():
    fmax = 0.02
    lmax = 10

    ry1 = rayleigh_fg(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=fmax*1.5,
                      n=4)
    ry2 = rayleigh_fg(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=fmax*1.5,
                      n=5)

    # elastic
    modes1 = ry1.rayleigh_problem(attenuation_mode='elastic', l0=1, lmax=lmax,
                                  fmax=fmax)

    modes2 = ry2.rayleigh_problem(attenuation_mode='elastic', l0=1, lmax=lmax,
                                  fmax=fmax)

    np.testing.assert_allclose(modes1['frequencies'], modes2['frequencies'],
                               rtol=3e-6)

    # anelastic
    modes1 = ry1.rayleigh_problem(
        attenuation_mode='eigenvector continuation stepwise',
        l0=1, lmax=lmax, fmax=fmax)

    modes2 = ry2.rayleigh_problem(
        attenuation_mode='eigenvector continuation stepwise',
        l0=1, lmax=lmax, fmax=fmax)

    np.testing.assert_allclose(modes1['frequencies'], modes2['frequencies'],
                               rtol=4e-6)
