#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Tests for radial modes.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2020
    Johannes Kemper (johannes.kemper@erdw.ethz.ch, 2023)
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np
import pytest

from .. import radial
from ..multishift_solver import multishift_slepc

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def test_radial_problem():
    ll = 0
    fmax = 0.004
    fmin = 0.5 / 3600.
    polyorder = 4

    ra = radial(model=os.path.join(MODEL_DIR, 'prem_ani'),
                fmax=fmax, n=polyorder,
                refine_center=False)

    # test if direct call to solver and member function give same frequencies
    M, K = ra.global_matrices(ll)

    ef, ev = multishift_slepc(K, M, fmin, fmax*2)

    ra._eigenvalue_problem_init('elastic')
    ef2, ev2 = ra._eigenvalue_problem(ll, fmin, fmax*2)

    np.testing.assert_allclose(ef, ef2)

    # test full catalogue (only single l for radial)
    modes = ra.radial_problem(attenuation_mode='elastic', fmin=fmin, fmax=fmax)

    fref = np.array([0.81437506, 1.63321983, 2.51352914, 3.27554998])
    Qref = np.array([5437.92878716, 1508.60445118, 1248.41812449,
                     1093.05225861])

    np.testing.assert_allclose(modes['frequencies'] * 1e3, fref, rtol=1e-8)
    np.testing.assert_allclose(modes['Q'], Qref, rtol=1e-10)


@pytest.fixture(
    params=[
        radial(model=os.path.join(MODEL_DIR, 'prem_ani'),
               fmax=0.01, n=4, gravity=False),
        radial(model=os.path.join(MODEL_DIR, 'prem_ani'),
               fmax=0.01, n=4, gravity=True),
        radial(model=os.path.join(MODEL_DIR, 'prem_ani'),
               fmax=0.01, n=6, gravity=True),
    ],
    ids=[
        "fmax=0.01, n=4, no gravity",
        "fmax=0.01, n=4, gravity",
        "fmax=0.01, n=6, gravity",
    ],
    scope="module",
)
def radial_test_object(request):
    return request.param


def test_radial_problem_att(radial_test_object):
    ra = radial_test_object

    fmax = ra.fmesh
    fmin = 0.5 / 3600.

    np.random.seed(1234)

    # elastic solution
    ra._update_physical_properties(2 * np.pi * (fmax*2 * fmin) ** 0.5)
    ra._eigenvalue_problem_init('elastic')
    ef_el, ev_el = ra._eigenvalue_problem(0, fmin, fmax*2)

    # do a triple recursion to get a most accurate eigenvalue as reference
    ef_rec = np.zeros_like(ef_el)

    for i, f in enumerate(ef_el):
        eps = f / 30.
        ra._update_physical_properties(2 * np.pi * f)
        f2, _ev = ra._eigenvalue_problem(0, f-eps, f+eps)

        ra._update_physical_properties(2 * np.pi * f2)
        f2, _ev = ra._eigenvalue_problem(0, f-eps, f+eps)

        ra._update_physical_properties(2 * np.pi * f2)
        ef_rec[i], _ev = ra._eigenvalue_problem(0, f2-eps, f2+eps)

    # use eigenvector continuation
    ra._eigenvalue_problem_init('eigenvector continuation')

    # use eigenvector continuation with 2 eigenvectors
    ef_evc, ev = ra._eigenvalue_problem(0, fmin, fmax*2)

    np.testing.assert_allclose(ef_rec, ef_evc, rtol=5e-8)

    # use eigenvector continuation with 3 eigenvectors
    ef_evc, ev = ra._eigenvalue_problem(0, fmin, fmax*2,
                                        number_of_eigenvectors=3)
    np.testing.assert_allclose(ef_rec, ef_evc, rtol=3e-10)

    # use eigenvector continuation with 4 eigenvectors
    ef_evc, ev = ra._eigenvalue_problem(0, fmin, fmax*2,
                                        number_of_eigenvectors=4)
    np.testing.assert_allclose(ef_rec, ef_evc, rtol=2e-8)

    # but significant difference for elastic
    assert np.max(np.abs((ef_rec - ef_el) / ef_el)) > 9e-4


def test_radial_problem_att_new(radial_test_object):
    ra = radial_test_object

    fmax = ra.fmesh
    fmin = 0.5 / 3600.

    # use eigenvector continuation as reference
    ra._eigenvalue_problem_init('eigenvector continuation')
    ef_ref, ev = ra._eigenvalue_problem(0, fmin, fmax,
                                        number_of_eigenvectors=3)

    # test stepwise eigenvector continuation
    ra._eigenvalue_problem_init('eigenvector continuation stepwise')
    ef, ev = ra._eigenvalue_problem(0, fmin, fmax)

    np.testing.assert_allclose(ef_ref, ef, rtol=1e-9)


def test_radial_gamma(radial_test_object):

    ra = radial_test_object
    f0 = 0.01

    # first compute elastic with moduli evaluated at f0
    ra._update_physical_properties(2 * np.pi * f0)
    modes_elastic = ra.radial_problem(attenuation_mode='elastic')
    f_elastic = modes_elastic['frequencies']

    # as a reference, use eigenvector continuation
    modes_evc = ra.radial_problem(attenuation_mode='eigenvector continuation')
    f_evc = modes_evc['frequencies']

    # compute linearized change in omega (first order)
    w = modes_elastic['angular frequencies']
    Q = modes_elastic['Q']

    # D&T 9.55
    delta_w = w / np.pi / Q * np.log(w / (2 * np.pi * f0))

    f_fo = f_elastic + delta_w / (2 * np.pi)
    np.testing.assert_allclose(f_fo, f_evc, rtol=1e-5)

    # make sure linearized version is better than elastic
    err_elastic = np.abs(f_elastic - f_evc)
    err_fo = np.abs(f_fo - f_evc)
    assert np.all(err_elastic > err_fo)

    # compare with direct appliction of first order shift in the problem
    modes_fo = ra.radial_problem(attenuation_mode='first order')
    f_fo2 = modes_fo['frequencies']

    np.testing.assert_allclose(f_fo2, f_fo, rtol=1e-10)


def test_radial_epsilon(radial_test_object):
    ra = radial_test_object
    fmax = 0.005

    modes = ra.radial_problem(attenuation_mode='elastic', fmax=fmax)
    np.testing.assert_allclose(modes['epsilons'], 0., atol=8e-8)

    modes = ra.radial_problem(attenuation_mode='eigenvector continuation',
                              fmax=fmax)
    np.testing.assert_allclose(modes['epsilons'], 0., atol=8e-8)

    modes = ra.radial_problem(
        attenuation_mode='eigenvector continuation stepwise',
        fmax=fmax)
    np.testing.assert_allclose(modes['epsilons'], 0., atol=8e-8)


def test_radial_catalogue():
    fmax = 0.1

    ra1 = radial(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=fmax, n=4)
    ra2 = radial(model=os.path.join(MODEL_DIR, 'prem_ani'), fmax=fmax, n=5)

    # elastic
    modes1 = ra1.radial_problem(attenuation_mode='elastic', fmax=fmax)
    modes2 = ra2.radial_problem(attenuation_mode='elastic', fmax=fmax)

    np.testing.assert_allclose(modes1['frequencies'], modes2['frequencies'],
                               rtol=1.5e-5)

    # anelastic
    modes1 = ra1.radial_problem(
        attenuation_mode='eigenvector continuation stepwise', fmax=fmax)

    modes2 = ra2.radial_problem(
        attenuation_mode='eigenvector continuation stepwise', fmax=fmax)

    np.testing.assert_allclose(modes1['frequencies'], modes2['frequencies'],
                               rtol=1.5e-5)
