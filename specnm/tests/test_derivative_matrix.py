#!/usr/bin/env python
"""
Unit test for derivative matrix

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2020
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
from .. import rayleigh

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def test_gll_derivatives():
    model = os.path.join(MODEL_DIR, 'homo-full-sphere')
    fmax = 0.01

    # get a mesh
    ray = rayleigh(fmax=fmax, model=model)
    r = ray.r / ray.radius
    n = ray.n

    # analytical functions
    ff = 3. * r + r ** n
    df = 3. * np.ones_like(r) + n * r ** (n - 1)

    # get derivate matrix and jacobian vector
    Dr = ray.l_prime_mat
    jac_vec = ray.jacobian_vec / ray.radius

    np.testing.assert_allclose(df, Dr.dot(ff) / jac_vec, atol=1e-18)
