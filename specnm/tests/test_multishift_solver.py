#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Tests for multishift solver.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2020
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
'''
import numpy as np

from .. import rayleigh_fg
from ..multishift_solver import multishift_slepc

import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))


def test_multishift_solver():
    ll = 3
    fmax = 0.01
    fmin = 0.5 / 3600.
    polyorder = 4

    model = os.path.join(MODEL_DIR, 'prem_ani')
    ray_fg = rayleigh_fg(model=model, fmax=fmax, n=polyorder,
                         refine_center=False)
    M, K = ray_fg.global_matrices(ll)

    ef, ev = multishift_slepc(K, M, fmin, fmax)

    ef_ref = np.array([0.00047127, 0.00094658, 0.00124762, 0.00128515,
                       0.00145093, 0.00151252, 0.00182271, 0.00205637,
                       0.00218192, 0.00223323, 0.00282016, 0.00283248,
                       0.00314773, 0.00330402, 0.00356202, 0.00411264,
                       0.00432254, 0.00447758, 0.00477934, 0.00520154,
                       0.00544506, 0.00604514, 0.00634252, 0.00649453,
                       0.00690269, 0.00754793, 0.00774229, 0.00790347,
                       0.00859162, 0.00863908, 0.00934188, 0.00948182,
                       0.00966181])

    np.testing.assert_allclose(ef, ef_ref, atol=1e-8)

    # test fixed number of modes
    for n in [1, 5, 15, 23, len(ef_ref)]:
        ef, ev = multishift_slepc(K, M, fmin, n_total=n)
        np.testing.assert_allclose(ef, ef_ref[:n], atol=1e-8)

    # test max number and max frequency where fmax hits
    ef, ev = multishift_slepc(K, M, fmin, fmax=ef_ref[10], n_total=20)
    np.testing.assert_allclose(ef, ef_ref[:10], atol=1e-8)

    # test max number and max frequency where n hits
    ef, ev = multishift_slepc(K, M, fmin, fmax=ef_ref[15], n_total=10)
    np.testing.assert_allclose(ef, ef_ref[:10], atol=1e-8)
