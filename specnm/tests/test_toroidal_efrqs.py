import numpy as np
from .. import love
import sys
import warnings
import inspect
import os
import pathlib


frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")

MODEL_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.parent.absolute(),
        "models"))

DATA_DIR = str(os.path.join(
        pathlib.Path(inspect.getfile(frame)).parent.absolute(),
        "benchmark_mineos"))


def read_mineos(fname):
    f = open(fname, 'r')

    nlw = [[line.split()[0],
            line.split()[2],
            line.split()[4]] for line in f.readlines()[1:]]

    ns = np.array(nlw)[:, 0].astype('int')
    ls = np.array(nlw)[:, 1].astype('int')
    ws = np.array(nlw)[:, 2].astype('float')

    return ns, ls, ws


def test_mineos_toroidal_noat():
    # disable warnings
    if not sys.warnoptions:
        warnings.simplefilter("ignore")

    # setting up specnm
    lov = love(model=os.path.join(MODEL_DIR, 'prem_noocean'), fmax=0.01)

    # produce catalogue without attenuation
    lout = lov.love_problem(attenuation_mode='elastic', lmax=50)

    # read mineos results
    [n_vec, l_vec, w_vec] = \
        read_mineos(fname=os.path.join(DATA_DIR, 'toroidal_att_off'))

    for l in range(1, 50):
        w_ref = w_vec[l_vec == l]
        w = lout['frequencies'][lout['angular orders'] == l]
        np.testing.assert_allclose(w_ref[:len(w)], w * 1e3, 2e-6)
