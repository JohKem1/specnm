#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A class to handle radial modes,
eigenfunctions and dispersion curves.

:copyright:
    |  Martin van Driel (Martin@vanDriel.de), 2016
    |  Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2018-2019
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np  # numerical python
from .specnm_base import spc_base  # god class
from .spheroidal_base import sph_base  # base class of spheroidal
from .sensitivity import sensitivity  # sensitivity kernels
from scipy.sparse import lil_matrix, block_diag, identity  # sparse matrices


class radial(sph_base, spc_base, sensitivity):
    def __init__(self, *args, **kwargs):
        # pass all arguments and keyword-argument tuples to base class
        spc_base.__init__(self, *args, **kwargs, mode='radial', sph_type=1)

    def radial_problem(self, fmin=0.5/3600., fmax=None, attenuation_mode=None,
                       sensitivity_kernels='none', **multishift_kwargs):
        """
        Produces the spectrum of eigenvalues and eigenfunctions
        until the max frequency is reached

        :type fmin: float
        :param fmin: minimum output frequency (fmin<=freq)
        :type fmax: float
        :param fmax: maximum output frequency (freq<=fmax)
        :type attenuation_mode: str
        :param attenuation_mode: attenuation mode for calculation
                                 {'no' or 'elastic',
                                 'first order',
                                 'eigenvector continuation',
                                 'full' or
                                 'eigenvector continuation stepwise'}
        :type sensitivity_kernels: str
        :param sensitivity_kernels: Produce sensitivity kernels (default none)
                                    {'none', 'isotropic', 'anisotropic'}
        :type multishift_kwargs: dict
        :param multishift_kwargs: additional arguments for the multishift
                                  solver
        :returns: Structured output with keys:
                  angular orders, wave numbers,
                  frequencies, angular frequencies,
                  eigenfunctions, eigenfunctions_dr,
                  phase velocities, group velocities,
                  gamma, Q, epsilons, energy ratios,
                  radius, polynomial order, gravitational acceleration
        :rtype: dict
        """

        # for compatibility with spheroidal interface
        multishift_kwargs.pop('l0', 0)
        multishift_kwargs.pop('lmax', 0)

        attenuation = (self.model_attenuation and
                       attenuation_mode not in ['elastic', 'first order'])

        if (self.verbose):
            print('Initialization of solver and start of calculation.')

        # initialize eigenvalue problem
        self._eigenvalue_problem_init(attenuation_mode)

        if (fmax is None):
            fmax = self.fmesh
        elif (fmax > self.fmesh):
            raise ValueError(
                'The maximum input frequency was \
                 higher than maximum frequency of the mesh.')

        if attenuation_mode == 'first order':
            f0 = fmax
            self._update_physical_properties(2.0 * np.pi * f0)

        # solve the eigenvalue problem in base class
        ef, ev = self._eigenvalue_problem(0, fmin, fmax, **multishift_kwargs)

        # get eigenfunctions according to Dahlen&Tromp
        ev *= self._sign(ev[:, -1])[:, np.newaxis]

        if (self.verbose):
            print('Current angular order: 0')
            print(f'Number of overtones  : {len(ef)}')

        ls = np.zeros_like(ef, dtype=int)
        ks = np.zeros_like(ef, dtype=float)
        fs = np.copy(ef)
        ws = 2.0 * np.pi * fs

        Us = self.scatter.dot(ev.T).T

        # normalize eigenfunctions
        Us /= self._normalize(Us)[:, np.newaxis]

        if self.model_attenuation:
            gammas = self._calculate_gammas(ks, ws, Us)
            Qs = ws / gammas / 2.0
        else:
            gammas = np.zeros_like(ef)
            Qs = np.zeros_like(ef) + np.inf

        if attenuation_mode == 'first order':
            # Dahlen & Tromp (eq. 9.55)
            fs += fs / np.pi / Qs * np.log(fs / f0)

        epsilons = self._epsilon(Us, ws, attenuation=attenuation)

        # derivative of eigenfunction
        jop = self.jacobian_vec[np.newaxis, :]
        derop = self.l_prime_mat
        Udot = derop.dot(Us.T).T / jop

        structured_output = {
            'angular orders': ls,
            'wave numbers': ks,
            'frequencies': fs,
            'angular frequencies': ws,
            'eigenfunctions': Us,
            'eigenfunctions_dr': Udot,
            'phase velocities': None,
            'group velocities': None,
            'gamma': gammas,
            'Q': Qs,
            'epsilons': epsilons,
            'radius': self.r,
            'polynomial order': self.n,
            'gravitational acceleration': self.g_acc,
        }

        # related to sensitivity kernel output
        sensitivities = {'isotropic': self.isotropic_kernels,
                         'anisotropic': self.anisotropic_kernels}

        if sensitivity_kernels in sensitivities:
            structured_output['sensitivity kernels'] =\
                sensitivities[sensitivity_kernels](structured_output)

        return structured_output

    def _precompute_stiffness_mass_matrices(self):
        """
        Precomputes the stiffness and mass matrices
        also gets norms, weights and lprime etc.
        """
        # Build gather and scatter operators
        # gather and scatter will act on x and matrices.
        self.gather, self.scatter = \
            self._gather_scatter_operator(self.fluid_elements)

        # Precompute patterns for medium matrices
        mini_blocks = [np.ones((self.n + 1, self.n + 1))] * self.Ne
        self.Kpattern = block_diag(mini_blocks, format='csr')
        self.Idiag = identity(self.Ne * (self.n + 1), format='csr')

        # Compute gll points
        self.r_ass = self.gather.dot(self.r.flatten())

        # Build mass matrix (Eq. 33 - Documentation)
        self.M = self.__global_mass()

        # Build components of the stiffness matrix K - indep. of material prop.
        [self.stiffK_AN,
         self.stiffK_C,
         self.stiffK_F] = self.__global_stiffnessK()

        if not self.gravity:
            self.G = 0.
        else:
            # build the gravity matrix G.
            self.G = self.__global_gravity()

        # build medium matrices for linear solver with reference frequency
        self._build_medium_matrices()

        # save medium matrices without anelasticity for later reset
        self.Cmatrix_block0 = self.Cmatrix_block.copy()
        self.Fmatrix_block0 = self.Fmatrix_block.copy()
        self.Kmatrix_ANmed0 = self.Kmatrix_ANmed.copy()

    def _precompute_mass_matrix(self):
        self.M = self.__global_mass()

    def _build_medium_matrices(self):
        """
        From updated anelastic properties
        we create matrices to incorporate the medium properties
        into the stiffness matrix.
        """
        # build medium matrices
        self.Cmatrix_block = []
        self.Fmatrix_block = []

        for k in np.arange(self.n + 1):
            # C matrix
            Ck_node = np.repeat(self.C[k::self.n + 1], (self.n + 1))
            self.Idiag.data = Ck_node
            Cmatrixnode = self.Kpattern.dot(self.Idiag)
            self.Cmatrix_block.append(Cmatrixnode)
            # F matrix
            Fk_node = np.repeat(self.F[k::self.n + 1], (self.n + 1))
            self.Idiag.data = Fk_node
            Fmatrixnode = self.Kpattern.dot(self.Idiag)
            self.Fmatrix_block.append(Fmatrixnode)

        # Additional AN matrix for stiffness matrix Kl0
        self.Kmatrix_ANmed = self.Idiag.copy()
        self.Kmatrix_ANmed.data = self.A - self.N

        return

    def global_matrices(self, l=0):
        """
        This function combines pre-computed stiffness matrices and assembles
        the stiffness and mass matrices for a given angular order l

        :type l: int
        :param l: angular order (zero for radial)
        :returns: assembled sparse Mass matrix,
                  assembled sparse stiffness matrix
        :rtype: Tuple(scipy.sparse.lil_matrix, scipy.sparse.lil_matrix)
        """
        # build full stiffness matrix
        K = self.stiffK_AN.dot(self.Kmatrix_ANmed)
        for k in np.arange(self.n + 1):
            K += self.stiffK_F[k].multiply(self.Fmatrix_block[k])
            K += self.stiffK_C[k].multiply(self.Cmatrix_block[k])

        # build mass matrix
        M_ass = self.gather.dot(self.M).dot(self.scatter)

        # add gravity into stiffness matrix and assemble
        K_ass = self.gather.dot(K + self.G).dot(self.scatter)

        return M_ass, K_ass

    def _epsilon(self, U, omega, attenuation=True):
        """
        compute energy partitioning error as suggested by Takeuchi & Saito 1972

        with an additional factor 2, this gives the relative error in omega,
        see also Zabranova et all 2017

        Compute everything at polynomial order n+1, as otherwise the error
        estimate is zero, because the discrete solutions fulfill the discrete
        rayleigh quotient.
        """
        jop = self.jacobian_vec_np1[np.newaxis, :]
        jopwop = self.jacobian_vec_np1 * self.weights_vec_np1
        derop = self.l_prime_mat_np1

        rho = self.P_n_to_np1.dot(self.rho)
        if attenuation:
            factorAC = self.P_n_to_np1.dot(self.factorAC)
            factorLN = self.P_n_to_np1.dot(self.factorLN)
            factorF = self.P_n_to_np1.dot(self.factorF)
            lnfrq = np.log(omega / self.wref)

            C = self.P_n_to_np1.dot(self.C0)
            A = self.P_n_to_np1.dot(self.A0)
            N = self.P_n_to_np1.dot(self.N0)
            F = self.P_n_to_np1.dot(self.F0)

            C = C[None, :] + factorAC[None, :] * lnfrq[:, None]
            A = A[None, :] + factorAC[None, :] * lnfrq[:, None]
            N = N[None, :] + factorLN[None, :] * lnfrq[:, None]
            F = F[None, :] + factorF[None, :] * lnfrq[:, None]
        else:
            C = self.P_n_to_np1.dot(self.C)
            A = self.P_n_to_np1.dot(self.A)
            N = self.P_n_to_np1.dot(self.N)
            F = self.P_n_to_np1.dot(self.F)

        r = self.P_n_to_np1.dot(self.r)
        gop = self.P_n_to_np1.dot(self.g_acc) if self.gravity else 0.

        U = self.P_n_to_np1.dot(U.T).T

        Udot = derop.dot(U.T).T / jop

        # D&T 8.126
        T = (rho * U ** 2 * r ** 2).dot(jopwop)

        # D&T 8.212 + 8.157
        VeVg = (C * (Udot * r) ** 2 + 4 * F * r * Udot * U +
                (4 * (A - N) - 4 * rho * gop * r) * U ** 2).dot(jopwop)

        epsilon = np.abs(VeVg / (omega ** 2 * T) / 2 - 0.5)

        return epsilon

#
# Gravity, Mass & Stiffness Matrix declarations
#
    def __global_mass(self):
        """
        build up unassembled global mass matrix
        see documentation
        """
        n = self.n
        Ne = self.Ne
        rho = self.rho
        r = self.r
        jac_ind = self.jacobian_indices

        # init matrix
        nldof = Ne * (n + 1)
        M = lil_matrix((nldof, nldof))

        # loop over elements
        for j in jac_ind:
            M[j[1]:j[2], j[1]:j[2]] = \
                self.__elemental_mass(rho[j[1]:j[2]], r[j[1]:j[2]], j[0], n)

        # return in compressed sparse row format
        return M.tocsr()

    def __elemental_mass(self, rho, r, J, n):
        """
        compute elemental mass matrix for a single element
        :type rho: float array
        :param rho: density for each point within element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        """
        weights = self.weights
        Ml = lil_matrix((n + 1, n + 1))

        for k in np.arange(n + 1):
            Ml[k, k] += weights[k] * rho[k] * r[k] ** 2 * J[k]

        # return in compressed sparse row format
        return Ml.tocsr()

    def __global_gravity(self):
        """
        build up unassembled global gravity matrix
        see documentation
        """
        n = self.n
        Ne = self.Ne
        rho = self.rho
        r = self.r
        g_acc = self.g_acc
        jac_ind = self.jacobian_indices

        # matrix dimensions
        nldof = Ne * (n + 1)
        G = lil_matrix((nldof, nldof))

        # loop over elements
        for j in jac_ind:
            G[j[1]:j[2],
              j[1]:j[2]] = self.__elemental_gravity(rho[j[1]:j[2]],
                                                    g_acc[j[1]:j[2]],
                                                    r[j[1]:j[2]],
                                                    j[0],
                                                    n)

        # return in compressed sparse row format
        return G.tocsr()

    def __elemental_gravity(self, rho, g0, r, J, n):
        """
        Elemental gravity matrix
        see documentation
        :type rho: float array
        :param rho: density for each point within element
        :type g0: float array
        :param g0: gravity data for each point within element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        """
        weights = self.weights

        Gl = lil_matrix((n + 1, n + 1))

        for k in np.arange(n + 1):
            Gl[k, k] -= 4. * g0[k] * weights[k] * rho[k] * J[k] * r[k]

        # return in compressed sparse row format
        return Gl.tocsr()

    def __global_stiffnessK(self):
        """
        build up unassembled K matrix (without medium information).
        """
        jac_ind = self.jacobian_indices
        n = self.n
        Ne = self.Ne
        r = self.r

        # setting up matrix (and dimensions)
        nldof = Ne * (n + 1)
        K_AN = lil_matrix((nldof, nldof))

        for j in jac_ind:
            K_AN[j[1]:j[2], j[1]:j[2]] = \
                self.__elemental_stiffness_AN(r[j[1]:j[2]], j[0], n)

        # create K_F and K_C matrices
        KC_list = []
        KF_list = []

        for k in np.arange(n + 1):
            KCtmp = lil_matrix((nldof, nldof))
            KFtmp = lil_matrix((nldof, nldof))
            for j in jac_ind:
                KCtmp[j[1]:j[2], j[1]:j[2]] = \
                    self.__elemental_stiffness_C(r[j[1]:j[2]], j[0], k, n)
                KFtmp[j[1]:j[2], j[1]:j[2]] = \
                    self.__elemental_stiffness_F(r[j[1]:j[2]], j[0], k, n)

            # Save matrix
            KC_list.append(KCtmp.tocsr())
            KF_list.append(KFtmp.tocsr())

        return K_AN.tocsr(), KC_list, KF_list

    def __elemental_stiffness_AN(self, r, J, n):
        """
        compute elemental K matrix for a single element
        (with medium information) see documentation
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type n: int
        :param n: polynomial order and elements
        """
        weights = self.weights

        # setting up matrix
        Kl = lil_matrix((n + 1, n + 1))

        for i in np.arange(n + 1):
            Kl[i, i] += 4. * weights[i] * J[i]

        return Kl.tocsr()

    def __elemental_stiffness_C(self, r, J, k, n):
        """
        compute elemental K matrix for a single element
        (with medium information) see documentation
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: int
        :param k: outter loop index
        :type n: int
        :param n: polynomial order and elements
        """
        weights = self.weights
        l_prime = self.l_prime

        Kl = lil_matrix((n + 1, n + 1))

        for i, j in np.ndindex(Kl.shape):
            Kl[j, i] += (r[k] ** 2 * weights[k] * l_prime[k, i] *
                         l_prime[k, j] / J[k])

        return Kl.tocsr()

    def __elemental_stiffness_F(self, r, J, k, n):
        """
        compute elemental K matrix for a single element
        (with medium information) see documentation
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: int
        :param k: outter loop index
        :type n: int
        :param n: polynomial order and elements
        """
        weights = self.weights
        l_prime = self.l_prime
        Kl = np.zeros((n + 1, n + 1))

        for i, j in np.ndindex(Kl.shape):
            if (k == i):
                Kl[j, i] += 2. * weights[i] * l_prime[k, j] * r[i]

            if (k == j):
                Kl[j, i] += 2. * weights[j] * l_prime[k, i] * r[j]

        return Kl
