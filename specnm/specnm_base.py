#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A metaclass for toroidal and spheroidal mode generation by SpecNM.
Contains all the general solver related code,
reading of model file, mesh generation and gather scatter operators.

copyright::
    Martin van Driel (Martin@vanDriel.de), 2016
    Federico D. Munch (federico.munch@erdw.ethz.ch), 2016
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2018-2024

license::
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
# abstract meta class
from abc import ABC, abstractmethod
# sparse matrix setups
from scipy.sparse import csr_matrix, lil_matrix, identity, block_diag
from scipy.linalg import eig
from scipy.interpolate import interp1d
from scipy.optimize import root_scalar

# disable warnings if option -W default is not set
import sys
import warnings

# lagrange basis funcs, gll points & weights, order mappings etc
from .lagrange_basis_quadrature import lagrange_basis
from . import models_1D

# solver related imports
from .multishift_solver import multishift_slepc


if not sys.warnoptions:
    warnings.simplefilter('ignore', UserWarning)


class spc_base(ABC):
    """
    Base class of specnm.

    This class serves as the foundation for initializing data
    from 1D models, utilizing the models_1D module. It supports mode-specific
    initialization, ensuring the correct setup based on the type of mode
    encountered.

    Key functionalities include:

    - Reading and parsing 1D model files (e.g. deck, polynomial).
    - Building computational meshes.
    - Setting up the gather-scatter operator.
    - Initializing frequency and angular order independent matrices
      for further calculations.
    - Holding the meta information for eigenvalue solvers.

    This class provides the core structure and functionality for derived
    classes that operate on 1D models in the specnm framework.
    """
    def __init__(self, model='prem_iso', n=5, fmax=0.1, fmesh=None, fref=None,
                 gravity=True, verbose=0, mode='spheroidal', sph_type=0,
                 solid_region=-1, rmin=None, refine_center=True,
                 dispersion=False):
        """
        :type model: string
        :param model: 1D planet model
        :type n: integer
        :param n: order of polynomials
        :type fmax: float
        :param fmax: maximum frequency for mesh generation
        :type fmesh: float
        :param fmesh: overwrites the fmax parameter if set (default=None)
                      this is a legacy parameter as codes exist that use fmesh
        :type fref: float
        :param fref: reference frequency of the input model (default=None)
                     if this is None the reference frequency is read from the
                     model file and if that cannot be found it is set to 1Hz
        :type gravity: bool
        :param gravity: gravity on or off
        :type verbose: integer
        :param verbose: output info to console (1) or not (0), (2) is used for
                        seismogram plotting
        :type mode: string
        :param mode: either spheroidal, toroidal or radial
        :type sph_type: integer
        :param sph_type: type of spheroidal mode (1=radial, 2=cowl, 3=full)
        :type solid_region: integer
        :param solid_region: solid region for toroidal modes, defaults to the
            one closest to the surface
        :type rmin: float
        :param rmin: minimum radius in meters for the calculation, using stress
            free boundary conditions if > 0
        :type refine_center: bool
        :param refine_center: refining mesh at zero radius yes or no
        :type dispersion: bool
        :param dispersion: only calculate lowest 10 modes and mesh starts
                           at highest fluid interface (small error),
                           but much faster for dispersion curves
        """
        self.modelname = model
        self.n = n
        self.fmesh = fmax if fmesh is None else fmesh
        self.gravity = gravity
        self.verbose = verbose
        self.mode = mode
        self.sph_type = sph_type
        self.solid_region = solid_region
        self.rmin = rmin
        self.refine_center = refine_center
        self.dispersion = dispersion
        self.GRAVITY_G = models_1D.GRAVITY_G

        if (dispersion and sph_type > 2):
            raise RuntimeError('Dispersion is made for simple spheroidal runs.'
                               ' Hence it only works properly with '
                               'rayleigh (without full gravity) '
                               'and love class.')

        if (verbose == 1):
            print('SPECtral element Normal Mode code!')
            print('By Johannes Kemper, Federico Munch and Martin van Driel')
            print('----------------------------------')

        # load model file and build mesh based on maximum frequency
        self._build_structure_and_load_model(mode, fref)

        # build mass matrix and pre-compute
        # stiffness matrix (without medium properties)
        self._precompute_stiffness_mass_matrices()

        if (verbose):
            messages = {'spheroidal': 'Spheroidal mode calculation',
                        'toroidal': 'Toroidal mode calculation.',
                        'radial': 'Radial mode calculation.'}
            if (mode in messages):
                if (verbose < 2):
                    print(messages[mode])
                if (mode in ['spheroidal', 'radial']):
                    print(f'Gravity: {gravity}')
            else:
                raise NotImplementedError(f'{mode} not implemented.')

            print('Mesh generation, precalculations and '
                  'assembly of matrices done.')

    def _eigenvalue_problem_init(self, attenuation_mode):
        """
        Setup the eigenvalue problem depending on attenuation.
        With attenuation the eigenvalue problem is nonlinear.
        In addition precalculate the projection matrices for spurious filter.

        :type attenuation_mode: string
        :param attenuation_mode: attenuation mode for calculation
        """
        # check for equivalent modes and map to existing vocabulary
        replacements = {'no': 'elastic',
                        'full': 'eigenvector continuation stepwise'}
        if attenuation_mode in replacements:
            attenuation_mode = replacements[attenuation_mode]

        # initialize attenuation by model if none is given
        if (attenuation_mode is None):
            if (not self.model_attenuation):
                attenuation_mode = 'elastic'
            else:
                if (self.dispersion):
                    attenuation_mode = 'first order'
                else:
                    attenuation_mode = 'eigenvector continuation stepwise'

        if (not attenuation_mode == 'elastic' and not self.model_attenuation):
            raise ValueError('Model does not allow for attenuation, but '
                             'attenuation was specified in problem.')

        attenuation_dct = {
            'elastic':
            self.__eigen_problem_noat,
            'first order':
            self.__eigen_problem_noat,
            'eigenvector continuation':
            self.__eigen_problem_at_evcont,
            'eigenvector continuation stepwise':
            self.__eigen_problem_at_evcont_step}

        if (attenuation_mode in attenuation_dct):
            self._eigenvalue_problem = attenuation_dct[attenuation_mode]
        else:
            raise NotImplementedError(
                f'Attenuation type:{attenuation_mode} not implemented.')

        return

    def __eigen_problem_noat(self, l, fmin, fmax, **multishift_kwargs):
        """
        Compute eigenfunctions, eigenvalues for a given angular order
        without attenuation (noat), meaning fully elastic

        :type l: integer
        :param l: angular order
        :type fmin: float
        :param fmin: lower boundary for eigenfrequencies
        :type fmax: float
        :param fmax: upper boundary for eigenfrequencies
        :type multishift_kwargs: dict
        :param multishift_kwargs: additional arguments for the multishift
            solver
        """
        # setting up matrices
        M_ass, K_ass = self.global_matrices(l)
        ef, ev = multishift_slepc(K_ass, M_ass, fmin, fmax,
                                  **multishift_kwargs)

        return ef, ev

    def __eigen_problem_at_evcont_step(self, l, fmin, fmax, n_per_step=10,
                                       max_f_jump=10., **multishift_kwargs):
        """
        Compute eigenfunctions, eigenvalues for a given angular order
        with attenuation based on eigenvector continuation

        :type l: integer
        :param l: angular order
        :type fmin: float
        :param fmin: lower boundary for eigenfrequencies
        :type fmax: float
        :param fmax: upper boundary for eigenfrequencies
        :type n_per_step: integer
        :param n_per_step: number of modes within each step of interpolation
                          for attenuation solution based on
                          eigenvector continuation and log interpolation
                          (default=10)
        :type max_f_jump: float
        :param max_f_jump: maximum frequency in multiple of ref frequency
                           in each bracket
        :type multishift_kwargs: dict
        :param multishift_kwargs: additional arguments for the multishift
            solver
        """
        # function used internally for logarithmic interpolation
        def logfun(x):
            return finterp(np.log(x)) - x

        # abuse multishift_kwargs to get this optional parameter in
        debug_figure = multishift_kwargs.pop('debug_figure', False)

        # for dispersion need n_total defined catch here
        if self.dispersion:
            n_per_step = multishift_kwargs.pop('n_total', 10)

        # left and right eigensolutions bracketing the eigenfrequencies
        fsml = []  # left medium frequency
        efsl = []  # left eigenfrequency
        evsl = []  # left eigenvectors
        fsmr = []  # right medium frequency
        efsr = []  # right eigenfrequency
        evsr = []  # right eigenvectors

        # solve for the left solutions of the first block
        # first solve is special, as we only need the left solutions at fmin
        self._update_physical_properties(2 * np.pi * fmin)
        M, K = self.global_matrices(l)
        ef, ev = multishift_slepc(K, M, fmin, n_total=n_per_step,
                                  **multishift_kwargs)

        # solve again with min(ef) for the physical properties to avoid the
        # first block having a distant left solution
        # adding some tolerance, as we only have ef.min() to numerical accuracy
        # otherwise we may be missing the first eigenvalue
        eps_fmin = 1e-6
        _fmin = max(fmin, ef.min() * (1 - eps_fmin))

        self._update_physical_properties(2 * np.pi * _fmin)
        M, K = self.global_matrices(l)
        ef, ev = multishift_slepc(K, M, _fmin, n_total=n_per_step,
                                  **multishift_kwargs)

        # normalize
        ev /= M.dot(ev.T ** 2).sum(axis=0)[:, None] ** 0.5

        fsml.append(_fmin * np.ones_like(ef))
        efsl.append(ef)
        evsl.append(ev)

        _fref = ef[-1]
        while True:
            self._update_physical_properties(2 * np.pi * _fref)
            M, K = self.global_matrices(l)
            ef, ev = multishift_slepc(K, M, _fmin,
                                      n_total=n_per_step * 2,
                                      **multishift_kwargs)

            # normalize
            ev /= M.dot(ev.T ** 2).sum(axis=0)[:, None] ** 0.5

            # identify right solutions corresponding to left side
            cc = ev.dot(M.dot(evsl[-1].T))

            if np.any(np.max(np.abs(cc), axis=0) < 0.3):
                # we arrive here, if we are missing a mode in the left side,
                # likely this happens if the krylov solver missed out one mode
                # on the previous iteration of the while loop.
                raise RuntimeError(
                    'Missed one eigensolution of the left side solution.'
                    ' Try with higher mesh frequency.')

            idx = np.argmax(np.abs(cc), axis=0)

            if not np.all(np.unique(idx) == sorted(idx)):
                # mostly arriving here, if we miss the first eigenvalue due to
                # too tight _fmin
                raise RuntimeError('Possibly too tight fmin.')

            # remaining ones are the next block left side
            maskl = np.ones(2 * n_per_step, dtype=bool)
            maskl[idx] = False

            # if block boundary eigenvalues are crossing, they appear a third
            # time in the spectrum. Filtering them out here.
            if len(evsr) > 0:
                cc2 = ev[maskl].dot(M.dot(evsr[-1].T))
                maskl[maskl] *= np.max(np.abs(cc2), axis=1) < 0.65

            # make sure we use at max n_per_step eigenvalues for the new left
            # side
            if maskl.sum() == 0:
                raise RuntimeError('WTF? Somehow found no eigenpairs.')
            elif maskl.sum() > n_per_step:
                # if we filter out big jumps, we also have
                # more modes left over for
                # the left side, which we want to avoid.
                mml = np.ones(maskl.sum(), dtype=bool)
                mml[n_per_step:] = False
                maskl[maskl] = mml

            # store right solutions and corresponding frequencies
            fsmr.append(_fref * np.ones_like(ef)[idx])
            efsr.append(ef[idx])
            evsr.append(ev[idx])

            # check if we reached maximum frequency
            _ftest = ef[maskl][0]
            if _ftest > fmax:
                break

            # keep modes with a maximum factor max_f_jump above the reference
            # frequency only
            maskl2 = maskl.copy()
            maskl[maskl] *= ef[maskl] < _fref * max_f_jump
            if maskl.sum() == 0:
                # if this killed all new left modes, need to restart with a new
                # set, similar to preloop
                if self.verbose:
                    print('BIG JUMP')

                _fmin = min(max(fmin, ef[maskl2].min() * (1 - eps_fmin)),
                            _fref * max_f_jump)

                self._update_physical_properties(2 * np.pi * _fmin)
                M, K = self.global_matrices(l)
                ef, ev = multishift_slepc(K, M, _fmin, n_total=n_per_step,
                                          **multishift_kwargs)

                # normalize
                ev /= M.dot(ev.T ** 2).sum(axis=0)[:, None] ** 0.5

                fsml.append(_fmin * np.ones_like(ef))
                efsl.append(ef)
                evsl.append(ev)

                _fref = ef[-1]

            else:
                # store left solutions and corresponding frequencies
                fsml.append(_fref * np.ones_like(ef[maskl]))
                efsl.append(ef[maskl])
                evsl.append(ev[maskl])

                _fmin = max(fmin, _ftest * (1 - eps_fmin))
                _fref = ef[maskl][-1]

        # collect everything in arrays
        evsl = np.concatenate(evsl)
        evsr = np.concatenate(evsr)

        fsml = np.concatenate(fsml)
        fsmr = np.concatenate(fsmr)

        efsl = np.concatenate(efsl)
        efsr = np.concatenate(efsr)

        f1 = np.c_[fsml, fsmr].T
        f2 = np.c_[efsl, efsr].T

        # find log linear interpolator. Seems like there is no closed form
        # analytical solution, so we do it numerically
        flog = []
        for i in range(len(fsml)):
            # log linear interpolation
            finterp = interp1d(np.log(f1[:, i]), f2[:, i], kind='linear',
                               fill_value='extrapolate')

            sol = root_scalar(logfun, method='brentq',
                              bracket=(f2[0, i],
                                       2 * f2[1, i] - f2[0, i]))
            # if this raises with the bracket error, than we likely missed a
            # mode perviously and got a wrong association
            # also happens if there are big gaps in the spectrum, e.g. when
            # jumping from undertones to elastic modes. Can be avoided using a
            # better estimate for fmin or lower max_f_jump
            fi = sol.root
            flog.append(fi)

        flog = np.array(flog)

        ef3 = np.zeros_like(efsl)
        ev3 = np.zeros_like(evsl)

        for i in range(len(fsml)):
            # make sure the vectors are parallel, not antiparallel
            evsr[i] *= np.sign(np.dot(evsr[i], M.dot(evsl[i])))

            vij = np.c_[evsl[i], evsr[i]]

            # update physical properties at estimated frequency
            self._update_physical_properties(2 * np.pi * flog[i])
            M, K = self.global_matrices(l)

            # if vectors are identical, i.e. independent of frequency, the
            # continuation becomes numerically unstable and we may as well just
            # use the rayleigh quotient
            # I think these are the spurious modes anyway, so this may be not
            # so relevant, but it should also be a robust solution
            if 1 - np.dot(evsr[i], M.dot(evsl[i])) < 1e-12:
                v3 = evsr[i]
                w = np.dot(v3, K.dot(v3)) / np.dot(v3, M.dot(v3))
                ef3[i] = w ** 0.5 / 2 / np.pi
                ev3[i] = v3
                continue

            # matrix for reduced basis eigenvalue problem
            N = np.dot(vij.T, M.dot(vij))
            H = np.dot(vij.T, K.dot(vij))

            # make sure H is symmetric (may not be due to floating round off)
            H = 0.5 * (H + H.T)

            # solve eigenvalue problem in reduced basis. Somehow eigh is
            # unstable, although H is definitely symmetric
            w, vl = eig(H, N)
            w = w.real

            # create and normalize new eigenvector in full basis
            v3 = np.dot(vij, vl)
            v3 /= M.dot(v3 ** 2).sum(axis=0)[None, :] ** 0.5

            # choose eigenvalue based on the dot product. The other eigenvalues
            # supposedly are spurious
            idx = np.argmax(np.abs(np.prod(np.dot(vij.T, M.dot(v3)), axis=0)))

            ef3[i] = w[idx] ** 0.5 / 2 / np.pi
            ev3[i] = v3[:, idx]
            ev3[i] *= np.sign(np.dot(ev3[i], M.dot(evsl[i])))

        # make sure solutions are sorted by frequency
        idx = np.argsort(ef3)
        ef3 = ef3[idx]
        ev3 = ev3[idx]

        # filter for max frequency
        mask = ef3 < fmax
        ef3 = ef3[mask]
        ev3 = ev3[mask]

        if debug_figure:
            import matplotlib.pyplot as plt

            cc = ev3.dot(M.dot(evsr.T))
            plt.imshow(np.abs(cc))
            plt.colorbar()

            flog = flog[idx][mask]

            gridspec_kw = {'height_ratios': [4, 1]}
            fig, (ax1, ax2) = plt.subplots(2, 1, gridspec_kw=gridspec_kw,
                                           sharex=True)

            ax1.semilogx(fsml[mask], efsl[mask] * 1e3, ls='none', marker='o',
                         zorder=0, color='C0', label=r'$\omega_l$')
            ax1.semilogx(fsmr[mask], efsr[mask] * 1e3, ls='none', marker='o',
                         zorder=0, color='C1', label=r'$\omega_r$')
            ax1.semilogx(flog, flog * 1e3, ls='none', marker='x', color='C2',
                         zorder=10, label='log linear')
            ax1.semilogx(ef3, ef3 * 1e3, ls='none', marker='o', color='C3',
                         zorder=0, label='ev continuation')
            ff = np.geomspace(fmin, fmax, 1000)
            ax1.semilogx(ff, ff * 1e3, color='k',
                         label=r'$\mathrm{f=f_{ref}}$')

            ax2.loglog(ef3, np.abs(ef3 - flog) / ef3, ls='none', marker='o',
                       label='diff')

        if self.dispersion:
            ef3 = ef3[:n_per_step]
            ev3 = ev3[:n_per_step]

        return ef3, ev3

    def __eigen_problem_at_evcont(self, l, fmin, fmax,
                                  number_of_eigenvectors=2,
                                  **multishift_kwargs):
        """
        Compute eigenfunctions, eigenvalues for a given angular order
        with attenuation based on eigenvector continuation

        :type l: integer
        :param l: angular order
        :type fmin: float
        :param fmin: lower boundary for eigenfrequencies
        :type fmax: float
        :param fmax: upper boundary for eigenfrequencies
        :type number_of_eigenvectors: integer
        :param number_of_eigenvectors: number of eigenvectors to use
                                       in eigenvector continuation to estimate
                                       solution with attenuation
        :type multishift_kwargs: dict
        :param multishift_kwargs: additional arguments for the multishift
            solver
        """
        # for dispersion need n_total defined catch here
        if self.dispersion:
            _ = multishift_kwargs.setdefault('n_total', 10)

        # use a log spacing of fiducial frequencies
        frequencies = np.geomspace(fmin, fmax, number_of_eigenvectors)

        efs = []
        evs = []

        # solve eigenvalue problem at fiducial frequencies
        for f in frequencies:
            self._update_physical_properties(2 * np.pi * f)
            M, K = self.global_matrices(l)
            ef, ev = multishift_slepc(K, M, fmin, fmax, **multishift_kwargs)

            # normalize
            ev /= M.dot(ev.T ** 2).sum(axis=0)[:, None] ** 0.5

            efs.append(ef)
            evs.append(ev)

        nf = len(efs[0])

        ef3 = np.zeros_like(efs[0])
        ev3 = np.zeros_like(evs[0])

        efs = np.array(efs)
        evs = np.array(evs)

        # interpolation scheme for eigenfrequencies depending on number of
        # samples
        if number_of_eigenvectors == 2:
            kind = 'linear'
        elif number_of_eigenvectors == 3:
            kind = 'quadratic'
        else:
            kind = 'cubic'

        for i in range(nf):
            vij = evs[:, i, :].T

            # we have sampled the frequency of the mode fm at n frequencies
            # and want to find the frequency at which the interpolated value
            # matches it's argument:
            #   fm(f) = f
            # instead of iterating, we use root finding here
            finterp = interp1d(np.log(frequencies), efs[:, i], kind=kind)

            def fun(x):
                return finterp(np.log(x)) - x

            sol = root_scalar(fun, method='brentq',
                              bracket=(efs[:, i].min(), efs[:, i].max()))
            fi = sol.root

            # update physical properties at estimated frequency
            self._update_physical_properties(2 * np.pi * fi)
            M, K = self.global_matrices(l)

            # matrix for reduced basis eigenvalue problem
            N = np.dot(vij.T, M.dot(vij))
            H = np.dot(vij.T, K.dot(vij))

            # make sure H is symmetric (may not be due to floating round off)
            H = 0.5 * (H + H.T)

            # solve eigenvalue problem in reduced basis. Somehow eigh is
            # unstable, although H is definitely symmetric
            w, vl = eig(H, N)
            w = w.real
            vl = vl.real

            # create and normalize new eigenvector in full basis
            v3 = np.dot(vij, vl).T
            v3 /= np.linalg.norm(v3, axis=-1)[:, np.newaxis]

            # choose eigenvalue based on the dot product. The other eigenvalues
            # supposedly are spurious
            idx = np.argmax(np.abs(np.prod(np.dot(v3, vij), axis=1)))

            ef3[i] = w[idx] ** 0.5 / 2 / np.pi
            ev3[i] = v3[idx]

        # make sure solutions are sorted by frequency
        idx = np.argsort(ef3)
        ef3 = ef3[idx]
        ev3 = ev3[idx]

        return ef3, ev3

    def _gather_scatter_operator(self, felem, nfields=1, decouple=0):
        """
        Gather and scatter operators for matrices
        compute the gather and scatter operators as defined by
        Nissen-Meyer et al 2007, eq 20.
        Note the slightly different normalization, such that
        scatter * gather = identity(ngdof)

        :type felem: bool array
        :param felem: array indicating if element is fluid
        :type nfields: integer
        :param nfields: number of fields concidered for operators
        :type decouple: integer
        :param decouple: field to decouple (should be within [0, 1, 2])
        """
        n = self.n
        Ne = self.Ne
        idx_add = 0
        ffelements = self.ffelements

        # setting up matrix dimensions
        endof = nfields * (n + 1)
        # added fluid boundary degrees of freedom needed if there is a fluid
        # and the mode is not radial
        if self.mode == 'radial':
            ngdof = nfields * (Ne * n + 1)
        else:
            ngdof = nfields * (Ne * n + 1) +\
                np.size(self.fluid_boundaries) * int(self.fluid) +\
                np.size(ffelements) * int(self.fluid)
        nldof = Ne * endof

        # setting up matrices
        gather = lil_matrix((ngdof, nldof))
        gelem = identity(endof)

        # building up the gather operator
        for e in np.arange(Ne):
            idxgb = e * endof - nfields * e + idx_add
            idxge = (e + 1) * endof - nfields * e + idx_add
            idxlb = e * endof
            idxle = (e + 1) * endof
            gather[idxgb:idxge, idxlb:idxle] = gelem

            if (e > 0 and ((felem[e] and not felem[e - 1] or  # inner
                           not felem[e] and felem[e - 1]) or  # outer
                           e in ffelements) and  # fluid-fluid
                    not self.mode == 'radial'):
                # if fluid is at the outer most boundary
                # we get a dimension mismatch for decouple unequal
                # to zero without this condition
                if (e == Ne - 1):
                    dc = 0
                else:
                    dc = decouple

                # copy the appropriate values one dof lower
                gather[idxgb + decouple + 1:idxge + decouple + 1,
                       idxlb - nfields:idxle] =\
                    gather[idxgb + decouple:idxge + dc,
                           idxlb - nfields:idxle]
                # setting the appropriate dof to zero to uncouple V
                gather[idxgb + decouple, idxlb + decouple] = 0
                gather[idxgb + decouple + 1, idxlb - nfields + decouple] = 0

                # adding one row index to get one more local dof
                idx_add += 1

        scatter = gather.T

        # set to compressed row format
        gather = csr_matrix(gather)
        scatter = csr_matrix(scatter)

        return gather, scatter

    def _build_structure_and_load_model(self, mode, fref=None):
        """
        - Reads velocity model (indicated by self.modelname variable)
        - Creates 1D mesh
        - Loads physical properties (incl. gravity acceleration)
          for mesh points

        :type mode: string
        :param mode: type of input mode (radial, toroidal, spheroidal)
        :type fref: float
        :param fref: reference frequency of the readin model
        """
        def get_element_centroid(r, n):
            """
            Get center of an element and repeat value throughout element

            :type r: ndarray (float)
            :param r: element grid
            :type n: integer
            :param n: element polynomial order (element length - 1)
            """
            rc = (r[:-1] + r[1:]) / 2.
            rc = rc.repeat(n + 1)
            return rc

        def gll_points(mesh_points, gll_points, Ne, n):
            """
            Compute gll points in the physical domain
            Al-Attar & Tromp, 2014, eq. D103

            :type mesh_points: float array
            :param mesh_points: mesh points per element
            :type gll_points: float array
            :param gll_points: gll points
            :type Ne: integer
            :param Ne: number of elements
            :type n: integer
            :param n: polynomial order
            """
            r = np.zeros((Ne, n + 1))
            for e in np.arange(Ne):
                r[e, :] = mesh_points[e] + \
                  (mesh_points[e + 1] - mesh_points[e]) / 2 * (gll_points + 1)

            return r.flatten()

        # Read planet model
        m = models_1D.model.read(self.modelname)

        # Get reference frequencies from model file or given input parameter
        if fref is None:
            self.fref = m.fref
            self.wref = m.fref * 2. * np.pi
        else:
            self.fref = fref
            self.wref = fref * 2. * np.pi

        # Get mass and moment of inertia
        self.planet_mass_kg = m.get_mass()
        self.planet_moment_of_inertia =\
            m.get_moment_of_inertia() / (self.planet_mass_kg * m.scale ** 2)

        # Get radius from model
        self.radius = m.scale

        # Get fluid boundaries from model
        self.fluid_boundaries = m.get_solid_fluid_boundaries() * self.radius
        self.fluid_fluid_boundaries = m.get_fluid_fluid_boundaries() *\
            self.radius
        if self.rmin is not None:
            self.fluid_boundaries = self.fluid_boundaries[
                self.fluid_boundaries > self.rmin]
            self.fluid_fluid_boundaries = self.fluid_fluid_boundaries[
                self.fluid_fluid_boundaries > self.rmin]

        self.fluid = self.fluid_boundaries.size > 0

        # Get Gauss Lobatto Legendre quadrature points and weights
        self.la_obj = lagrange_basis(n=self.n + 1)
        self.gll_points = self.la_obj.points
        self.weights = self.la_obj.weights
        self.l_prime = self.la_obj.derivative_matrix()

        # Create 1D mesh for the solid part of the planet, based on
        # the maximum frequency requested by the user (fmax)
        # for toroidal modes fluids have no meaning
        # so we constrain the modes to one solid layer (default: upper)
        rmax = None
        if ((mode == 'toroidal' or self.dispersion) and self.fluid):
            solid_regions = m.solid_regions
            rmin = solid_regions[self.solid_region][0] * self.radius
            rmax = solid_regions[self.solid_region][1] * self.radius

            # setting fluid to false, since there is none within comp. domain
            self.fluid = False
        else:
            rmin = 0.

        if self.rmin is not None:
            if rmin > self.rmin:
                raise ValueError('rmin below the solid fluid boundary')
            else:
                rmin = self.rmin

        # save rmin and rmax for general use
        self.rmin = rmin
        self.rmax = rmax

        grid = m.get_radial_mesh(dominant_period=(1. / self.fmesh),
                                 rmin=rmin, rmax=rmax,
                                 elements_per_wavelength=2.)

        # refine first few elements
        if self.refine_center and rmin == 0.:
            addp = [(grid[1] + grid[0]) / 2,
                    (grid[1] + grid[0]) / 4,
                    (grid[2] + grid[1]) / 2]
            grid = np.r_[addp, grid]
            grid.sort()

        # get fluid related variables
        self.fluid_elements = m.get_is_fluid((grid[1:] + grid[:-1]) / 2.)
        self.fluid_indices = self.fluid_elements.repeat(self.n+1)
        if self.fluid:
            idxs = [np.where(np.isclose(grid, ffb))[0][0]
                    for ffb in m.get_fluid_fluid_boundaries()]
            self.ffelements = np.ravel(idxs)
        else:
            self.ffelements = []

        # Get number of elements
        self.Ne = np.size(grid) - 1

        # Compute grid including GLL points
        grid_r = gll_points(grid, self.la_obj.points, self.Ne, self.n)

        # Get coordinates of the center of the elements
        elem_centroid_r = get_element_centroid(grid, self.n)

        # Getting the scaled r values, scaled mesh, element size, mesh freq
        self.r = grid_r * self.radius
        self.mesh_points = grid * self.radius

        # Compute jacobian and relevant indices for later use
        jac_ind = [
            [(np.ones(self.n + 1) / 2 *
             (self.mesh_points[e + 1] - self.mesh_points[e])),
             e * (self.n + 1),
             (e + 1) * (self.n + 1)]
            for e in np.arange(self.Ne)]

        self.jacobian_indices = jac_ind

        # creating some full vector or matrix operations for weights, der, jac
        self.weights_vec = np.tile(self.weights, self.Ne)
        self.l_prime_mat = block_diag((self.l_prime,) * self.Ne)
        self.jacobian_vec = np.concatenate(np.array(jac_ind,
                                                    dtype=object)[:, 0])

        # related to upmapping to order plus one
        la_obj_np1 = lagrange_basis(n=self.n + 2)
        weights_np1 = la_obj_np1.weights
        l_prime_np1 = la_obj_np1.derivative_matrix()

        self.weights_vec_np1 = np.tile(weights_np1, self.Ne)
        self.l_prime_mat_np1 = block_diag((l_prime_np1,) * self.Ne)
        self.jacobian_vec_np1 = np.repeat(
            (self.mesh_points[1:] - self.mesh_points[:-1]) / 2, self.n + 2)

        self.r_np1 = self.radius * gll_points(grid, la_obj_np1.points, self.Ne,
                                              self.n + 1)

        gll_n_to_np1 = self.la_obj.get_gll_order_mapping(self.n + 2)
        self.P_n_to_np1 = block_diag((gll_n_to_np1,) * self.Ne)

        # Read model parameters
        # set them to transversely isotropic parameters (L, N, A, C, F)
        kwargs = {'radius': grid_r, 'element_centroids': elem_centroid_r}
        self.rho = m.get_elastic_parameter('RHO', **kwargs)
        self.L = m.get_elastic_parameter('L', **kwargs)
        self.N = m.get_elastic_parameter('N', **kwargs)
        self.A = m.get_elastic_parameter('A', **kwargs)
        self.C = m.get_elastic_parameter('C', **kwargs)
        self.F = m.get_elastic_parameter('F', **kwargs)
        self.vp = m.get_elastic_parameter('VP', **kwargs)
        self.vs = m.get_elastic_parameter('VS', **kwargs)
        self.vpv = m.get_elastic_parameter('VPV', **kwargs)
        self.vsv = m.get_elastic_parameter('VSV', **kwargs)
        self.vph = m.get_elastic_parameter('VPH', **kwargs)
        self.vsh = m.get_elastic_parameter('VSH', **kwargs)
        self.eta = m.get_elastic_parameter('ETA', **kwargs)
        self.kappa0 = m.get_elastic_parameter('KAPPA', **kwargs)
        self.mu0 = m.get_elastic_parameter('MU', **kwargs)

        # Save elastic properties at w0 as reference
        self.A0 = self.A.copy()
        self.C0 = self.C.copy()
        self.F0 = self.F.copy()
        self.N0 = self.N.copy()
        self.L0 = self.L.copy()

        # Get attenuation specific parameters from model
        self.model_attenuation = m.anelastic
        if (self.model_attenuation):
            self.QKAPPA = m.get_elastic_parameter('QKAPPA', **kwargs)
            self.QMU = m.get_elastic_parameter('QMU', **kwargs)

            # precompute parameters for attenuation shifting of material par.
            self.QMUinv = np.divide(1.0,
                                    self.QMU,
                                    out=np.zeros_like(self.QMU),
                                    where=self.QMU != 0.0)
            self.factorAC = 2. / np.pi *\
                (self.kappa0 / self.QKAPPA + 4. / 3. * self.mu0 * self.QMUinv)
            self.factorF = 2. / np.pi *\
                (self.kappa0 / self.QKAPPA - 2. / 3. * self.mu0 * self.QMUinv)
            self.factorLN = 2. / np.pi * self.mu0 * self.QMUinv
            self.factorT = 2. / np.pi * self.mu0 * self.QMUinv

        # Compute local gravity for each mesh point
        # salvus_mesher_lite uses linear interpolation on g, which is not
        # accurate enough at least for the displacement modes, so we increase
        # the sampling here to sth insane
        self.g_acc = m.get_gravity(
            grid_r, compute_ellipticity_kwargs={'nsamp_per_layer': 100})

        return

    def _normalize(self, F1, F2=None):
        """
        Calculates the norm of the eigenfunctions

        :type F1: float array
        :param F1: first eigenfunction
        :type F2: float array
        :param F2: second eigenfunction
        """
        if (F2 is None):
            F2 = 0.

        # localize variables
        rho = self.rho
        r = self.r
        wopjop = self.weights_vec * self.jacobian_vec

        normfact = ((F1 ** 2 + F2 ** 2) * rho * r ** 2).dot(wopjop) ** 0.5

        return normfact

    def _sign(self, x):
        """
        Signum function with 0 = 1

        :type x: type that can be bigger or smaller zero
        :param x: contains sth that has a sign
        """
        return 2 * (x >= 0) - 1

    def _cutoff_eigenfunctions(self, cutoff_depth_m):
        """
        Cutoff eigenfunctions at specific meter value and keep above

        :type cutoff_depth_m: float
        :param cutoff_depth_m: actual cutoff depth in meters
        """
        if (self.verbose):
            print("Using cutoff depth: " + str(cutoff_depth_m))

        cutoff_radius = self.radius - cutoff_depth_m

        if (cutoff_radius < self.r[0]):
            raise ValueError("Cutoff depth below innest mesh dof.")
        elif (cutoff_radius > self.r[-1]):
            raise ValueError("Cutoff depth higher than mesh radius.")

        ri = self.r
        jac_ind = self.jacobian_indices
        for e, j in reversed(list(enumerate(jac_ind))):
            if ((ri[j[1]] <= cutoff_radius) and
                    (cutoff_radius < ri[j[2] - 1])):
                cutoff_idx = j[1]
                ele_idx = e
                break

        return cutoff_idx, ele_idx

    @abstractmethod
    def global_matrices(self, ll):
        """
        assembly of the relevant matrices in subclasses
        """
        pass

    @abstractmethod
    def _update_physical_properties(self, freq_ref):
        """
        update of physical properties in subclasses
        """
        pass

    @abstractmethod
    def _precompute_stiffness_mass_matrices(self):
        """
        Precompute the stiffness and mass matrices
        fill them with values get weights, l_prime etc.
        """
        pass

    @abstractmethod
    def _precompute_mass_matrix(self):
        """
        Precompute the mass matrix only
        """
        pass

    @abstractmethod
    def _reset_physical_properties(self):
        """
        Resets material matrices to reference frequency ones.
        """
        pass
