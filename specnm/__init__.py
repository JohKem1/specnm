#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
specnm is a tool for the computation of gravito-elastic free oscillations or
normal modes of spherically symmetric bodies based on a
spectral element discretization of the underlying radial
ordinary differential equations as given in the journal publication.

:publication:
    |  https://doi.org/10.1093/gji/ggab476

:authors:
    |  Johannes Kemper
    |  Martin van Driel
    |  Federico Munch

:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
from .dispersion import SurfaceWaveDispersion
from .sensitivity import sensitivity
from .love import love
from .radial import radial
from .rayleigh import rayleigh
from .rayleigh_fg import rayleigh_fg
