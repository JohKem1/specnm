#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A class to compute interpolation by
Lagrange polynomial bases and their derivatives.

:copyright:
    | Martin van Driel (Martin@vanDriel.de), 2015 - 2019
    | Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2017 - 2024
:license:
    | GNU Lesser General Public License, Version 3
    | (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
from sympy import Symbol, integrate, diff, rootof, legendre
import inspect
import os
import pathlib
import pickle
import hashlib
from functools import wraps

frame = inspect.currentframe()
if frame is None:
    raise ValueError("Could not determine caller stack frame.")
CUR_DIR = str(pathlib.Path(inspect.getfile(frame)).parent.parent.absolute())
CACHE_DIR = os.path.join(CUR_DIR, "gll_order_mapping_cache")
os.makedirs(CACHE_DIR, exist_ok=True)


def memoize(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        # Create a unique cache key using the function name and arguments
        pick = pickle.dumps((func.__name__, args, kwargs))
        key = f"{hashlib.md5(pick).hexdigest()}.pkl"
        cache_path = os.path.join(CACHE_DIR, key)

        result = None

        # Check if the result is already cached
        if os.path.exists(cache_path):
            # Load cached result (use pickle for more flexibility)
            with open(cache_path, 'rb') as f:
                result = pickle.load(f)
        else:
            # Compute the result and cache it
            result = func(*args, **kwargs)
            with open(cache_path, 'wb') as f:
                pickle.dump(result, f)

        return result

    return wrapper


class lagrange_basis(object):
    def __init__(self, points=None, n=None):
        if ((points is None) and (n is None)):
            raise ValueError('No order or points specified for lagrange obj.')
        elif (points is None):
            points, weights =\
                gauss_lobatto_legendre_quadrature_points_weights(n)
        elif (n is None):
            n = len(points)
            weights =\
                gauss_lobatto_legendre_quadrature_points_weights(n)[1]

        self.n = n
        self.points = points
        self.weights = weights

        # calculate actual polynomial representation for grid points
        self._polynomials(points)

    def _polynomials(self, points):
        """
        Compute Lagrange basis polynomials

        :param points: interpolation points
        :type points: list of floats
        :returns: nothing
        :rtype: None
        """

        n = self.n
        x = Symbol('x')

        self.polys = [np.prod([(x - points[m]) / (points[j] - points[m])
                               for m in np.arange(n) if m != j])
                      for j in np.arange(n)]
        self.dpolys = [p.diff() for p in self.polys]

        return

    def derivative_matrix(self, points=None):
        """
        compute derivatives of Lagrange basis polynomials

        D(i,j) = d/dx l_j (x_i)

        :param points: interpolation points
        :type points: list of floats
        :returns: Derivative matrix
        :rtype: ndarray (float)
        """

        if points is None:
            points = self.points

        n = self.n
        x = Symbol('x')
        dpolys = self.dpolys

        derivative_matrix = [
            [dpolys[j].subs(x, points[i])
                for j in np.arange(n)] for i in np.arange(n)]

        return np.array(derivative_matrix).astype(float)

    def interpolation(self, values):
        """
        Perform lagrange interpolation
        with the already calculated basis polynomials

        :param values: function values at points
        :type values: list of floats
        :returns: function, derivative of function
        :rtype: (sympy object, sympy object)
        """
        # seems counterintuitive to localize first, but is faster
        polys = self.polys
        dpolys = self.dpolys

        f = np.dot(polys, values)
        df = np.dot(dpolys, values)

        return f, df

    def get_gll_order_mapping(self, n2):
        """
        Get gll order mapping operator from basis order to provided order

        :param n2: Polynomial order of new basis
        :type n2: integer
        :returns: Mapping matrix from order n to provided n2
        :rtype: ndarray (float)
        """
        n1 = self.n

        return _get_gll_order_mapping(n1, n2)


@memoize
def _get_gll_order_mapping(n1, n2):
    """
    Get the projection operator from internal order n to n2

    :type n2: integer:
    :param n2: order of gll quad 2
    :returns: Mapping matrix from order n to provided n2
    :rtype: ndarray (float)
    """
    x = Symbol('x')

    la_n1 = lagrange_basis(n=n1)
    poly1 = la_n1.polys

    la_n2 = lagrange_basis(n=n2)
    poly2 = la_n2.polys

    # see http://pwp.gatech.edu/ece-jrom/wp-content/
    #     uploads/sites/436/2017/07/12-notes-6250-f16.pdf
    # compute Gram matrix
    H = np.zeros((n2, n2))
    for i, j in np.ndindex(*H.shape):
        H[i, j] = integrate(poly2[i] * poly2[j], (x, -1, 1))
    H = np.linalg.inv(H)

    # compute dual basis
    poly2_dual = []
    for i in range(n2):
        poly = 0.
        for j in range(n2):
            poly += H[i, j] * poly2[j]
        poly2_dual.append(poly)

    # compute mapping of order n2 basis to dual basis n1
    mapping = np.zeros((n2, n1))
    for i, j in np.ndindex(*mapping.shape):
        mapping[i, j] = integrate(poly1[j] * poly2_dual[i],
                                  (x, -1, 1))

    return mapping


@memoize
def gauss_lobatto_legendre_quadrature_points_weights(n=5):
    """
    Compute Gauss-Lobatto-Legendre (GLL) quadrature weights and points [-1, 1]
    using the sympy symbolic package and analytical definitions of these points

    :param n: number of integration points (order + 1)
    :type n: integer
    :returns: tuple of two numpy arrays of floats containing the points and
        weights
    :rtype: (ndarray (float), ndarray (float))
    """
    x = Symbol('x')

    # GLL points are defined as the roots of the derivative of the Legendre
    # Polynomials and include the boundaries
    Pn = legendre(n - 1, x)
    dPn = diff(Pn)

    # evaluate and sort
    pointsl = [rootof((1 - x ** 2) * dPn, i).evalf() for i in np.arange(n)]
    pointsl.sort()

    # weights
    weightsl =\
        [2. / (n * (n - 1)) if ((p == -1.) or (p == 1.)) else
         2. / (n * (n - 1) * Pn.subs(x, p) ** 2)
         for p in pointsl]

    return np.array(pointsl, dtype='float'), np.array(weightsl, dtype='float')
