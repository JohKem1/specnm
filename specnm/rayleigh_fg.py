#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A class to handle spheroidal eigenvalues,
eigenfunctions and dispersion curves
with full gravity.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2016
    Federico D. Munch (federico.munch@erdw.ethz.ch), 2016
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2019-2020
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np  # numerical python
from .specnm_base import spc_base  # god class
from .spheroidal_base import sph_base  # base class of spheroidal
from scipy.sparse import csr_matrix, lil_matrix, identity  # sparse matrices


class rayleigh_fg(sph_base, spc_base):
    def __init__(self, *args, **kwargs):
        # pass all arguments and keyword-argument tuples to base class
        spc_base.__init__(self, *args, **kwargs, mode='spheroidal', sph_type=3)

    def _precompute_stiffness_mass_matrices(self):
        """
        Precomputes the stiffness and mass matrices
        also gets norms, weights and lprime etc.
        and build the reference medium matrices
        """
        # Build gather and scatter operators
        # in contrast to toroidal modes, we have 2 different kind of operators
        # gather and scatter will act on the matrices.
        # gather_vec and scatter_vec will act on x.
        self.gather, self.scatter = \
            self._gather_scatter_operator(self.fluid_elements, 3, 1)
        self.gather_vec, self.scatter_vec = \
            self._gather_scatter_operator(self.fluid_elements)

        # set up diagonal matrix needed later
        self.Idiag = identity(3 * self.Ne * (self.n + 1), format='csr')

        # build mass matrix
        self.M = self.__global_mass()

        # build components of the stiffness matrix (K, L and R).
        [stiffKs,
         stiffKlp1,
         self.stiffK_C,
         self.stiffK_F,
         self.stiffK_L,
         self.stiffK_AN,
         self.stiffK_N] = \
            self.__global_stiffnessK()

        [self.stiffL_F,
         self.stiffL_L,
         self.stiffL_AN] = \
            self.__global_stiffnessL()

        [stiffRs,
         self.stiffR_A,
         self.stiffR_L] = \
            self.__global_stiffnessR()

        # some presummation
        self.stiffKs = np.sum(stiffKs)
        self.stiffKlp1 = np.sum(stiffKlp1)
        self.stiffLs = csr_matrix(self.stiffKs.shape)
        self.stiffRs = np.sum(stiffRs)

        # build the gravity matrix G
        self.G = self.__global_gravity()
        self.G_k = self.__global_gravity_k()

        # build medium matrices for linear solver with reference frequency
        self._build_medium_matrices()

    def _precompute_mass_matrix(self):
        self.M = self.__global_mass()

    def _build_medium_matrices(self):
        """
        From updated anelastic properties (A, L, N, F, C)
        we create matrices and incorporate the medium properties
        into the stiffness matrix.
        """
        # list of matrices to incorporate medium properties into
        # stiffness matrix
        Cmatrix_diag = []
        Fmatrix_diag = []
        Lmatrix_diag = []
        Nmatrix_diag = []
        Amatrix_diag = []
        ANmatrix_diag = []

        for k in np.arange(self.n + 1):
            Ck_node = np.repeat(self.C[k::self.n + 1], 3 * (self.n + 1))
            Fk_node = np.repeat(self.F[k::self.n + 1], 3 * (self.n + 1))
            Lk_node = np.repeat(self.L[k::self.n + 1], 3 * (self.n + 1))
            Nk_node = np.repeat(self.N[k::self.n + 1], 3 * (self.n + 1))
            Ak_node = np.repeat(self.A[k::self.n + 1], 3 * (self.n + 1))
            ANk_node = np.repeat(self.A[k::self.n + 1] - self.N[k::self.n + 1],
                                 3 * (self.n + 1))

            # append medium matrices (for node index k) to lists
            self.Idiag.data = Ck_node
            Cmatrix_diag.append(self.Idiag.copy())
            self.Idiag.data = Fk_node
            Fmatrix_diag.append(self.Idiag.copy())
            self.Idiag.data = Lk_node
            Lmatrix_diag.append(self.Idiag.copy())
            self.Idiag.data = Nk_node
            Nmatrix_diag.append(self.Idiag.copy())
            self.Idiag.data = Ak_node
            Amatrix_diag.append(self.Idiag.copy())
            self.Idiag.data = ANk_node
            ANmatrix_diag.append(self.Idiag.copy())

        # summing all matrices in one list comprehension
        stiffK = self.stiffKs.copy()
        stiffL = self.stiffLs.copy()
        stiffR = self.stiffRs.copy()

        [stiffK, stiffL, stiffR] = [stiffK, stiffL, stiffR] +\
            np.sum([[KC.dot(Cdiag) +
                     KF.dot(Fdiag) +
                     KL.dot(Ldiag) +
                     KN.dot(Ndiag) +
                     KAN.dot(ANdiag),
                     LF.dot(Fdiag) +
                     LL.dot(Ldiag) +
                     LAN.dot(ANdiag),
                     RA.dot(Adiag) +
                     RL.dot(Ldiag)]
                    for KC, Cdiag,
                    KF, Fdiag,
                    KL, Ldiag,
                    KN, Ndiag,
                    KAN, ANdiag,
                    LF,
                    LL,
                    LAN,
                    RA, Adiag,
                    RL in
                    zip(self.stiffK_C, Cmatrix_diag,
                        self.stiffK_F, Fmatrix_diag,
                        self.stiffK_L, Lmatrix_diag,
                        self.stiffK_N, Nmatrix_diag,
                        self.stiffK_AN, ANmatrix_diag,
                        self.stiffL_F,
                        self.stiffL_L,
                        self.stiffL_AN,
                        self.stiffR_A, Amatrix_diag,
                        self.stiffR_L)], axis=0)

        self.stiffK = stiffK
        self.stiffL = stiffL
        self.stiffR = stiffR

    def global_matrices(self, l):
        """
        This function combines pre-computed stiffness matrices and assembles
        the stiffness and mass matrices for a given angular order l

        :type l: integer
        :param l: angular order
        :returns: assembled sparse Mass matrix,
                  assembled sparse stiffness matrix
        :rtype: Tuple(scipy.sparse.lil_matrix, scipy.sparse.lil_matrix)
        """
        kn = (l * (l + 1)) ** 0.5

        # assemble the stiffness matrix
        # ( K + Klp1 (l+1) + Klm1 (l-1) + L k + R k^2 )
        K = self.stiffK + self.stiffKlp1 * (l + 1)\
            + self.stiffL * kn + self.stiffR * kn ** 2

        # assemble and add gravity matrix
        K += self.G + self.G_k * kn

        # globalize the matrices
        K_ass = self.gather.dot(K).dot(self.scatter)
        M_ass = self.gather.dot(self.M).dot(self.scatter)

        return M_ass, K_ass

#
# Gravity, Mass & Stiffness Matrix declarations
#
    def __global_mass(self):
        """
        build up unassembled global mass matrix
        see documentation
        """
        n = self.n
        Ne = self.Ne
        rho = self.rho
        r = self.r
        jac_ind = self.jacobian_indices

        # init matrix
        nldof = Ne * (n + 1)
        M1 = lil_matrix((nldof, nldof))

        # loop over elements
        for j in jac_ind:
            M1[j[1]:j[2], j[1]:j[2]] = self.__elemental_mass(
                rho[j[1]:j[2]], r[j[1]:j[2]], j[0], n)

        M = lil_matrix((3 * nldof, 3 * nldof))
        M[:: 3, :: 3] = M1
        M[1:: 3, 1:: 3] = M1

        # return in compressed sparse row format
        return M.tocsr()

    def __elemental_mass(self, rho, r, J, n):
        """
        compute elemental mass matrix for a single element
        :type rho: float array
        :param rho: density for each point within element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights

        Ml = lil_matrix((n + 1, n + 1))
        for k in np.arange(n + 1):
            Ml[k, k] += rho[k] * r[k] ** 2 * weights[k] * J[k]

        # return in compressed sparse row format
        return Ml.tocsr()

    def __global_gravity(self):
        """
        build up unassembled global gravity matrix
        see documentation
        """
        n = self.n
        Ne = self.Ne
        rho = self.rho
        r = self.r
        g_acc = self.g_acc
        jac_ind = self.jacobian_indices

        # matrix init - dimensions and G
        nldof = 3 * Ne * (n + 1)
        G = lil_matrix((nldof, nldof))

        # loop over elements
        for j in jac_ind:
            G[j[1] * 3:j[2] * 3,
              j[1] * 3:j[2] * 3] = self.__elemental_gravity(rho[j[1]:j[2]],
                                                            g_acc[j[1]:j[2]],
                                                            r[j[1]:j[2]],
                                                            j[0],
                                                            n)

        # return in compressed sparse row format
        return G.tocsr()

    def __elemental_gravity(self, rho, g0, r, J, n):
        """
        Elemental gravity matrix
        see documentation
        :type rho: float array
        :param rho: density for each point within element
        :type g0: float array
        :param g0: gravity data for each point within element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights
        l_prime = self.l_prime

        Gl = lil_matrix((3 * n + 3, 3 * n + 3))
        Guu = np.zeros((n + 1, n + 1))
        Gup = np.zeros((n + 1, n + 1))
        Gpu = np.zeros((n + 1, n + 1))

        for k in np.arange(n + 1):
            Guu[k, k] -= 4. * rho[k] * g0[k] * weights[k] * J[k] * r[k]
            Guu[k, k] += 4. * np.pi * self.GRAVITY_G * rho[k] ** 2 * \
                weights[k] * J[k] * r[k] ** 2

            for i in np.arange(n + 1):
                Gup[k, i] += rho[k] * weights[k] * l_prime[k, i] * r[k] ** 2
                Gpu[i, k] += rho[k] * weights[k] * l_prime[k, i] * r[k] ** 2

        # Build the final matrix
        # Gl = [ Guu Guv Gup; Gvu Gvv Gvp; Gpu Gpv Gpp ]
        Gl[:: 3, :: 3] = Guu
        Gl[:: 3, 2:: 3] = Gup
        Gl[2:: 3, :: 3] = Gpu

        # return in compressed sparse row format
        return Gl.tocsr()

    def __global_gravity_k(self):
        """
        build up unassembled global gravity matrix
        see documentation
        """
        n = self.n
        Ne = self.Ne
        rho = self.rho
        r = self.r
        g_acc = self.g_acc
        jac_ind = self.jacobian_indices
        # matrix dimensions
        nldof = 3 * Ne * (n + 1)

        G = lil_matrix((nldof, nldof))
        # loop over elements
        for j in jac_ind:
            G[j[1] * 3:j[2] * 3,
              j[1] * 3:j[2] * 3] =\
                self.__elemental_gravity_k(rho[j[1]:j[2]],
                                           g_acc[j[1]:j[2]],
                                           r[j[1]:j[2]],
                                           j[0],
                                           n)

        # return in compressed sparse row format
        return G.tocsr()

    def __elemental_gravity_k(self, rho, g0, r, J, n):
        """
        elemental gravity matrix
        see documentation
        :type rho: float array
        :param rho: density for each point within element
        :type g0: float array
        :param g0: gravity data for each point within element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights

        Gl = lil_matrix((3 * n + 3, 3 * n + 3))
        Guv = np.zeros((n + 1, n + 1))
        Gvu = np.zeros((n + 1, n + 1))
        Gvp = np.zeros((n + 1, n + 1))
        Gpv = np.zeros((n + 1, n + 1))

        for k in np.arange(n + 1):
            Guv[k, k] += g0[k] * weights[k] * rho[k] * J[k] * r[k]
            Gvu[k, k] += g0[k] * weights[k] * rho[k] * J[k] * r[k]

            Gvp[k, k] += rho[k] * weights[k] * J[k] * r[k]
            Gpv[k, k] += rho[k] * weights[k] * J[k] * r[k]

        # Build the final matrix
        # Gl = [ Guu Guv Gup; Gvu Gvv Gvp; Gpu Gpv Gpp ]
        Gl[:: 3, 1:: 3] = Guv
        Gl[1:: 3, :: 3] = Gvu
        Gl[1:: 3, 2:: 3] = Gvp
        Gl[2:: 3, 1:: 3] = Gpv

        # return in compressed sparse row format
        return Gl.tocsr()

    def __global_stiffnessK(self):
        """
        build up unassembled global stiffness matrix
        see documentation
        """
        n = self.n
        Ne = self.Ne
        r = self.r
        jac_ind = self.jacobian_indices

        # matrix dimensions
        nldof = 3 * Ne * (n + 1)

        # create K, Klp1, Klm1, K_C, K_F, K_L, K_ANCF, K_N, K_LCm1F
        # matrices
        K_list = []
        Klp1_list = []
        KC_list = []
        KF_list = []
        KL_list = []
        KAN_list = []
        KN_list = []

        for k in np.arange(n + 1):
            Ktmp = lil_matrix((nldof, nldof))
            Klp1tmp = lil_matrix((nldof, nldof))
            KCtmp = lil_matrix((nldof, nldof))
            KFtmp = lil_matrix((nldof, nldof))
            KLtmp = lil_matrix((nldof, nldof))
            KANtmp = lil_matrix((nldof, nldof))
            KNtmp = lil_matrix((nldof, nldof))

            for j in jac_ind:
                Ktmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessK(r[j[1]:j[2]], j[0], k, n)
                Klp1tmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessKlp1(r[j[1]:j[2]], j[0], k, n)
                KCtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessK_C(r[j[1]:j[2]], j[0], k, n)
                KFtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessK_F(r[j[1]:j[2]], j[0], k, n)
                KLtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessK_L(r[j[1]:j[2]], j[0], k, n)
                KANtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessK_AN(r[j[1]:j[2]], j[0], k, n)
                KNtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessK_N(r[j[1]:j[2]], j[0], k, n)

            # Save matrix
            K_list.append(Ktmp.tocsr())
            Klp1_list.append(Klp1tmp.tocsr())
            KC_list.append(KCtmp.tocsr())
            KF_list.append(KFtmp.tocsr())
            KL_list.append(KLtmp.tocsr())
            KAN_list.append(KANtmp.tocsr())
            KN_list.append(KNtmp.tocsr())

        # return in compressed sparse row format
        return K_list,\
            Klp1_list,\
            KC_list,\
            KF_list,\
            KL_list,\
            KAN_list,\
            KN_list

    def __elemental_stiffnessK(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights
        l_prime = self.l_prime

        Kl = lil_matrix((3 * n + 3, 3 * n + 3))
        Kpp = np.zeros((n + 1, n + 1))

        for i, j in np.ndindex(Kpp.shape):
            Kpp[j, i] += (1. / (4. * np.pi * self.GRAVITY_G) * weights[k] *
                          l_prime[k, i] * l_prime[k, j] / J[k] * r[k] ** 2)

        # Build the final matrix
        # Kl = [ Kuu Kuv Kup; Kvu Kvv Kvp; Kpu Kpv Kpp ]
        Kl[2:: 3, 2:: 3] = Kpp

        # return in compressed sparse row format
        return Kl.tocsr()

    def __elemental_stiffnessKlp1(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights
        l_prime = self.l_prime

        Kl = lil_matrix((3 * n + 3, 3 * n + 3))
        Kpp = np.zeros((n + 1, n + 1))

        Kpp[k, k] += 1. / (4. * np.pi * self.GRAVITY_G) * weights[k] * J[k]

        for i in np.arange(n + 1):
            Kpp[i, k] += (1. / (4. * np.pi * self.GRAVITY_G) * weights[k] *
                          l_prime[k, i] * r[k])
            Kpp[k, i] += (1. / (4. * np.pi * self.GRAVITY_G) * weights[k] *
                          l_prime[k, i] * r[k])

        # Build the final matrix
        # Kl = [ Kuu Kuv Kup; Kvu Kvv Kvp; Kpu Kpv Kpp ]
        Kl[2:: 3, 2:: 3] = Kpp

        # return in compressed sparse row format
        return Kl.tocsr()

    def __elemental_stiffnessK_C(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights
        l_prime = self.l_prime

        Kl = lil_matrix((3 * n + 3, 3 * n + 3))
        Kuu = np.zeros((n + 1, n + 1))

        for i, j in np.ndindex(Kuu.shape):
            Kuu[j, i] += (weights[k] * l_prime[k, i] * l_prime[k, j] / J[k]
                          * r[k] ** 2)

        # Build the final matrix
        # Kl = [ Kuu Kuv Kup; Kvu Kvv Kvp; Kpu Kpv Kpp ]
        Kl[:: 3, :: 3] = Kuu

        # return in compressed sparse row format
        return Kl.tocsr()

    def __elemental_stiffnessK_F(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights
        l_prime = self.l_prime

        Kl = lil_matrix((3 * n + 3, 3 * n + 3))
        Kuu = np.zeros((n + 1, n + 1))

        for i in np.arange(n + 1):
            Kuu[i, k] += 2. * weights[k] * l_prime[k, i] * r[k]
            Kuu[k, i] += 2. * weights[k] * l_prime[k, i] * r[k]

        # Build the final matrix
        # Kl = [ Kuu Kuv Kup; Kvu Kvv Kvp; Kpu Kpv Kpp ]
        Kl[:: 3, :: 3] = Kuu

        # return in compressed sparse row format
        return Kl.tocsr()

    def __elemental_stiffnessK_L(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights
        l_prime = self.l_prime

        Kl = lil_matrix((3 * n + 3, 3 * n + 3))
        Kvv = np.zeros((n + 1, n + 1))

        Kvv[k, k] += weights[k] * J[k]

        for i, j in np.ndindex(Kvv.shape):
            Kvv[j, i] += (weights[k] * l_prime[k, i] * l_prime[k, j] / J[k]
                          * r[k] ** 2)

            if (k == i):
                Kvv[j, i] -= weights[i] * l_prime[k, j] * r[i]

            if (k == j):
                Kvv[j, i] -= weights[j] * l_prime[k, i] * r[j]

        # Build the final matrix
        # Kl = [ Kuu Kuv Kup; Kvu Kvv Kvp; Kpu Kpv Kpp ]
        Kl[1:: 3, 1:: 3] = Kvv

        # return in compressed sparse row format
        return Kl.tocsr()

    def __elemental_stiffnessK_AN(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights

        Kl = lil_matrix((3 * n + 3, 3 * n + 3))
        Kuu = np.zeros((n + 1, n + 1))

        Kuu[k, k] += 4. * weights[k] * J[k]

        # Build the final matrix
        # Kl = [ Kuu Kuv Kup; Kvu Kvv Kvp; Kpu Kpv Kpp ]
        Kl[:: 3, :: 3] = Kuu

        # return in compressed sparse row format
        return Kl.tocsr()

    def __elemental_stiffnessK_N(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights

        Kl = lil_matrix((3 * n + 3, 3 * n + 3))
        Kvv = np.zeros((n + 1, n + 1))

        Kvv[k, k] -= 2. * weights[k] * J[k]

        # Build the final matrix
        # Kl = [ Kuu Kuv Kup; Kvu Kvv Kvp; Kpu Kpv Kpp ]
        Kl[1:: 3, 1:: 3] = Kvv

        # return in compressed sparse row format
        return Kl.tocsr()

    def __global_stiffnessL(self):
        """
        build up unassembled global stiffness matrix
        see documentation
        """
        n = self.n
        Ne = self.Ne
        r = self.r
        jac_ind = self.jacobian_indices

        # matrix dimensions
        nldof = 3 * Ne * (n + 1)

        LF_list = []
        LL_list = []
        LAN_list = []

        for k in np.arange(n + 1):
            LFtmp = lil_matrix((nldof, nldof))
            LLtmp = lil_matrix((nldof, nldof))
            LANtmp = lil_matrix((nldof, nldof))

            # loop over blocks on the diagonal
            for j in jac_ind:
                LFtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessL_F(r[j[1]:j[2]], j[0], k, n)
                LLtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessL_L(r[j[1]:j[2]], j[0], k, n)
                LANtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessL_AN(r[j[1]:j[2]], j[0], k, n)

            # Save matrix
            LF_list.append(LFtmp.tocsr())
            LL_list.append(LLtmp.tocsr())
            LAN_list.append(LANtmp.tocsr())

        return LF_list, LL_list, LAN_list

    def __elemental_stiffnessL_F(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights
        l_prime = self.l_prime

        Ll = lil_matrix((3 * n + 3, 3 * n + 3))
        Luv = np.zeros((n + 1, n + 1))
        Lvu = np.zeros((n + 1, n + 1))

        for i in np.arange(n + 1):
            Luv[i, k] -= weights[k] * l_prime[k, i] * r[k]
            Lvu[k, i] -= weights[k] * l_prime[k, i] * r[k]

        # Build the final matrix
        # Ll = [ Luu Luv Lup; Lvu Lvv Lvp; Lpu Lpv Lpp ]
        Ll[:: 3, 1:: 3] = Luv
        Ll[1:: 3, :: 3] = Lvu

        # return in compressed sparse row format
        return Ll.tocsr()

    def __elemental_stiffnessL_L(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights
        l_prime = self.l_prime

        Ll = lil_matrix((3 * n + 3, 3 * n + 3))
        Luv = np.zeros((n + 1, n + 1))
        Lvu = np.zeros((n + 1, n + 1))

        Luv[k, k] -= weights[k] * J[k]
        Lvu[k, k] -= weights[k] * J[k]

        for i in np.arange(n + 1):
            Luv[k, i] += weights[k] * l_prime[k, i] * r[k]
            Lvu[i, k] += weights[k] * l_prime[k, i] * r[k]

        # Build the final matrix
        # Ll = [ Luu Luv Lup; Lvu Lvv Lvp; Lpu Lpv Lpp ]
        Ll[:: 3, 1:: 3] = Luv
        Ll[1:: 3, :: 3] = Lvu

        # return in compressed sparse row format
        return Ll.tocsr()

    def __elemental_stiffnessL_AN(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights

        Ll = lil_matrix((3 * n + 3, 3 * n + 3))
        Luv = np.zeros((n + 1, n + 1))
        Lvu = np.zeros((n + 1, n + 1))

        Luv[k, k] -= 2. * weights[k] * J[k]
        Lvu[k, k] -= 2. * weights[k] * J[k]

        # Build the final matrix
        # Ll = [ Luu Luv Lup; Lvu Lvv Lvp; Lpu Lpv Lpp ]
        Ll[:: 3, 1:: 3] = Luv
        Ll[1:: 3, :: 3] = Lvu

        # return in compressed sparse row format
        return Ll.tocsr()

    def __global_stiffnessR(self):
        """
        build up unassembled global stiffness matrix
        see documentation
        """
        n = self.n
        Ne = self.Ne
        r = self.r
        jac_ind = self.jacobian_indices

        # matrix dimensions
        nldof = 3 * Ne * (n + 1)

        R_list = []
        RA_list = []
        RL_list = []

        for k in np.arange(n + 1):
            Rtmp = lil_matrix((nldof, nldof))
            RAtmp = lil_matrix((nldof, nldof))
            RLtmp = lil_matrix((nldof, nldof))

            # loop over blocks on the diagonal
            for j in jac_ind:
                Rtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessR(r[j[1]:j[2]], j[0], k, n)
                RAtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessR_A(r[j[1]:j[2]], j[0], k, n)
                RLtmp[j[1] * 3:j[2] * 3, j[1] * 3:j[2] * 3] = \
                    self.__elemental_stiffnessR_L(r[j[1]:j[2]], j[0], k, n)

            # append to lists in compressed row format
            R_list.append(Rtmp.tocsr())
            RA_list.append(RAtmp.tocsr())
            RL_list.append(RLtmp.tocsr())

        return R_list, RA_list, RL_list

    def __elemental_stiffnessR(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights

        Rl = lil_matrix((3 * n + 3, 3 * n + 3))
        Rpp = np.zeros((n + 1, n + 1))

        Rpp[k, k] += 1. / (4. * np.pi * self.GRAVITY_G) * weights[k] * J[k]

        # Build the final matrix
        # Rl = [ Ruu Ruv Rup; Rvu Rvv Rvp; Rpu Rpv Rpp ]
        Rl[2:: 3, 2:: 3] = Rpp

        # return in compressed sparse row format
        return Rl.tocsr()

    def __elemental_stiffnessR_A(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights

        Rl = lil_matrix((3 * n + 3, 3 * n + 3))
        Rvv = np.zeros((n + 1, n + 1))

        Rvv[k, k] += weights[k] * J[k]

        # Build the final matrix
        # Rl = [ Ruu Ruv Rup; Rvu Rvv Rvp; Rpu Rpv Rpp ]
        Rl[1:: 3, 1:: 3] = Rvv

        # return in compressed sparse row format
        return Rl.tocsr()

    def __elemental_stiffnessR_L(self, r, J, k, n):
        """
        compute elemental stiffness matrix for a single element
        :type r: float array
        :param r: grid points within element
        :type J: float array
        :param J: jacobian for points within element
        :type k: integer
        :param k: outter loop index
        :type n: integer
        :param n: polynomial order
        """
        weights = self.weights

        Rl = lil_matrix((3 * n + 3, 3 * n + 3))
        Ruu = np.zeros((n + 1, n + 1))

        Ruu[k, k] += weights[k] * J[k]

        # Build the final matrix
        # Rl = [ Ruu Ruv Rup; Rvu Rvv Rvp; Rpu Rpv Rpp ]
        Rl[:: 3, :: 3] = Ruu

        # return in compressed sparse row format
        return Rl.tocsr()
