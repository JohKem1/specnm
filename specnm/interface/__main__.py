#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file includes the main functionality of
the interface to the specnm class.

:copyright:
    Johannes Kemper, 2018
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import sys
from argparse import ArgumentParser  # parse from console
import numpy as np
from .. import seismogram
from instaseis import Source, Receiver


if __name__ == "__main__":
    # setting up the parsing of the console input
    parser = ArgumentParser(
        description="Interface for seismology class of \
                     spectral element normal mode code (specnm)")
    parser.add_argument(
        "--fmin",
        dest="fmin",
        default=0.5/3600.,
        type=float,
        help="Minimum frequency for modes (default 0.5/3600 = PREM)")
    parser.add_argument(
        "--fmax",
        dest="fmax",
        default=0.005,
        type=float,
        help="Maximum frequency for modes (default 0.05 Hz)")
    parser.add_argument(
        "--model",
        "--m",
        dest="model",
        help="1D mesher model file (deck, poly, etc.)",
        required=True)
    parser.add_argument(
        "--attenuation",
        "--at",
        dest="attenuation",
        default=False,
        action="store_true",
        help="Attenuation on or off (default off)")
    parser.add_argument(
        "--gravity",
        "--g",
        dest="gravity",
        default='cowling',
        help="Type of gravity ('full', 'cowling', 'none')")
    parser.add_argument(
        "--moment_tensor",
        "--mt",
        nargs='+',
        dest="M",
        type=float,
        help="Moment tensor of source in format \
              --mt M_rr M_tt M_pp M_rt M_rp M_tp",
        required=True)
    parser.add_argument(
        "--loc_source",
        "--locs",
        nargs='+',
        dest="loc_source",
        type=float,
        help="Source location in format "
             "--locs lat_in_deg long_in_deg depth_in_m",
        required=True)
    parser.add_argument(
        "--loc_receiver",
        "--locr",
        nargs='+',
        dest="loc_receiver",
        type=float,
        help="Receiver location in format "
             "--locr lat_in_deg long_in_deg depth_in_m",
        required=True)
    parser.add_argument(
        "--start_time",
        "--t0",
        dest="start_time",
        default=0.0,
        type=float,
        help="Start time for seismogram generation")
    parser.add_argument(
        "--stop_time",
        "--tn",
        dest="stop_time",
        default=3600.0,
        type=float,
        help="Stop time for seismogram generation")
    parser.add_argument(
        "--dt",
        dest="dt",
        default=1.0,
        type=int,
        help="dt for seismogram generation")
    parser.add_argument(
        "--read_file",
        "--rfile",
        dest="read_file",
        default=None,
        help="Read database from the specified filename")
    parser.add_argument(
        "--save_file",
        "--sfile",
        dest="save_file",
        default=None,
        help="Save database to filename with the specified filename")
    parser.add_argument(
        "--verbose",
        "--v",
        dest="verbose",
        default=False,
        action="store_true",
        help="Whether to print some textual output.")
    parser.add_argument(
        "--kind",
        dest="kind",
        default='displacement',
        help="Which kind of trace "
             "[displacement, velocity, acceleration, displacement anelastic]"
             " (defaults to displacement)")

    # to allow negative values in argparse
    # with this snipped - is not an argument indicator anymore
    for i, arg in enumerate(sys.argv):
        if (arg[0] == '-') and arg[1].isdigit():
            sys.argv[i] = ' ' + arg

    # actual parsing
    args = parser.parse_args()

    # setting up seismogram database
    if args.attenuation:
        att_mode = 'eigenvector continuation stepwise'
    else:
        att_mode = 'elastic'

    if args.read_file is None:
        seis_db =\
            seismogram.mode_database.compute(
                model=args.model,
                fmax=args.fmax,
                gravity_mode=args.gravity,
                verbose=args.verbose,
                init_kwargs={'verbose': 2 if args.verbose else 0},
                problem_kwargs={'attenuation_mode': att_mode})
        if args.save_file is not None:
            seis_db.write_h5(args.save_file)
    else:
        seis_db =\
            seismogram.mode_database.read(args.read_file)

    # get seismogram
    source = Source(args.loc_source[0], args.loc_source[1], args.loc_source[2],
                    m_rr=args.M[0], m_tt=args.M[1], m_pp=args.M[2],
                    m_rt=args.M[3], m_rp=args.M[4], m_tp=args.M[5],
                    dt=args.dt)
    receiver = Receiver(args.loc_receiver[0],
                        args.loc_receiver[1],
                        args.loc_receiver[2])
    traces = seis_db.get_seismograms(
        source,
        receiver,
        dt=args.dt,
        t0=args.start_time,
        nt=np.ceil(args.stop_time // args.dt).astype(int),
        kind=args.kind)

    # plot only zrt
    traces[:3].plot()
