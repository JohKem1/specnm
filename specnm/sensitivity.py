#!/usr/bin/env python
"""
Short test for sensitivity kernels in specnm.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np  # noqa
from scipy.interpolate import lagrange  # noqa
from . import models_1D
from . import spheroidal_base  # noqa


class sensitivity:
    """
    Specnm class to produce isotropic and anisotropic sensitivity kernels
    """
    def isotropic_kernels(self, cls_out=None):
        """
        Produce isotropic sensitivity kernels, keys should be self explanatory

        :type cls_out: dict
        :param cls_out: specnm class structured output
        """
        print('Warning: Sensitivity kernels are not benchmarked. '
              'Use at own risk.')

        omegas = cls_out['angular frequencies'][:, np.newaxis]
        ks = cls_out['wave numbers'][:, np.newaxis]

        kappa0 = self.kappa0[np.newaxis, :]
        mu0 = self.mu0[np.newaxis, :]
        vp0 = self.vp[np.newaxis, :]
        vs0 = self.vs[np.newaxis, :]

        tpilog = np.log(omegas / self.wref)

        kappaw = kappa0 + kappa0 / self.QKAPPA[np.newaxis, :] *\
            2. / np.pi * tpilog
        muw = mu0 + mu0 * self.QMUinv[np.newaxis, :] *\
            2. / np.pi * tpilog
        Qpinv = (1.0 - 4.0 / 3.0 * vs0 ** 2 / vp0 ** 2) /\
            self.QKAPPA[np.newaxis, :] +\
            4.0 / 3.0 * (vs0 ** 2 / vp0 ** 2) *\
            self.QMUinv[np.newaxis, :]
        vpw = vp0 + vp0 / np.pi * Qpinv * tpilog
        vsw = vs0 +\
            vs0 / np.pi * self.QMUinv[np.newaxis, :] * tpilog

        rr = self.r
        rho = self.rho[np.newaxis, :]
        gacc = self.g_acc

        if (self.mode == 'spheroidal'):
            if self.sph_type > 1:
                [U, V, P] =\
                    spheroidal_base._UVP_to_U_V_P(cls_out['eigenfunctions'],
                                                  self.sph_type)
                [Udot, Vdot, Pdot] =\
                    spheroidal_base._UVP_to_U_V_P(cls_out['eigenfunctions_dr'],
                                                  self.sph_type)
                W = np.zeros_like(U)
                Wdot = np.zeros_like(U)
            else:
                U = cls_out['eigenfunctions']
                V = np.zeros_like(U)
                P = np.zeros_like(U)
                W = np.zeros_like(U)
                Udot = cls_out['eigenfunctions_dr']
                Vdot = np.zeros_like(U)
                Pdot = np.zeros_like(U)
                Wdot = np.zeros_like(U)
        else:
            W = cls_out['eigenfunctions']
            U = np.zeros_like(W)
            V = np.zeros_like(W)
            P = np.zeros_like(W)

            Wdot = cls_out['eigenfunctions_dr']
            Udot = np.zeros_like(W)
            Vdot = np.zeros_like(W)
            Pdot = np.zeros_like(W)

        Kkappa = self.Kkappa(omegas, ks, rr, U, Udot, V)
        Kmu = self.Kmu(omegas, ks, rr, U, Udot, V, Vdot, P, W, Wdot)

        # precalculate integral from r to radius planet for grav pert into rho
        intg = self.grav_int(U, V, ks, rr, rho)
        intg = np.zeros_like(U)
        Krho = self.Krho(omegas, ks, rho, rr, gacc, U, V, P, Pdot, W, intg)

        Kvp = self.Kvp(rho, vpw, Kkappa)
        Kvs = self.Kvs(rho, vsw, Kmu, Kkappa)
        Krhoprime = self.Krhoprime(vpw, vsw, Kkappa, Kmu, Krho)

        Kdiso = self.Kdiso(omegas, ks, rr, rho, kappaw, muw,
                           Kkappa, Kmu, Krho,
                           U, Udot, V, Vdot, P, Pdot, W, Wdot)

        # for plots it is the kernel multiplied by the respective parameter
        out_kernel = {'Qkappa': kappa0 * Kkappa,
                      'Qmu': mu0 * Kmu,
                      'rho': rho * Krho,
                      'rhop': rho * Krhoprime,
                      'vp': vp0 * Kvp,
                      'vs': vs0 * Kvs,
                      # after these are the pure kernels
                      'Kkappa': Kkappa,
                      'Kmu': Kmu,
                      'Krho': Krho,
                      'Krhoprime': Krhoprime,
                      'Kvp': Kvp,
                      'Kvs': Kvs,
                      'Kdiso': Kdiso}

        return out_kernel

    def anisotropic_kernels(self, cls_out=None, implemented=False):
        """
        Produce anisotropic sensitivity kernels, keys should
        be self explanatory

        :type cls_out: dict
        :param cls_out: specnm class structured output
        :type implemented: bool
        :param implemented: Whether the kernels are benchmarked (default=False)
        """
        if not implemented:
            raise NotImplementedError(
                'Anisotropic kernels are not benchmarked yet.'
                'Set parameter implemented to True'
                ' if you want to risk using it or debug it.')

        omegas = cls_out['angular frequencies'][:, np.newaxis]
        ks = cls_out['wave numbers'][:, np.newaxis]
        kappa0 = self.kappa0[np.newaxis, :]
        mu0 = self.mu0[np.newaxis, :]

        tpilog = np.log(omegas / self.wref)

        C0 = self.C0[np.newaxis, :]
        A0 = self.A0[np.newaxis, :]
        L0 = self.L0[np.newaxis, :]
        N0 = self.N0[np.newaxis, :]
        #  F0 = self.F0[np.newaxis, :]
        Cw = C0 + C0 * self.factorAC * tpilog
        Aw = A0 + A0 * self.factorAC * tpilog
        Lw = L0 + L0 * self.factorLN * tpilog
        Nw = N0 + N0 * self.factorLN * tpilog
        #  Fw = F0 + F0 * self.factorF * tpilog

        vpv = self.vpv[np.newaxis, :]
        vsv = self.vsv[np.newaxis, :]
        vph = self.vph[np.newaxis, :]
        vsh = self.vsh[np.newaxis, :]

        rr = self.r[np.newaxis, :]
        eta = self.eta[np.newaxis, :]
        rho = self.rho[np.newaxis, :]
        gacc = self.g_acc

        if (self.mode == 'spheroidal'):
            if self.sph_type > 1:
                [U, V, P] =\
                    spheroidal_base._UVP_to_U_V_P(cls_out['eigenfunctions'],
                                                  self.sph_type)
                [Udot, Vdot, Pdot] =\
                    spheroidal_base._UVP_to_U_V_P(cls_out['eigenfunctions_dr'],
                                                  self.sph_type)
                W = np.zeros_like(U)
                Wdot = np.zeros_like(U)
            else:
                U = cls_out['eigenfunctions']
                V = np.zeros_like(U)
                P = np.zeros_like(U)
                W = np.zeros_like(U)
                Udot = cls_out['eigenfunctions_dr']
                Vdot = np.zeros_like(U)
                Pdot = np.zeros_like(U)
                Wdot = np.zeros_like(U)
        else:
            U = np.zeros_like(W)
            V = np.zeros_like(W)
            P = np.zeros_like(W)
            W = cls_out['eigenfunctions']

            Udot = np.zeros_like(W)
            Vdot = np.zeros_like(W)
            Pdot = np.zeros_like(W)
            Wdot = cls_out['eigenfunctions_dr']

        Kkappa = self.Kkappa(omegas, ks, rr, U, Udot, V)
        Kmu = self.Kmu(omegas, ks, rr, U, Udot, V, Vdot, P, W, Wdot)

        # precalculate integral from r to radius planet for grav pert into rho
        intg = self.grav_int(U, V, ks, rr, rho)
        Krho = self.Krho(omegas, ks, rho, rr, gacc, U, V, P, Pdot, W, intg)

        KF = self.KFani(omegas, ks, rr, U, V, Udot)
        KL = self.KLani(omegas, ks, rr, U, V, W, Vdot, Wdot)
        KN = self.KNani(omegas, ks, U, V, W)
        KA = self.KAani(omegas, ks, U, V)
        KC = self.KCani(omegas, rr, Udot)

        Kvpv = self.Kvpv(rho, vpv, KC)
        Kvph = self.Kvph(rho, vph, KA, KF, eta)
        Kvsv = self.Kvsv(rho, vsv, KL, KF, eta)
        Kvsh = self.Kvsh(rho, vsh, KN)

        Kdani = self.Kdani(omegas, ks, rr, Cw, Aw, Lw, Nw, rho,
                           U, V, W, Vdot, Wdot,
                           KC, KA, KL, KN, Krho)
        Krhoani = self.Krhoani(vpv, vph, vsv, vsh, eta,
                               KC, KA, KF, KL, KN, Krho)

        out_kernel = {'kappa': kappa0 * Kkappa,
                      'mu': mu0 * Kmu,
                      'rho': rho * Krho,
                      'rhop': rho * Krhoani,
                      'vpv': Kvpv,
                      'vph': Kvph,
                      'vsv': Kvsv,
                      'vsh': Kvsh,
                      'diso': Kdani}

        return out_kernel

    # gravity integral function
    # this can be optimized
    def grav_int(self, U, V, k, r, rho):
        def lagrange_interp(f, nodeid):
            x = self.gll_points[nodeid:]
            if len(x) > 0:
                pol = lagrange(x, f)
                intep = pol.integ()
                res = intep(x[-1]) - intep(x[0])
            else:
                res = 0.0
            return abs(res)

        def kernel(U, V, k, r, rho):
            ri = np.copy(r)
            ri[0] = 1e-3
            return rho * U * (2 * U - k * V) / ri

        if np.all(U == 0.0):
            intg = np.zeros_like(U)
        else:
            intg = []

            indices = self.jacobian_indices
            jobwob = self.jacobian_vec * self.weights_vec

            for UU, VV, ks in zip(U, V, k):
                tmp = np.zeros_like(UU)
                intkernel = kernel(UU, VV, ks, r, rho[0])
                integs = [intkernel[idx1:].dot(jobwob[idx1:])
                          for _, idx1, idx2 in indices]
                integs.append(0.0)
                inmbr = 1
                for _, idx1, idx2 in indices:
                    tmp[idx1] = integs[inmbr - 1]
                    tmp[idx1 + 1: idx2] =\
                        [lagrange_interp(intkernel[ii:idx2], ii) +
                         integs[inmbr]
                         for ii in range(idx1 + 1, idx2)]
                    inmbr += 1
                intg.append(tmp)

        return np.array(intg)

    # isotropic
    def Krho(self, omega, k, rho, r, gacc,
             U, V, P, Pdot, W, intg):
        GRAVG = models_1D.GRAVITY_G

        Krho_o = -omega ** 2 * r ** 2 * (U ** 2 + V ** 2 + W ** 2) +\
            8 * np.pi * GRAVG * rho * r ** 2 * U ** 2 +\
            2 * r ** 2 * U * Pdot + 2 * r * k * V * P -\
            2 * gacc * r * U * (2 * U - k * V) -\
            8 * np.pi * GRAVG * r ** 2 * intg

        return Krho_o / (2 * omega)

    def Kkappa(self, omega, k, r, U, Udot, V):
        Kkappa_o = (r * Udot + 2 * U - k * V) ** 2
        return Kkappa_o / (2 * omega)

    def Kmu(self, omega, k, r, U, Udot, V, Vdot, P, W, Wdot):
        Kmu_o = 1. / 3. * (2 * r * Udot - 2 * U + k * V) ** 2 +\
            (r * Vdot - V + k * U) ** 2 + (r * Wdot - W) ** 2 +\
            (k ** 2 - 2) * (V ** 2 + W ** 2)
        return Kmu_o / (2 * omega)

    def Kdiso(self, omega, k, r, rho, kappa, mu, Kkappa, Kmu, Krho,
              U, Udot, V, Vdot, P, Pdot, W, Wdot):
        Kd_o = -kappa * Kkappa - mu * Kmu - rho * Krho +\
            2 * kappa * r * Udot * (r * Udot + 2 * U - k * V) +\
            4. / 3. * mu * r * Udot * (2 * r * Udot - 2 * U + k * V) +\
            2 * mu * r * Vdot * (r * Vdot - V + k * U) +\
            2 * mu * r * Wdot * (r * Wdot - W)
        return Kd_o / (2 * omega)

    def Kvp(self, rho, vp, Kkappa):
        Kvp_o = 2.0 * rho * vp * Kkappa
        return Kvp_o

    def Kvs(self, rho, vs, Kmu, Kkappa):
        Kvs_o = 2.0 * rho * vs * (Kmu - 4.0 / 3.0 * Kkappa)
        return Kvs_o

    def Krhoprime(self, vp, vs, Kkappa, Kmu, Krho):
        Krhop_o = (vp ** 2 - 4.0 / 3.0 * vs ** 2) * Kkappa +\
            vs ** 2 * Kmu + Krho
        return Krhop_o

    # anisotropic
    def Kvpv(self, rho, vpv, KC):
        return 2 * rho * vpv * KC

    def Kvph(self, rho, vph, KA, KF, eta):
        return 2 * rho * vph * (KA + eta * KF)

    def Kvsv(self, rho, vsv, KL, KF, eta):
        return 2 * rho * vsv * (KL - 2 * eta * KF)

    def Kvsh(self, rho, vsh, KN):
        return 2 * rho * vsh * KN

    #  def Keta(self, rho, vph, vsv, KF):
    #      return rho * (vph ** 2 - 2 * vsv ** 2) * KF

    def KCani(self, omega, r, Udot):
        KCani_o = r ** 2 * Udot ** 2
        return KCani_o / (2 * omega)

    def KAani(self, omega, k, U, V):
        KAani_o = (2 * U - k * V) ** 2
        return KAani_o / (2 * omega)

    def KLani(self, omega, k, r, U, V, W, Vdot, Wdot):
        KLani_o = (r * Vdot - V + k * U) ** 2 + (r * Wdot - W) ** 2
        return KLani_o / (2 * omega)

    def KNani(self, omega, k, U, V, W):
        KNani_o = - (2 * U - k * V) ** 2 + (k ** 2 - 2) * (V ** 2 + W ** 2)
        return KNani_o / (2 * omega)

    def KFani(self, omega, k, r, U, V, Udot):
        KFani_o = 2 * r * Udot * (2 * U - k * V)
        return KFani_o / (2 * omega)

    def Kdani(self, omega, k, r, C, A, L, N, rho, U, V, W, Vdot, Wdot,
              KC, KA, KL, KN, Krho):
        Kdani_o = C * (2 * omega * KC) - A * (2 * omega * KA) -\
            L * (2 * omega * KL) - N * (2 * omega * KN) -\
            rho * (2 * omega * Krho) +\
            2 * L * r * Vdot * (r * Vdot - V + k * U) +\
            2 * L * r * Wdot * (r * Wdot - W)
        return Kdani_o / (2 * omega)

    def Krhoani(self, vpv, vph, vsv, vsh, eta, KC, KA, KF, KL, KN, Krho):
        Krhoani_o = vpv ** 2 * KC + vph ** 2 * (KA + eta * KF) +\
                vsv ** 2 * (KL - 2 * eta * KF) + vsh ** 2 * KN + Krho
        return Krhoani_o
