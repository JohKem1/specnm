#!/usr/bin/env python
"""
Base class for spheroidal modes.
Contains functions that are used by all spheroidal mode types.
Full gravity, Cowling, No Gravity or Radial modes.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2019-2024
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
from itertools import cycle
from .sensitivity import sensitivity


def _UVP_to_U_V_P(UVP, sph_type):
    """
    Produces the individual eigenfunctions from spheroidal structured output

    :param UVP: eigenfunction output from problem calculation
    :type UVP: ndarray (float)
    :param sph_type: Spheroidal type from base class (cls.sph_type)
    :type sph_type: int
    :returns: U, V, P eigenfunctions in seperature numpy float arrays
    :rtype: ndarray (float)
    """
    U = UVP[:, 0::sph_type]
    V = UVP[:, 1::sph_type]

    if sph_type > 2:
        P = UVP[:, 2::sph_type]
    else:
        P = np.zeros_like(U)
    return U, V, P


class sph_base(sensitivity):
    """
    Specnm spheroidal base class that handles spheroidal mode eigenvalue
    problem input and output functionionality
    """
    def rayleigh_problem(self, l0=1, lmax=None, fmin=0.5/3600., fmax=None,
                         attenuation_mode=None, logskip=False, dlog10l=0.1,
                         llist=None, sensitivity_kernels='none',
                         epsilon_spurious=1e-3, energy_ratio_spurious=0.999,
                         **multishift_kwargs):
        """
        Produces the spectrum of eigenvalues and eigenfunctions
        until the max frequency or max angular order is reached

        :type l0: int
        :param l0: minimum angular order (should be l0>0)
        :type lmax: int
        :param lmax: maximum angular order (output only till fmax of mesh)
        :type fmin: float
        :param fmin: minimum output frequency (cutoff for undertones)
                     defaults to 1/120 min based on minimum earth frequency
        :type fmax: float
        :param fmax: maximum output frequency (freq<=fmax)
        :type attenuation_mode: str
        :param attenuation_mode: attenuation mode for calculation
                                 {'no' or 'elastic',
                                 'first order',
                                 'eigenvector continuation',
                                 'full' or
                                 'eigenvector continuation stepwise'}
        :type logskip: bool
        :param logskip: logarithmic steps in angular order
        :type dlog10l: float
        :param dlog10l: declogarithmic stepsize according to
                        l = int(10^(log10(l-1) + dlog10l))
        :type llist: list of integers or ndarray (int)
        :param llist: list of angular orders to iterate
        :type sensitivity_kernels: str
        :param sensitivity_kernels: Produce sensitivity kernels (default none)
                                    {'none', 'isotropic', 'anisotropic'}
        :type epsilon_spurious: float
        :param epsilon_spurious: error cutoff after upmapping and downmapping
                                 of solutions (filter numerical spurious modes)
                                 (default=1e-3)
        :type energy_ratio_spurious: float
        :param energy_ratio_spurious: Filter mode over ratio of
                                      kinetic energy in fluid (default=0.999)
        :type multishift_kwargs: dict
        :param multishift_kwargs: additional arguments for the multishift
            solver
        :returns: Structured output with keys:
                  angular orders, wave numbers,
                  frequencies, angular frequencies,
                  eigenfunctions, eigenfunctions_dr,
                  phase velocities, group velocities,
                  gamma, Q, epsilons, energy ratios,
                  radius, polynomial order, gravitational acceleration
        :rtype: dict
        """
        if llist is not None:
            llist = np.copy(sorted(llist)).tolist()
            lmax = llist[-1]
            llist.append(lmax + 1)
            lcycle = cycle(llist)
            l0 = next(lcycle)

        if (l0 <= 0):
            raise ValueError('l0 has to be positive.')
        if (lmax is not None and l0 > lmax):
            raise ValueError('l0 has to be smaller than lmax.')

        attenuation = (self.model_attenuation and
                       attenuation_mode not in ['elastic', 'first order'])

        if (self.verbose):
            print('Initialization of solver and start of calculation.')

        # initialize eigenvalue problem
        self._eigenvalue_problem_init(attenuation_mode)

        if self.dispersion:
            _ = multishift_kwargs.setdefault('n_total', 10)

        if (fmax is None):
            fmax = self.fmesh
        elif (fmax > self.fmesh):
            raise ValueError(
                'The maximum input frequency was \
                 higher than maximum frequency of the mesh.')

        if attenuation_mode == 'first order':
            f0 = fmax
            self._update_physical_properties(2 * np.pi * f0)

        # initialize variables
        fs = []
        UVPs = []
        ls = []
        energy_ratios = []
        epsilons = []

        if logskip:
            # since the dispersion curves are interpolated we do not need
            # each l
            lfunc = lambda l: int(10**(np.log10(l) + dlog10l))  # NOQA
        else:
            # in general need all angular orders
            lfunc = lambda l: l + 1  # NOQA

        cur_l = l0

        _fmin = fmin
        # compute eigenfrequency and eigenfunctions
        while lmax is None or cur_l <= lmax:
            if (self.verbose):
                print(f'Current angular order: {cur_l:5d}')
                print(f'Current fmin         : {_fmin}')

            # solve the eigenvalue problem in base class
            ef, ev = self._eigenvalue_problem(cur_l, _fmin, fmax,
                                              **multishift_kwargs)

            # maximum l reached? If not provided by the user, stop if at this
            # frequency the interval contains no modes
            if len(ef) == 0:
                if cur_l > 1 and lmax is None:
                    break
            else:
                # get eigenfunctions according to Dahlen&Tromp
                ev = self.scatter.dot(ev.T).T
                ev *= self._sign(ev[:, -self.sph_type])[:, np.newaxis]

                _U, _V, _P = _UVP_to_U_V_P(ev, self.sph_type)

                _k = (cur_l * (cur_l + 1)) ** 0.5 * np.ones_like(ef)
                _w = 2 * np.pi * ef
                eps = self._epsilon(_U, _V, _k, _w, _P,
                                    attenuation=attenuation)

                if not self.dispersion:
                    # get energy ratios
                    er = self._get_energy_ratio(_U, _V)

                    # if the accuracy is low AND the energy ratio is not high,
                    # there is at least one mode that has significant energy
                    # in the solid with low accuracy,
                    # i.e. we can't be sure if it is
                    # spurious or not. With better mesh resolution, these modes
                    # should disappear.
                    if np.any((eps > epsilon_spurious) *
                              (er < energy_ratio_spurious)):
                        raise RuntimeError(
                            'Modes in danger zone, '
                            'try increasing mesh frequency.')

                    spurious_mask = eps < epsilon_spurious

                    # check again if maximum l was reached after filtering
                    if spurious_mask.sum() == 0:
                        if lmax is None:
                            break

                    if (self.verbose):
                        print(f'Number of overtones: {spurious_mask.sum()}')

                    # only keep good modes
                    fs += ef[spurious_mask].tolist()
                    UVPs += ev[spurious_mask].tolist()
                    epsilons += eps[spurious_mask].tolist()
                    energy_ratios += er[spurious_mask].tolist()
                    ls += [cur_l] * spurious_mask.sum()

                    # update _fmin to avoid computing all long period spurious
                    # modes and problems with jumping across large gaps in the
                    # spectrum. This may cause a problem in case the
                    # fundamental mode branch at l0 is smaller than the user
                    # input fmin and far away from the first overtone. For
                    # l = 1 the fundamental mode is missing, so can't update
                    # (actually the Slichter mode is just very low frequency
                    # so we mostly miss it due to fmin)
                    if cur_l >= 2:
                        _fmin = max(fmin, ef[spurious_mask].min() * 0.8)
                else:
                    fs += ef.tolist()
                    UVPs += ev.tolist()
                    epsilons += eps.tolist()
                    energy_ratios += [np.nan] * len(ef)
                    ls += [cur_l] * len(ef)

                    # same as above
                    if cur_l >= 2:
                        _fmin = max(fmin, ef.min() * 0.8)

            if llist is None:
                cur_l = lfunc(cur_l)
            else:
                cur_l = next(lcycle)

        if len(fs) == 0:
            raise ValueError('No mode found!')

        fs = np.array(fs)
        ls = np.array(ls)
        UVPs = np.array(UVPs)
        epsilons = np.array(epsilons)
        energy_ratios = np.array(energy_ratios)
        ws = 2 * np.pi * fs
        ks = (ls * (ls + 1)) ** 0.5

        Us, Vs, Ps = _UVP_to_U_V_P(UVPs, self.sph_type)

        # compute phase & group velocity
        cp = ws / ks
        cg = self._group_vel(Us, Vs, cp, ks, ls, ws, Ps,
                             attenuation=attenuation)

        # normalize eigenfunctions
        UVPs /= self._normalize(Us, Vs)[:, np.newaxis]

        if self.model_attenuation:
            gammas = self._calculate_gammas(ks, ws, Us, Vs)
            Qs = ws / gammas / 2
            if attenuation:
                # correct group velocity for physical dispersion
                # seem to go the right direction, but still higher error than
                # for elastic
                cg *= 1 + 2 * gammas / (np.pi * ws)
        else:
            gammas = np.zeros_like(fs)
            Qs = np.zeros_like(fs) + np.inf

        if attenuation_mode == 'first order':
            # Dahlen & Tromp (9.55)
            fs += fs / np.pi / Qs * np.log(fs / f0)

        # derivative of eigenfunction
        jop = self.jacobian_vec[np.newaxis, :]
        derop = self.l_prime_mat
        UVPdot = np.zeros_like(UVPs)
        Us, Vs, Ps = _UVP_to_U_V_P(UVPs, self.sph_type)

        UVPdot[:, 0::self.sph_type] = derop.dot(Us.T).T / jop
        UVPdot[:, 1::self.sph_type] = derop.dot(Vs.T).T / jop

        if self.sph_type > 2:
            UVPdot[:, 2::self.sph_type] = derop.dot(Ps.T).T / jop

        # reset physical properties if next run is elastic
        if attenuation_mode != 'elastic':
            self._reset_physical_properties()

        structured_output = {
            'angular orders': ls,
            'wave numbers': ks,
            'frequencies': fs,
            'angular frequencies': ws,
            'eigenfunctions': UVPs,
            'eigenfunctions_dr': UVPdot,
            'phase velocities': cp * self.radius,
            'group velocities': cg * self.radius,
            'gamma': gammas,
            'Q': Qs,
            'energy ratios': energy_ratios,
            'epsilons': epsilons,
            'radius': self.r,
            'polynomial order': self.n,
            'gravitational acceleration': self.g_acc,
        }

        # related to sensitivity kernel output
        sensitivities = {'isotropic': self.isotropic_kernels,
                         'anisotropic': self.anisotropic_kernels}

        if sensitivity_kernels in sensitivities:
            structured_output['sensitivity kernels'] =\
                sensitivities[sensitivity_kernels](structured_output)

        if (self.verbose):
            print('Done.')

        return structured_output

    def _update_physical_properties(self, wfreq):
        """
        Computes perturbation of material properties
        :type wfreq: float
        :param wfreq: angular frequency for update
        """
        lnfrq = np.log(wfreq / self.wref)

        self.A = self.A0 + self.factorAC * lnfrq
        self.C = self.C0 + self.factorAC * lnfrq
        self.L = self.L0 + self.factorLN * lnfrq
        self.N = self.N0 + self.factorLN * lnfrq
        self.F = self.F0 + self.factorF * lnfrq

        # build updated medium matrices
        self._build_medium_matrices()

    def _reset_physical_properties(self):
        """
        Resets material matrices to reference frequency ones.
        """
        self.C = self.C0.copy()
        self.N = self.N0.copy()
        self.F = self.F0.copy()
        self.A = self.A0.copy()
        self.L = self.L0.copy()

        self._build_medium_matrices()

    def _group_vel(self, U, V, c, k, l, omega, P=None, attenuation=True):
        """
        Compute group velocity according to D&T1998 (11.85)-(11.87)
        :type U: ndarray (float)
        :param U: eigenfunction U within domain
        :type V: ndarray (float)
        :param V: eigenfunction V within domain
        :type c: float
        :param c: phase velocity
        :type k: float
        :param k: wavenumber
        :type l: int
        :param l: angular order
        :type omega: float
        :param omega: angular frequency
        :type P: ndarray (float)
        :param P: eigenfunction P within domain
        :type attenuation: bool
        :param attenuation: whether to use attenuation (default True)
        :returns: Group velocities
        :rtype: ndarray (float)
        """
        if (P is None):
            P = np.zeros_like(U)

        # setting up operators
        derop = self.l_prime_mat
        jop = self.jacobian_vec
        jopwop = self.jacobian_vec * self.weights_vec
        gop = self.g_acc if self.gravity else 0.

        # localize variables
        rho = self.rho
        r = self.r

        if attenuation:
            lnfrq = np.log(omega / self.wref)

            A = self.A0[None, :] + self.factorAC[None, :] * lnfrq[:, None]
            L = self.L0[None, :] + self.factorLN[None, :] * lnfrq[:, None]
            N = self.N0[None, :] + self.factorLN[None, :] * lnfrq[:, None]
            F = self.F0[None, :] + self.factorF[None, :] * lnfrq[:, None]
        else:
            A = self.A
            L = self.L
            N = self.N
            F = self.F

        AN = A - N

        Udot = derop.dot(U.T).T / jop
        Vdot = derop.dot(V.T).T / jop

        I1 = (rho * (U ** 2 + V ** 2) * r ** 2).dot(jopwop)
        I2 = (L * U ** 2 + A * V ** 2).dot(jopwop)  # note typo in 11.85 !!
        I3 = (2. * L * U * (Vdot * r - V) -
              2. * F * Udot * V * r -
              4. * AN * U * V +
              rho * r * (V * P + 2. * gop * U * V)).dot(jopwop)

        # extra terms in primed integrals
        if not np.all(P == 0.):
            P_fac_prime = (
                rho[None, :] * (r[None, :] / self.radius) ** (l[:, None] + 1) *
                (l[:, None] * U + k[:, None] * V)).dot(jopwop)
            I2 += ((4 * np.pi * self.GRAVITY_G) / (2 * l + 1) ** 3 *
                   P_fac_prime ** 2 * self.radius)
            I2 += 1. / (4 * np.pi * self.GRAVITY_G) * (P ** 2).dot(jopwop)
            I3 += (rho * r * V * P).dot(jopwop)

        return (I2 + 0.5 * I3 / k) / (c * I1)

    def _epsilon(self, U, V, k, omega, P=None, attenuation=True):
        """
        Compute energy partitioning error as suggested by Takeuchi & Saito 1972

        with an additional factor 2, this gives the relative error in omega,
        see also Zabranova et al (2017)

        Compute everything at polynomial order n+1, as otherwise the error
        estimate is zero, because the discrete solutions fulfill the discrete
        rayleigh quotient.

        :param U: U eigenfunction(s)
        :type U: ndarray (float)
        :param V: V eigenfunction(s)
        :type V: ndarray (float)
        :param k: wave number(s)
        :type k: ndarray (float)
        :param omega: angular frequencie(s)
        :type omega: numpy arrayndarray (float)
        :param P: P eigenfunction(s)
        :type P: ndarray (float)
        :param attenuation: Whether to use attenuation (default True)
        :type attenuation: bool
        :returns: Energy partitioning error estimate
        :rtype: ndarray (float)
        """
        if (P is None):
            P = np.zeros_like(U)

        jop = self.jacobian_vec_np1[np.newaxis, :]
        jopwop = self.jacobian_vec_np1 * self.weights_vec_np1
        derop = self.l_prime_mat_np1

        rho = self.P_n_to_np1.dot(self.rho)
        r = self.P_n_to_np1.dot(self.r)
        gop = self.P_n_to_np1.dot(self.g_acc) if self.gravity else 0.

        if attenuation:
            factorAC = self.P_n_to_np1.dot(self.factorAC)
            factorLN = self.P_n_to_np1.dot(self.factorLN)
            factorF = self.P_n_to_np1.dot(self.factorF)
            lnfrq = np.log(omega / self.wref)

            C = self.P_n_to_np1.dot(self.C0)
            A = self.P_n_to_np1.dot(self.A0)
            L = self.P_n_to_np1.dot(self.L0)
            N = self.P_n_to_np1.dot(self.N0)
            F = self.P_n_to_np1.dot(self.F0)

            C = C[None, :] + factorAC[None, :] * lnfrq[:, None]
            A = A[None, :] + factorAC[None, :] * lnfrq[:, None]
            L = L[None, :] + factorLN[None, :] * lnfrq[:, None]
            N = N[None, :] + factorLN[None, :] * lnfrq[:, None]
            F = F[None, :] + factorF[None, :] * lnfrq[:, None]
        else:
            C = self.P_n_to_np1.dot(self.C)
            A = self.P_n_to_np1.dot(self.A)
            L = self.P_n_to_np1.dot(self.L)
            N = self.P_n_to_np1.dot(self.N)
            F = self.P_n_to_np1.dot(self.F)

        U = self.P_n_to_np1.dot(U.T).T
        V = self.P_n_to_np1.dot(V.T).T
        P = self.P_n_to_np1.dot(P.T).T

        Udot = derop.dot(U.T).T / jop
        Vdot = derop.dot(V.T).T / jop
        Pdot = derop.dot(P.T).T / jop

        # D&T 8.126
        T = (rho * (U ** 2 + V ** 2) * r ** 2).dot(jopwop)

        # D&T 8.208
        Ve = (C * (Udot * r) ** 2 +
              2 * F * r * Udot * (2 * U - k[:, None] * V) +
              (A - N) * (2 * U - k[:, None] * V) ** 2 +
              L * (Vdot * r - V + k[:, None] * U) ** 2 +
              (k[:, None] ** 2 - 2) * N * V ** 2).dot(jopwop)

        # D&T 8.129
        Vg = (- 2 * gop * rho * r * U * (2 * U - k[:, None] * V)).dot(jopwop)

        if not np.all(P == 0.):
            Vg += (rho * U * Pdot * r ** 2 +
                   k[:, None] * r * rho * V * P +
                   4 * np.pi * self.GRAVITY_G * rho ** 2 * U ** 2 * r ** 2
                   ).dot(jopwop)

        epsilon = np.abs((Ve + Vg) / (omega ** 2 * T) / 2 - 0.5)

        return epsilon

    def _calculate_gammas(self, k, omega, U, V=None):
        """
        Calculates the gamma damping factor for spheroidal modes
        :type k: float or ndarray (float)
        :param k: wave number
        :type omega: float or ndarray (float)
        :param omega: angular frequency
        :type U: ndarray (float)
        :param U: U eigenfunction
        :type V: ndarray (float)
        :param V: V eigenfunction
        :returns: Decay factor gamma
        :rtype: float or ndarray (float)
        """
        if V is None:
            V = np.zeros_like(U)

        # setting up operators and localize variables
        derop = self.l_prime_mat
        jopwop = self.jacobian_vec * self.weights_vec
        jop = self.jacobian_vec
        r = self.r

        # add frequency dependent log factor to material properties
        tpilog = 2. / np.pi * np.log(omega / self.wref)[:, np.newaxis]
        kappaw = self.kappa0[np.newaxis, :] + \
            self.kappa0[np.newaxis, :] / self.QKAPPA[np.newaxis, :] * tpilog
        muw = self.mu0 + self.mu0 * self.QMUinv * tpilog

        # calculate gamma factors
        Udot = derop.dot(U.T).T / jop
        Vdot = derop.dot(V.T).T / jop

        K_kappa = ((r * Udot + 2. * U - k[:, np.newaxis] * V) ** 2 /
                   (2. * omega[:, np.newaxis]))
        K_mu = ((1. / 3. * (2. * r * Udot - 2. * U + k[:, np.newaxis] * V) ** 2
                 + (r * Vdot - V + k[:, np.newaxis] * U) ** 2 +
                 (k[:, np.newaxis] ** 2 - 2.) * V ** 2) /
                (2. * omega[:, np.newaxis]))

        gamma_kappa = (K_kappa * jopwop * kappaw /
                       self.QKAPPA[np.newaxis, :]).sum(axis=1)
        gamma_mu = (K_mu * jopwop * muw *
                    self.QMUinv[np.newaxis, :]).sum(axis=1)

        return (gamma_kappa + gamma_mu)

    def _get_energy_ratio(self, U, V):
        """
        Calculate the fluid to solid energy ratio as a criterion to filter
        spurious modes
        :param U: U eigenfunction(s)
        :type U: ndarray (float)
        :param V: V eigenfunction(s)
        :type V: ndarray (float)
        :returns: Ratio of fluid to global kinetic energy of mode(s)
        :rtype: float or ndarray (float)
        """
        r = self.r
        rho = self.rho
        wobjob = self.weights_vec * self.jacobian_vec

        r_f = self.r[self.fluid_indices]
        rho_f = self.rho[self.fluid_indices]
        wobjob_f = (self.weights_vec * self.jacobian_vec)[self.fluid_indices]

        U_f = U[:, self.fluid_indices]
        V_f = V[:, self.fluid_indices]

        T_f = (r_f ** 2 * rho_f * (U_f ** 2 + V_f ** 2)).dot(wobjob_f)
        T_g = (r ** 2 * rho * (U ** 2 + V ** 2)).dot(wobjob)

        return T_f / T_g
