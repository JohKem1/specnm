#!/usr/bin/env python
"""
Renormalize the output eigenfunctions for spheroidal full gravity.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import rayleigh_fg, models_1D
from specnm.spheroidal_base import _UVP_to_U_V_P
import matplotlib.pyplot as plt
import numpy as np


def renormalize_specnm(cls, w, F1, F2=None):
    """
    Computes norm of eigenfunction using function provided in normfact

    :type F1: float array
    :param F1: first eigenfunction (U, W)
    :type F2: float array
    :param F2: second eigenfunction (V)
    """
    if (F2 is None):
        F2 = 0.

    # localize variables
    # r = cls.r
    G = models_1D.GRAVITY_G
    rho = cls.rho
    wopjop = cls.weights_vec * cls.jacobian_vec

    # edit this function if you want a different normalization
    normfact = (G * rho * (F1 ** 2 + F2 ** 2)).dot(wopjop)
    return normfact


if __name__ == '__main__':
    model = '../models/prem_ani'
    att_mode = 'full'

    ray = rayleigh_fg(model=model, fmax=0.005)
    ray_out = ray.rayleigh_problem(attenuation_mode=att_mode,
                                   fmax=0.004)

    # renomalize the specnm efs to fit mineos norm definition
    # get eigenfunctions
    UVPs = np.copy(ray_out['eigenfunctions'])
    Us, Vs, Ps = _UVP_to_U_V_P(UVPs, ray.sph_type)
    UVPsf = UVPs / renormalize_specnm(ray,
                                      ray_out['angular frequencies'],
                                      Us,
                                      Vs)[:, np.newaxis]
    Usf, Vsf, Psf = _UVP_to_U_V_P(UVPsf, ray.sph_type)

    # plotting
    plt.plot(Usf[9], ray.r / 1e3, 'k-', label='U')
    plt.plot(Vsf[9], ray.r / 1e3, ls='dashed', color='black', label='V')
    plt.plot(Psf[9], ray.r / 1e3, ls='dotted', color='black', label='P')
    plt.ylabel('radius in km')
    plt.xlabel('eigenfunction')
    plt.ylim(ymin=0.0, ymax=ray.r[-1] / 1e3)
    for fb in ray.fluid_boundaries:
        plt.axhline(fb / 1e3, color='tab:blue', ls='dashed')
    plt.legend()
    plt.tight_layout()
    plt.show()
