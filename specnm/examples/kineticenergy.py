#!/usr/bin/env python
"""
Calculate kinetic energy in radius for some PREM modes.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import rayleigh
from specnm.spheroidal_base import _UVP_to_U_V_P
import numpy as np


def kinetic_energy(ray, ray_out, region_indices=[]):
    # get eigenfunctions
    [U, V, P] = _UVP_to_U_V_P(ray_out['eigenfunctions'], ray.sph_type)

    # restrict to specific region if needed
    region_ids = range(len(ray.r))\
        if len(region_indices) == 0 else region_indices

    rho = ray.rho[region_ids]
    r = ray.r[region_ids]
    wopjop = (ray.weights_vec * ray.jacobian_vec)[region_ids]
    U_reg = U[:, region_ids]
    V_reg = V[:, region_ids]

    # calculate kinetic energies
    out = (rho * (U_reg ** 2 + V_reg ** 2)
           * r ** 2).dot(wopjop)

    return out


if __name__ == '__main__':
    ray = rayleigh('../models/prem_ani', fmax=0.005)
    ray_out = ray.rayleigh_problem(fmax=0.001)

    # get kinetic energies
    ekins = kinetic_energy(ray, ray_out, ray.fluid_indices)

    # get overtone numbers
    l_uniq, l_count = np.unique(ray_out['angular orders'], return_counts=True)
    overtones = np.concatenate(
                [np.arange(lc) if lu != 1 else np.arange(2, lc + 2)
                 for lu, lc in zip(l_uniq, l_count)])

    print('Kinetic energies in fluid outer core ekin:')
    print('nSl: frequency in mHz, period in s, ekin')
    for nn, ll, frq, per, ekin in zip(overtones,
                                      ray_out['angular orders'],
                                      ray_out['frequencies'],
                                      1. / ray_out['frequencies'],
                                      ekins):
        print(f'{nn}S{ll}: {frq * 1e3: 1.5f}, {per: 1.5f}, {ekin:1.5f}')
