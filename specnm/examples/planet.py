#!/usr/bin/env python
"""
Deal with new planet in specnm and find apropriate fmin.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
from specnm import rayleigh, plotter
import matplotlib.pyplot as plt


if __name__ == '__main__':
    model = '../models/vpremoon'
    # model = '../models/prem_ani' # uncomment to see some spurious mode

    fmesh = 0.01  # increase this if you run into accuracy problems
    fmax = 0.005  # smaller bodies: physical spectrum higher frequencies

    fmin = 1e-4  # start with 1e-6 or lower and increase
    verbose = False

    # these are set to let all modes through - also spurious
    # they need to be adapted to fit the spectrum of the body
    # for this there is the plot below
    # if the energy ratio is close to 1 the mode *lives* solely in the fluid
    # layers and is hence probably a undertone
    # if the projected error is high the mode might be a numerical spurious
    # mode that emerges from underresolving the infinite spectrum
    #
    # the following two parameters need to be set accordingly
    # if you do not know what to put here the default values indicated are a
    # good start
    #
    eps_max = 1.0  # default is 1e-3 (normalized max. error projected modes)
    eng_rat = -1e-3  # default is 0.999 (ratio of energy in fluid to full body)

    # look at the spectrum to determine if you got thegravest mode
    spectrum = False

    # run specnm
    ray = rayleigh(model=model, fmax=fmesh, verbose=False)
    ray_out = ray.rayleigh_problem(fmin=fmin,
                                   fmax=fmax,
                                   epsilon_spurious=eps_max,
                                   energy_ratio_spurious=eng_rat)

    if spectrum:
        plotter.spectrum(ray, ray_out)

    # plot
    # uniq = np.unique(ray_out['angular orders'])
    # just plot one possible output for this case
    uniq = [ray_out['angular orders'][0]]
    for ll in uniq:
        idxs = np.where(ray_out['angular orders'] == ll)[0]

        fig, axs = plt.subplots(1, 2)

        axl = axs[0]
        axr = axs[1]
        engs = ray_out['energy ratios'][idxs]
        epss = ray_out['epsilons'][idxs]
        frqs = ray_out['frequencies'][idxs] * 1e3

        axl.set_xlabel('frequencies in mHz')
        axl.set_ylabel('kinetic energy ratio fluid / solid')
        axl.scatter(frqs, engs, marker='x', color='black')
        axl.axhline(eng_rat, color='tab:red')
        axl.axvline(fmin * 1e3, color='tab:red')
        axl.set_ylim(ymax=1.01)

        axr.set_xlabel('frequencies in mHz')
        axr.set_ylabel(r'error of solution $\epsilon$')
        axr.scatter(frqs, epss, marker='x', color='black')
        axr.axhline(eps_max, color='tab:red')
        axr.axvline(fmin * 1e3, color='tab:red')

        plt.tight_layout()
        plt.show()
