#!/usr/bin/env python
"""
Example seismogram using the mode database of specnm.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import seismogram
from instaseis import Source, Receiver
import matplotlib.pyplot as plt


if __name__ == '__main__':
    # write or read a database
    read = False
    write = False

    if read:
        db = seismogram.mode_database.read('test.h5')
    else:
        # setup the database and run specnm internally
        db = seismogram.mode_database.compute(model='../models/prem_noocean',
                                              fmax=0.008,
                                              verbose=True)
        if write:
            db.write_h5('test.h5')

    # access the output of the toroidal run as follows
    # polynomials order
    tor_poly = db.modes['T']['polynomial order']
    print(f'Polynomial order toroidal run: {tor_poly}')

    print('Arguments used to produce this database:')
    print(db.arguments)

    # some source from italy S123195A from Pondrelli et al. (2006)
    source = Source(latitude=44.89,
                    longitude=11.23,
                    depth_in_m=48000.0,
                    m_rr=-1.75e15,
                    m_tt=-5.65e15,
                    m_pp=7.4e15,
                    m_rt=-1.76e15,
                    m_rp=-1.44e15,
                    m_tp=2.05e15)

    # some random receiver
    receiver = Receiver(latitude=26.34,
                        longitude=20.78,
                        network="AB",
                        station="CDE",
                        location="SY")

    # run the mode summation to get the obspy traces
    traces = db.get_seismograms(source,
                                receiver,
                                dt=1.0,
                                t0=0.0,
                                nt=1000.0,
                                seismometer_response=True)

    traces[2].filter('bandpass', freqmin=1./3600.0, freqmax=0.0075)

    # simple plot
    plt.xlabel('time in s')
    plt.ylabel('displacement in m')
    plt.title('XT')
    plt.plot(traces[2].times(), traces[2].data, 'k-')
    plt.tight_layout()
    plt.show()
