#!/usr/bin/env python
"""
Simple script to extract some eigenfrequencies from either
toroidal or spheroidal classes

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2020--2024
:license:
    | GNU Lesser General Public License, Version 3
    | (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import love, rayleigh
import numpy as np


if __name__ == '__main__':
    model = '../models/prem_iso_one_crust'
    fmax = 0.002  # maximum mesh frequency
    att = 'full'  # with attenuation

    # initialize toroidal class for isotropic prem
    lov = love(model=model, fmax=fmax)

    # solve the eigenvalue problem
    lov_out = lov.love_problem(lmax=5, attenuation_mode=att)

    # get overtone numbers
    l_uniq, l_count = np.unique(lov_out['angular orders'], return_counts=True)
    overtones = np.concatenate(
                [np.arange(lc) if lu != 1 else np.arange(1, lc + 1)
                 for lu, lc in zip(l_uniq, l_count)])

    print('Printing the eigenfrequencies for toroidal run:')
    print('mode: frequency in Mhz')
    for nn, ll, frq in zip(overtones,
                           lov_out['angular orders'],
                           lov_out['frequencies']):
        print(f'{nn}T{ll}: {frq * 1e3:1.5f}')

    # initialize spheroidal class for isotropic prem
    ray = rayleigh(model=model, fmax=fmax)
    # solve the eigenvalue problem
    ray_out = ray.rayleigh_problem(lmax=8, attenuation_mode=att)

    # get overtone numbers
    l_uniq, l_count = np.unique(ray_out['angular orders'], return_counts=True)
    overtones = np.concatenate(
                [np.arange(lc) if lu != 1 else np.arange(2, lc + 2)
                 for lu, lc in zip(l_uniq, l_count)])

    print('\nPrinting the eigenfrequencies for spheroidal run:')
    print('mode: frequency in Mhz')
    for nn, ll, frq in zip(overtones,
                           ray_out['angular orders'],
                           ray_out['frequencies']):
        print(f'{nn}S{ll}: {frq * 1e3:1.5f}')
