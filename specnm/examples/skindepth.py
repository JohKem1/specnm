#!/usr/bin/env python
"""
Skin depth for normal modes.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2022-2024
:license:
    | GNU Lesser General Public License, Version 3
    | (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
from scipy.interpolate import lagrange
from specnm import rayleigh_fg, love
from specnm.spheroidal_base import _UVP_to_U_V_P
import matplotlib.pyplot as plt


def get_kinetic_energy(cls, U=None, V=None, W=None):
    """
    Calculate kinetic energy as function of radius
    """
    r = cls.r
    rho = cls.rho

    if U is None:
        T_g = r ** 2 * rho * W ** 2
    else:
        T_g = r ** 2 * rho * (U ** 2 + V ** 2)

    return T_g


def skin_depths(rad, ekin, cls_out, cutoff=1./np.e):
    """
    Calculation of skin depth based on kinetic energy of modes till cutoff
    from surface of planet
    """
    def sign_changes(fct):
        return np.where((np.diff(np.sign(fct)) != 0))[0]

    def split(fct, n):
        return [fct[i: i + n] for i in range(0, len(fct), n)]

    # normalization of kinetic energy of mode
    ekin0 = ekin - min(ekin)
    ekin_norm = ekin0 / max(ekin0)
    ekinfct = np.flip(ekin_norm - cutoff)
    radfct = np.flip(rad)

    # interpolation based on lagrange polynomials
    np1 = cls_out['polynomial order'] + 1
    idxs_split = split(np.arange(len(ekinfct)), np1)
    ekin_split = split(ekinfct, np1)
    rad_split = split(radfct, np1)
    la_idx = np.where(idxs_split == sign_changes(ekinfct)[0])[0][0]
    zero_rad = lagrange(ekin_split[la_idx], rad_split[la_idx])(0.0)

    # transform radius to depth
    out = rad[-1] - zero_rad

    # never allow for negative output
    if out < 0.0:
        out = rad[-1]
    elif out > rad[-1]:
        out = rad[-1]

    return out


def use_specnm_sph(model, fmin=0.5 / 3600.0, fmax=0.01, att_mode='full'):
    """
    Spheroidal full gravity run of specnm
    """
    ray = rayleigh_fg(model=model, fmax=fmax * 1.5, n=9)
    ray_out = ray.rayleigh_problem(fmin=fmin,
                                   fmax=fmax,
                                   attenuation_mode=att_mode)

    U, V, P = _UVP_to_U_V_P(ray_out['eigenfunctions'], ray.sph_type)
    ekins = get_kinetic_energy(ray, U=U, V=V)

    return ekins, ray_out


def use_specnm_tor(model, fmin=0.5 / 3600.0, fmax=0.01, att_mode='full'):
    """
    Toroidal run of specnm
    """
    lov = love(model=model, fmax=fmax * 1.1, n=9)
    lov_out = lov.love_problem(fmin=fmin,
                               fmax=fmax,
                               attenuation_mode=att_mode)

    ekins = get_kinetic_energy(lov, W=lov_out['eigenfunctions'])

    return ekins, lov_out


if __name__ == '__main__':
    model = '../models/prem_ani'

    # running specnm for spheroidal and toroidal modes
    # producing also the kinetic energy in radius of the modes
    ekin_sph, prem_sph_out = use_specnm_sph(model)
    ekin_tor, prem_tor_out = use_specnm_tor(model)

    # parameters
    lmin = 8
    lmax = 100
    cutoff = 1. / np.e
    out_par = 'frequencies'
    out_xlabel = 'frequencies in mHz'

    # collecting data
    ls_prem_sph = np.unique(prem_sph_out['angular orders'])
    ls_prem_sph = ls_prem_sph[(ls_prem_sph <= lmax) & (ls_prem_sph >= lmin)]
    sds_prem_sph =\
        [skin_depths(prem_sph_out['radius'],
                     ekin_sph[prem_sph_out['angular orders'] == ll][0],
                     prem_sph_out,
                     cutoff=cutoff) / 1e3
         for ll in ls_prem_sph]
    prem_sph_frqs =\
        [prem_sph_out[out_par]
         [prem_sph_out['angular orders'] == ll][0] * 1e3
         for ll in ls_prem_sph]

    ls_prem_tor = np.unique(prem_tor_out['angular orders'])
    ls_prem_tor = ls_prem_tor[(ls_prem_tor <= lmax) & (ls_prem_tor >= lmin)]
    sds_prem_tor =\
        [skin_depths(prem_tor_out['radius'],
                     ekin_tor[prem_tor_out['angular orders'] == ll][0],
                     prem_tor_out,
                     cutoff=cutoff) / 1e3
         for ll in ls_prem_tor]
    prem_tor_frqs =\
        [prem_tor_out[out_par]
         [prem_tor_out['angular orders'] == ll][0] * 1e3
         for ll in ls_prem_tor]

    ls_gen = [ls_prem_sph, ls_prem_tor]
    sds_gen = [sds_prem_sph, sds_prem_tor]
    frqs_gen = [prem_sph_frqs, prem_tor_frqs]
    color_gen = ['tab:red', 'tab:blue']
    lstyles_gen = ['solid'] * 2
    label_gen = ['Spheroidal Earth', 'Toroidal Earth']

    # plotting
    fig, ax = plt.subplots(1, 1)

    ax.set_xlabel(out_xlabel)
    ax.set_ylabel('skin depths [km]')
    for e, _ in enumerate(ls_gen):
        ax.plot(frqs_gen[e], sds_gen[e],
                color=color_gen[e], ls=lstyles_gen[e],
                label=label_gen[e])
    ax.set_ylim(ymin=0.0)
    ax.set_xlim(xmin=1.0)
    ax.legend()

    plt.tight_layout()
    plt.show()
