#!/usr/bin/env python
"""
Calculate Slichter (1961) mode of the Earth in PREM.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import rayleigh_fg, plotter


if __name__ == '__main__':
    model = '../models/prem_ani'
    att_mode = 'full'
    fmin = 1. / (3600. * 6.)  # 1 over 6. hours as slichter should be at 5.42 h

    ray = rayleigh_fg(model=model, n=9, fmax=0.005)
    ray_out = ray.rayleigh_problem(fmin=fmin,
                                   attenuation_mode=att_mode,
                                   fmax=0.002,
                                   llist=[1])

    slichter_frq = ray_out['frequencies'][0]
    slichter_period_in_h = 1. / slichter_frq / 60. / 60.

    print(f'{slichter_period_in_h=}')

    plotter.eigenfunc_struct(ray, ray_out, 1, 0)
