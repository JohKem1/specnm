#!/usr/bin/env python
"""
Example of how to plot interactive spectrum with specnm.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2023
:license:
    | GNU Lesser General Public License, Version 3
    | (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import rayleigh, plotter


if __name__ == '__main__':
    # loading specnm class for spheroidal mode calculation for europa model
    ray = rayleigh('../models/europa', fmax=0.05)
    # calculate elastic spectrum as model has 600 Qmu model in whole planet
    # due to unknown attenuation in depth within the Jupiter moon Europa
    # need to specify low minimum frequency to get all modes
    ray_out = ray.rayleigh_problem(fmin=1e-7, attenuation_mode='elastic')

    # interactive plotting
    # double click on modes in spectrum to view eigenfunctions in depths
    # in right window - could be interesting for teaching about normal modes
    plotter.spectrum(ray, ray_out, init_mode=(2, 8))
