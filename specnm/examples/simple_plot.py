#!/usr/bin/env python
"""
Simple plotting of structured eigenfunctions from output of specnm classes for
full gravity, cowling, radial and toroidal modes.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2023--2024
:license:
    GNU Lesser General Public License, Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import plotter, love, rayleigh_fg, rayleigh, radial


if __name__ == '__main__':
    # execute spheroidal run with full gravity
    rfg = rayleigh_fg(model='../models/prem_ani', fmax=0.005)
    rfg_out = rfg.rayleigh_problem(fmax=0.0025)

    # plot using specnm structured plotter
    plotter.eigenfunc_struct(rfg, rfg_out, 1, 0)

    # execute spheroidal run with Cowling approximation (no P eigenfunction)
    ray = rayleigh(model='../models/prem_ani', fmax=0.005)
    ray_out = ray.rayleigh_problem(fmax=0.0025)

    # plot using specnm structured plotter
    plotter.eigenfunc_struct(ray, ray_out, 1, 0)

    # execute radial run (only U eigenfunction)
    rad = radial(model='../models/prem_ani', fmax=0.005)
    rad_out = rad.radial_problem(fmax=0.0025)

    # plot using specnm structured plotter
    plotter.eigenfunc_struct(rad, rad_out, 0, 0)

    # execute toroidal run (only W eigenfunction)
    lov = love(model='../models/prem_ani', fmax=0.01)
    lov_out = lov.love_problem(fmax=0.005)

    # plot using specnm structured plotter
    plotter.eigenfunc_struct(lov, lov_out, 1, 1)
