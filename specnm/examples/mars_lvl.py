#!/usr/bin/env python
"""
Example spheroidal run for Mars with molten layer on top of core.
See Khan et al. (2023) for more information on the model.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import rayleigh, plotter


if __name__ == '__main__':
    model = '../models/mars_lvl'

    fmesh = 0.01
    fmax = 0.005

    fmin = 5.0e-5  # value found by method in planet.py

    # run specnm
    ray = rayleigh(model=model, fmax=fmesh, verbose=False)
    ray_out = ray.rayleigh_problem(fmin=fmin,
                                   fmax=fmax)

    # plot the spectrum
    plotter.spectrum(ray, ray_out, init_mode=(1, 5))
