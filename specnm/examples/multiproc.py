#!/usr/bin/env python
"""
Run specnm threadsafe in HPC environment.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
import resource
from threadpoolctl import threadpool_limits
from pebble import concurrent
from concurrent.futures import TimeoutError
from specnm import rayleigh_fg


@concurrent.process(timeout=360)
def specnm_memsave(modelfile,
                   period_q,
                   obs_data,
                   maxmemory=6.0e9,
                   att_mode='eigenvector continuation stepwise'):
    def ceil(a, precision=2):
        return np.true_divide(np.ceil(a * 10**precision), 10**precision)

    # set memory limit for mp purposes
    resource.setrlimit(resource.RLIMIT_AS, (int(maxmemory), int(maxmemory)))

    with threadpool_limits(limits=1, user_api='blas'):
        ang_order_sph = np.unique([od[1] for od in obs_data.keys()])
        fmax = np.max([frq[0] for frq in obs_data.values()])
        fmaxc = ceil(fmax)

        sph_out = {}
        while len(obs_data) > 0:
            try:
                # spheroidal specnm run to get frequencies in muHz
                ray = rayleigh_fg(model=modelfile,
                                  fmax=fmaxc)
                # set period of q for internal calculation attenuation
                ray.wref = 1. / period_q * 2. * np.pi
                sph_out =\
                    ray.rayleigh_problem(
                        llist=ang_order_sph,
                        attenuation_mode=att_mode,
                        fmax=fmax)
            except RuntimeError:
                if (fmaxc < fmaxc + 0.005):
                    fmaxc += 0.005
                else:
                    break
            except Exception:
                raise
            else:
                break

    return sph_out


if __name__ == '__main__':
    modelfile = '../models/prem_ani'
    period_q = 1.0
    # some normal mode data (Laske REM page)
    obs_data = {(1, 2): [0.00068034, 0.00021],
                (1, 3): [0.00094002, 0.00005],
                (1, 4): [0.00117283, 0.0001],
                (1, 5): [0.00137004, 0.00013],
                (1, 6): [0.00152153, 0.00028],
                (1, 7): [0.00165449, 0.00025],
                (1, 8): [0.001798, 0.00024],
                (1, 9): [0.00196211, 0.00038],
                (1, 10): [0.00214684, 0.00066],
                (2, 3): [0.00124303, 0.00006],
                (2, 4): [0.00137974, 0.0002],
                (2, 5): [0.00151579, 0.00092]}

    # run specnm memory save
    future = specnm_memsave(modelfile,
                            period_q,
                            obs_data)
    try:
        # blocks until results are ready
        sph_out = future.result()
    except TimeoutError:
        future.cancel()
        raise
    except (MemoryError, Exception):
        raise

    frq0 = sph_out['frequencies'][1]
    print(f'First frequency in mHz= {frq0 * 1e3}')
    print('Absolute difference in mHz= '
          f'{abs(frq0 - obs_data[(1, 2)][0]) * 1e3}')
