#!/usr/bin/env python
"""
Sensitivity kernel plot example.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2024
:license:
    |  GNU Lesser General Public License, Version 3
    |  (http://www.gnu.org/copyleft/lgpl.html)
"""
from specnm import rayleigh_fg
import matplotlib.pyplot as plt


if __name__ == '__main__':
    ray = rayleigh_fg(model='../models/prem_ani', fmax=0.005)
    ray_out = ray.rayleigh_problem(fmax=0.0025,
                                   sensitivity_kernels='isotropic')

    kernels = ray_out['sensitivity kernels']

    # get kernels for 2S4
    rhok = kernels['rhop'][ray_out['angular orders'] == 4][2]
    vsk = kernels['vs'][ray_out['angular orders'] == 4][2]
    vpk = kernels['vp'][ray_out['angular orders'] == 4][2]

    # normalize
    rhok /= max(abs(rhok))
    vpk /= max(abs(vpk))
    vsk /= max(abs(vsk))

    # plotting
    plt.figure(figsize=(5, 10))
    plt.xlabel('sensitivity in a.u.')
    plt.ylabel('radius in km')
    plt.plot(vpk, ray.r / 1e3, color='k', ls='dotted', label='Kvp')
    plt.plot(vsk, ray.r / 1e3, color='k', ls='dashed', label='Kvs')
    plt.plot(rhok, ray.r / 1e3, color='k', ls='solid', label='Krhop')
    for fb in ray.fluid_boundaries:
        plt.axhline(fb / 1e3, c='tab:blue', ls='dashed')

    plt.ylim([0.0, ray.radius / 1e3])

    plt.legend()
    plt.tight_layout()
    plt.show()
