#!/usr/bin/env python
"""
Class for the calculation of synthetic seismograms
from eigenvalues, eigenfunctions of love and rayleigh waves.
This is part of the mode database class which is able to store modes in
a compressed h5 database for further usage.

:copyright:
    Johannes Kemper (johannes.kemper@erdw.ethz.ch), 2019-2024
:license:
    | GNU Lesser General Public License, Version 3
    | (http://www.gnu.org/copyleft/lgpl.html)
"""
# load packages
import numpy as np
# legendre polynomials
from scipy.special import lpmn
# specnm related class import
from . import love
from . import radial
from . import rayleigh
from . import rayleigh_fg
from .spheroidal_base import _UVP_to_U_V_P
try:
    # epicentral distance & azimuth and Stream and Trace from obspy
    from obspy.taup.taup_geo import calc_dist_azi
    from obspy import Stream, Trace
    # instaseis source and receiver import
    from instaseis import Source, Receiver
    # import and export databases to h5 format
    import h5py
except ImportError as e:
    print("Install specnm with seismogram option to get this functionality!")
    print("Command: pip install link[seismogram]")
    raise e


class mode_database(object):
    """
    Computes and stores a database of all mode type up to specific fmax.
    """
    def __init__(self, toroidal_modes=None, radial_modes=None,
                 spheroidal_modes=None, arguments=None):

        if (toroidal_modes is None and radial_modes is None and
                spheroidal_modes is None):
            raise ValueError('give me some modes, please')

        self.modes = {}

        if toroidal_modes is not None:
            self.modes['T'] = toroidal_modes

        if radial_modes is not None:
            self.modes['R'] = radial_modes

        if spheroidal_modes is not None:
            self.modes['S'] = spheroidal_modes

        self.arguments = arguments

    @classmethod
    def compute(cls, model, fmax, mode_types='TRS',
                gravity_mode='full', verbose=False,
                init_kwargs={}, problem_kwargs={}):
        """
        Computes a mode database for 1D radial model till maximum frequency.

        :param model: filename of model
        :type model: string
        :param fmax: maximum frequency of database
        :type fmax: float
        :param mode_types: Modetypes as in T=Toroidal, R=Radial, S=Spheroidal
        :type mode_types: string
        :param gravity_mode: Gravity mode (full, cowling, none)
        :type gravity_mode: string
        :param verbose: Boolean wether to print some output
        :type verbose: bool
        :param init_kwargs: Additional parameters for class initialization
        :type init_kwargs: dict
        :param problem_kwargs: Additional parameters into problem functions
        :type problem_kwargs: dict
        :returns: Computed mode database
        :rtype: class instance of mode_database
        """

        arguments = locals()
        arguments.pop('cls')

        assert gravity_mode in ['full', 'cowling', 'none']
        gravity = not gravity_mode == 'none'

        if 'verbose' in init_kwargs:
            verbose = init_kwargs['verbose']
        else:
            init_kwargs['verbose'] = 2 if verbose else 0

        if 'T' in mode_types:
            if verbose:
                print('\nTOROIDAL\n')
            lov = love(model=model, fmax=fmax, **init_kwargs)
            toroidal_modes = lov.love_problem(**problem_kwargs)
        else:
            toroidal_modes = None

        if 'R' in mode_types:
            if verbose:
                print('\nRADIAL\n')
            rad = radial(model=model, fmax=fmax, gravity=gravity,
                         **init_kwargs)
            radial_modes = rad.radial_problem(**problem_kwargs)
        else:
            radial_modes = None

        if 'S' in mode_types:
            if verbose:
                print('\nSPHEROIDAL\n')

            _rayleigh = rayleigh_fg if gravity_mode == 'full' else rayleigh
            ray = _rayleigh(model=model, fmax=fmax, gravity=gravity,
                            **init_kwargs)
            spheroidal_modes = ray.rayleigh_problem(**problem_kwargs)
        else:
            spheroidal_modes = None

        return cls(toroidal_modes, radial_modes, spheroidal_modes, arguments)

    @classmethod
    def read(cls, filename):
        """
        Reads a mode database.

        :param filename: Filename to read in (h5)
        :type filename: string
        :returns: Read mode database
        :rtype: class instance of mode_database
        """

        def h5_to_dict(f):
            d = {}
            if isinstance(f, h5py.Group):
                for key, value in f.items():
                    if isinstance(value, h5py.Group):
                        d[key] = h5_to_dict(value)
                    else:
                        d[key] = value[()]

                        # decode strings
                        if type(d[key]) is bytes:
                            d[key] = d[key].decode()
            return d

        with h5py.File(filename, 'r') as f:

            modes = h5_to_dict(f)

        toroidal_modes = modes['T'] if 'T' in modes else None
        radial_modes = modes['R'] if 'R' in modes else None
        spheroidal_modes = modes['S'] if 'S' in modes else None
        arguments = modes['arguments'] if 'arguments' in modes else None

        return cls(toroidal_modes, radial_modes, spheroidal_modes, arguments)

    def write_h5(self, filename):
        """
        Writes a mode database.

        :param filename: Filename to write the mode_database into (.h5)
        :type filename: string
        """

        def dict_to_h5grp(f, grpname, d):
            grp = f.create_group(grpname)
            for k, v in d.items():

                if v is None:
                    # skip None types as they cause issues in h5py
                    continue

                elif type(v) is dict:
                    # work recursively for dicts
                    dict_to_h5grp(grp, k, v)

                else:
                    grp.create_dataset(k, data=v)

        with h5py.File(filename, 'w') as f:
            for k, v in self.modes.items():
                dict_to_h5grp(f, k, v)

            if self.arguments is not None:
                dict_to_h5grp(f, 'arguments', self.arguments)

        return

    def get_seismograms(self, source, receiver, mode_types='TRS', dt=1.,
                        nt=3600, t0=None, kind='displacement',
                        seismometer_response=False, flattening=0.):
        """
        Produces seismogram from mode_database.

        :param source: Source object containing all relevant source information
        :type source: obspy Source object
        :param source: Receiver object containing all receiver information
        :type source: obspy Receiver object
        :param mode_types: Modetypes as in T=Toroidal, R=Radial, S=Spheroidal
        :type mode_types: string
        :param dt: timestep
        :type dt: float
        :param nt: length of timeseries in number of timesteps of length dt
        :type nt: integer
        :param t0: Source time function center (default None = No STF)
        :type t0: float
        :param kind: Either 'displacement', 'velocity', 'acceleration' or
                     'displacement anelastic'
        :type kind: string
        :param seismometer_response: If true add seimometer correction
                                     (default False)
        :type seismometer_response: bool
        :param flattening: flattening of planet (default 0.0=no flattening)
        :type flattening: float
        :returns: Trace object containing the three component waveforms
        :rtype: obspy Trace object
        """
        def time_function(w, t, gamma=0., kind='displacement'):
            # time factor as in D&T 10.61
            # NOTE: this is a high Q approximation, maybe better work
            # with 10.51 instead. In particular for acceleration,
            # the exact relation is even less cumbersome, see 10.63
            # JK: implemented 10.51 as 'displacement anelastic'
            if kind == 'displacement':
                tf = 1 - np.cos(w * t) * np.exp(-gamma * t)

            elif kind == 'velocity':
                tf = (w * np.sin(w * t) + gamma * np.cos(w * t)) \
                        * np.exp(-gamma * t)

            elif kind == 'acceleration':
                tf = ((w ** 2 - gamma ** 2) * np.cos(w * t) -
                      2 * gamma * w * np.sin(w * t)) * np.exp(-gamma * t)

            elif kind == 'displacement anelastic':
                wgammainvsq = (1. / (w ** 2 + gamma ** 2))
                tf = wgammainvsq *\
                    (wgammainvsq * (w ** 2 - gamma ** 2) *
                     (1 - np.cos(w * t) * np.exp(-gamma * t)) -
                     wgammainvsq * 2 * w * gamma *
                     np.sin(w * t) * np.exp(-gamma * t))

            else:
                raise ValueError

            if kind != 'displacement anelastic':
                tf *= 1. / w ** 2

            return tf

        def source_spectrum(w, t0, decay=3.5):
            # frequency domain gaussian source time function centered at t=0
            return np.exp(-(w * t0 / (2 * decay)) ** 2)

        def boxcar(w, tau_H=10.5):
            # boxcar source time function as in D&T
            return np.sin(w * tau_H) / (w * tau_H)

        assert isinstance(source, Source)
        assert isinstance(receiver, Receiver)

        M = source.tensor
        # Need this to match mineos. I suspect sth with the definition of the
        # azimuth phi or the condon shortley phase:
        M[3] = -M[3]
        M[5] = -M[5]

        # calculation of epicentral distance & azimuth
        # assuming geocentric latitudes
        theta_degrees, phi_degrees, baz_degrees = calc_dist_azi(
            source.latitude, source.longitude, receiver.latitude,
            receiver.longitude, 1.0, flattening)

        # converting from m to km > km to degree >/or degree to radians
        theta = np.deg2rad(theta_degrees)
        phi = np.deg2rad(phi_degrees)
        baz = np.deg2rad(baz_degrees)

        t = np.arange(nt) * dt
        s_Z = np.zeros_like(t)
        s_R = np.zeros_like(t)
        s_T = np.zeros_like(t)

        # TOROIDAL modes
        if 'T' in mode_types:
            r = self.modes['T']['radius']
            n = self.modes['T']['polynomial order']
            r_s = r.max() - source.depth_in_m
            r_r = r.max() - receiver.depth_in_m \
                if receiver.depth_in_m is not None else r.max()

            # interpolate eigenfunctions at source and receiver depth
            W_s = self.seismogram.lagrange_interpolation(
                self.modes['T']['eigenfunctions'], r, r_s, n)
            W_dot_s = self.seismogram.lagrange_interpolation(
                self.modes['T']['eigenfunctions_dr'], r, r_s, n)
            W_r = self.seismogram.lagrange_interpolation(
                self.modes['T']['eigenfunctions'], r, r_r, n)

            l = self.modes['T']['angular orders']
            k = self.modes['T']['wave numbers']
            w = self.modes['T']['angular frequencies']
            gamma = self.modes['T']['gamma']

            nAl_t = self.seismogram.nAl_toroidal(*M, W_r, W_s, W_dot_s,
                                                 theta, phi, r_s, l, k)

            if t0 is not None:
                nAl_t *= source_spectrum(w, t0)[:, None]

            for i, (_w, _gamma) in enumerate(zip(w, gamma)):
                tf = time_function(_w, t, _gamma, kind=kind)
                s_Z += nAl_t[i, 0] * tf
                s_R += nAl_t[i, 1] * tf
                s_T += nAl_t[i, 2] * tf

        # SPHEROIDAL modes
        if 'S' in mode_types:
            sph_type = (self.modes['S']['eigenfunctions'].shape[-1] //
                        self.modes['S']['radius'].shape[0])

            r = self.modes['S']['radius']
            n = self.modes['S']['polynomial order']
            r_s = r.max() - source.depth_in_m
            r_r = r.max() - receiver.depth_in_m \
                if receiver.depth_in_m is not None else r.max()

            # interpolate eigenfunctions at source and receiver depth
            U, V, P = _UVP_to_U_V_P(
                self.modes['S']['eigenfunctions'][:, :], sph_type)
            U_s = self.seismogram.lagrange_interpolation(U, r, r_s, n)
            V_s = self.seismogram.lagrange_interpolation(V, r, r_s, n)

            U_dot, V_dot, P_dot = _UVP_to_U_V_P(
                self.modes['S']['eigenfunctions_dr'][:, :], sph_type)
            U_dot_s = self.seismogram.lagrange_interpolation(U_dot, r, r_s, n)
            V_dot_s = self.seismogram.lagrange_interpolation(V_dot, r, r_s, n)

            U_r = self.seismogram.lagrange_interpolation(U, r, r_r, n)
            V_r = self.seismogram.lagrange_interpolation(V, r, r_r, n)
            P_r = self.seismogram.lagrange_interpolation(P, r, r_r, n)

            l = self.modes['S']['angular orders']
            k = self.modes['S']['wave numbers']
            w = self.modes['S']['angular frequencies']
            gamma = self.modes['S']['gamma']

            if seismometer_response:
                g_acc_r = self.seismogram.lagrange_interpolation(
                    self.modes['S']['gravitational acceleration'], r, r_r, n)
                # D&T 10.72
                U_r_free = 2 / w ** 2 * g_acc_r / r_r * U_r
                U_r_pot = (l + 1) / w ** 2 / r_r * P_r

                V_r_tilt = - k / w ** 2 * g_acc_r / r_r * U_r
                V_r_pot = - k / w ** 2 / r_r * P_r

                # avoid cross effects of changing U_r on V_r
                U_r += U_r_free + U_r_pot
                V_r += V_r_pot + V_r_tilt

            nAl_s = self.seismogram.nAl_spheroidal(*M, U_r, V_r, U_s,
                                                   U_dot_s, V_s, V_dot_s,
                                                   theta, phi, r_s, l, k)
            if t0 is not None:
                nAl_s *= source_spectrum(w, t0)[:, None]

            for i, (_w, _gamma) in enumerate(zip(w, gamma)):
                tf = time_function(_w, t, _gamma, kind=kind)
                s_Z += nAl_s[i, 0] * tf
                s_R += nAl_s[i, 1] * tf
                s_T += nAl_s[i, 2] * tf

        # RADIAL modes
        if 'R' in mode_types:
            r = self.modes['R']['radius']
            n = self.modes['R']['polynomial order']
            r_s = r.max() - source.depth_in_m
            r_r = r.max() - receiver.depth_in_m \
                if receiver.depth_in_m is not None else r.max()

            # interpolate eigenfunctions at source and receiver depth
            U_s = self.seismogram.lagrange_interpolation(
                self.modes['R']['eigenfunctions'], r, r_s, n)
            U_dot_s = self.seismogram.lagrange_interpolation(
                self.modes['R']['eigenfunctions_dr'], r, r_s, n)
            U_r = self.seismogram.lagrange_interpolation(
                self.modes['R']['eigenfunctions'], r, r_r, n)

            l = self.modes['R']['angular orders']
            k = self.modes['R']['wave numbers']
            w = self.modes['R']['angular frequencies']
            gamma = self.modes['R']['gamma']

            if seismometer_response:
                # D&T 10.72, P(a) = 0 for radial modes
                g_acc_r = self.seismogram.lagrange_interpolation(
                    self.modes['R']['gravitational acceleration'], r, r_r, n)
                U_r_free = 2 / w ** 2 * g_acc_r / r_r * U_r
                U_r += U_r_free

            nAl_r = self.seismogram.nAl_radial(*M,
                                               U_r, U_s, U_dot_s,
                                               theta, phi, r_s)
            if t0 is not None:
                nAl_r *= source_spectrum(w, t0)

            for i, (_w, _gamma) in enumerate(zip(w, gamma)):
                tf = time_function(_w, t, _gamma, kind=kind)
                s_Z += nAl_r[i] * tf

        sp = np.sin(baz + np.pi)
        cp = np.cos(baz + np.pi)

        s_N = cp * s_R - sp * s_T
        s_E = sp * s_R + cp * s_T

        st = Stream()
        for comp, data in zip(['Z', 'R', 'T', 'N', 'E'],
                              [s_Z, s_R, s_T, s_N, s_E]):

            tr = Trace(
                data=data,
                header={
                    "delta": dt,
                    # "starttime": starttime,
                    # "station": receiver.station,
                    # "network": receiver.network,
                    # "location": receiver.location,
                    "channel": "X" + comp,
                },
            )
            st += tr

        return st

    class seismogram:
        @staticmethod
        def nAl_spheroidal(m_rr, m_tt, m_pp, m_rt, m_rp, m_tp,
                           U_r, V_r, U_s, U_dot_s, V_s, V_dot_s,
                           theta, phi, r_s, ll, kk):
            """
            Calculates the factor nAl in D&T equation 10.61 for
            spheroidal modes.

            :type m_rr: ndarray (float)
            :param m_rr: moment tensor components in r, theta, phi in Nm
            :type m_tt: ndarray (float)
            :param m_tt: moment tensor components in r, theta, phi in Nm
            :type m_pp: ndarray (float)
            :param m_pp: moment tensor components in r, theta, phi in Nm
            :type m_rt: ndarray (float)
            :param m_rt: moment tensor components in r, theta, phi in Nm
            :type m_rp: ndarray (float)
            :param m_rp: moment tensor components in r, theta, phi in Nm
            :type m_tp: ndarray (float)
            :param m_tp: moment tensor components in r, theta, phi in Nm
            :type U_r: float
            :param U_r: U eigenfunction at receiver depth
            :type V_r: float
            :param V_r: V eigenfunction at receiver depth
            :type U_s: float
            :param U_s: U eigenfunction at source depth
            :type U_dot_s: float
            :param U_dot_s: derivative of U eigenfunction at source depth
            :type V_s: float
            :param V_s: V eigenfunction at source depth
            :type V_dot_s: float
            :param V_dot_s: derivative of V eigenfunction at source depth
            :param theta: float
            :type theta: epicentral distance
            :type phi: float
            :param phi: polar angle
            :type r_s: float
            :param r_s: source radius in m
            :type ll: int
            :param ll: angular order
            :type kk: float
            :param kk: wavenumber
            :returns: nAl coefficients for spheroidal type modes
            :rtype: ndarray (float)
            """
            # legendre polynomials
            Plm, dPlm = mode_database.seismogram.Plm_cos_theta(ll.max(), theta)

            # produce the summation coefficients (10.54 - 10.59)
            A0 = m_rr * U_dot_s\
                + (m_tt + m_pp) * (U_s - 0.5 * kk * V_s) / r_s
            A1 = m_rt * (V_dot_s - V_s / r_s + kk * U_s / r_s) / kk
            A2 = 0.5 * (m_tt - m_pp) * V_s / (kk * r_s)

            B1 = m_rp * (V_dot_s - V_s / r_s + kk * U_s / r_s) / kk
            B2 = m_tp * V_s / (kk * r_s)

            # calculation of A from Dahlen&Tromp eq. 10.53
            A = Plm[ll, 0] * A0 \
                + Plm[ll, 1] * (A1 * np.cos(phi) + B1 * np.sin(phi)) \
                + Plm[ll, 2] * (A2 * np.cos(2. * phi) + B2 * np.sin(2. * phi))

            # derivatives for D*A
            dA_dphi = Plm[ll, 1] * (- A1 * np.sin(phi) + B1 * np.cos(phi)) \
                + Plm[ll, 2] * (- A2 * 2. * np.sin(2. * phi)
                                + B2 * 2. * np.cos(2. * phi))

            dA_dtheta = dPlm[ll, 0] * A0\
                + dPlm[ll, 1] * (A1 * np.cos(phi) + B1 * np.sin(phi)) \
                + dPlm[ll, 2] * (A2 * np.cos(2. * phi) + B2 * np.sin(2. * phi))

            # Dahlen & Tromp eq. (10.60)
            # states that the eigenfunctions in the displacement op.
            # have to be evaluated at the receiver meaning F_r
            DA_r = U_r * A
            DA_theta = V_r * dA_dtheta / kk
            DA_phi = V_r / np.sin(theta) * dA_dphi / kk

            fact = ((2. * ll + 1.) / (4. * np.pi))

            nAl = np.c_[DA_r, DA_theta, DA_phi] * fact[:, None]

            return nAl

        @staticmethod
        def nAl_toroidal(m_rr, m_tt, m_pp, m_rt, m_rp, m_tp,
                         W_r, W_s, W_dot_s,
                         theta, phi, r_s, ll, kk):
            """
            Calculates the factor nAl in D&T equation 10.61 for toroidal modes.

            :type m_rr: ndarray (float)
            :param m_rr: moment tensor components in r, theta, phi in Nm
            :type m_tt: ndarray (float)
            :param m_tt: moment tensor components in r, theta, phi in Nm
            :type m_pp: ndarray (float)
            :param m_pp: moment tensor components in r, theta, phi in Nm
            :type m_rt: ndarray (float)
            :param m_rt: moment tensor components in r, theta, phi in Nm
            :type m_rp: ndarray (float)
            :param m_rp: moment tensor components in r, theta, phi in Nm
            :type m_tp: ndarray (float)
            :type W_r: float
            :param W_r: W eigenfunction at receiver depth
            :type W_s: float
            :param W_s: W eigenfunction at source depth
            :type W_dot_s: float
            :param W_dot_s: derivative of W eigenfunction at source depth
            :param theta: float
            :type theta: epicentral distance
            :type phi: float
            :param phi: polar angle
            :type r_s: float
            :param r_s: source radius in m
            :type ll: integer
            :param ll: angular order
            :type kk: float
            :param kk: wavenumber
            :returns: nAl coefficients for toroidal type modes
            :rtype: ndarray (float)
            """
            # legendre polynomials
            Plm, dPlm = mode_database.seismogram.Plm_cos_theta(ll.max(), theta)

            # produce the summation coefficients (10.54 - 10.59)
            A1 = - m_rp * (W_dot_s - W_s / r_s) / kk
            A2 = - m_tp * W_s / (kk * r_s)

            B1 = m_rt * (W_dot_s - W_s / r_s) / kk
            B2 = 0.5 * (m_tt - m_pp) * W_s / (kk * r_s)

            # derivatives for D*A
            dA_dphi = Plm[ll, 1] * (- A1 * np.sin(phi) + B1 * np.cos(phi)) \
                + Plm[ll, 2] * (- A2 * 2. * np.sin(2. * phi)
                                + B2 * 2. * np.cos(2. * phi))

            dA_dtheta = + dPlm[ll, 1] * (A1 * np.cos(phi) + B1 * np.sin(phi)) \
                + dPlm[ll, 2] * (A2 * np.cos(2. * phi) + B2 * np.sin(2. * phi))

            # Dahlen & Tromp eq. (10.60)
            # states that the eigenfunctions in the displacement op.
            # have to be evaluated at the receiver meaning F_r
            DA_r = np.zeros_like(W_r)
            DA_theta = W_r / np.sin(theta) * dA_dphi / kk
            DA_phi = -W_r * dA_dtheta / kk

            fact = ((2. * ll + 1.) / (4. * np.pi))

            nAl = np.c_[DA_r, DA_theta, DA_phi] * fact[:, None]

            return nAl

        @staticmethod
        def nAl_radial(m_rr, m_tt, m_pp, m_rt, m_rp, m_tp,
                       U_r, U_s, U_dot_s, theta, phi, r_s):
            """
            Calculates the factor nAl in D&T equation 10.61 for radial modes.

            :type m_rr: ndarray (float)
            :param m_rr: moment tensor components in r, theta, phi in Nm
            :type m_tt: ndarray (float)
            :param m_tt: moment tensor components in r, theta, phi in Nm
            :type m_pp: ndarray (float)
            :param m_pp: moment tensor components in r, theta, phi in Nm
            :type m_rt: ndarray (float)
            :param m_rt: moment tensor components in r, theta, phi in Nm
            :type m_rp: ndarray (float)
            :param m_rp: moment tensor components in r, theta, phi in Nm
            :type m_tp: ndarray (float)
            :type U_r: float
            :param U_r: U eigenfunction at receiver depth
            :type U_s: float
            :param U_s: U eigenfunction at source depth
            :type U_dot_s: float
            :param U_dot_s: derivative of U eigenfunction at source depth
            :param theta: float
            :type theta: epicentral distance
            :type phi: float
            :param phi: polar angle
            :type r_s: float
            :param r_s: source radius in m
            :returns: nAl coefficients for radial type modes
            :rtype: ndarray (float)
            """
            ll = 0

            # legendre polynomials
            Plm, dPlm = mode_database.seismogram.Plm_cos_theta(ll, theta)

            # produce the summation coefficients (10.54 - 10.59)
            A0 = m_rr * U_dot_s + (m_tt + m_pp) * U_s / r_s
            A1 = m_rt * U_s / r_s
            B1 = m_rp * U_s / r_s

            # calculation of A from Dahlen&Tromp eq. 10.53
            A = Plm[ll, 0] * A0 \
                + Plm[ll, 1] * (A1 * np.cos(phi) + B1 * np.sin(phi))

            # Dahlen & Tromp eq. (10.60)
            # states that the eigenfunctions in the displacement op.
            # have to be evaluated at the receiver meaning F_r
            DA_r = U_r * A

            fact = 1. / (4. * np.pi)

            nAl = DA_r * fact

            return nAl

        @staticmethod
        def Plm_cos_theta(lmax, theta, eps=1e-100):
            """
            Gets the associated legendre polynomials
            from scipy special functions
            without Condon-Shortley phase (-1) ** m

            :type lmax: integer
            :param lmax: maximum order to be produced
            :type theta: float
            :param theta: epicentral distance
            :returns: Legendre polynomials, Derivative of Legendre polynomials
            :rtype: Tuple(ndarray (float), ndarray (float))
            """
            theta = np.clip(theta, eps, np.pi - eps)

            # get associated lagrange polynomials and their derivatives
            Pml, dPml = lpmn(2, max(2, lmax), np.cos(theta))

            # correct weird ordering
            Plm = Pml.T
            dPlm = dPml.T

            # multiply derivatives by inner derivative
            dPlm *= -np.sin(theta)

            # remove condon shortley phase
            Plm[:, 1] *= -1
            dPlm[:, 1] *= -1

            return Plm, dPlm

        @staticmethod
        def lagrange_interpolation(f, r, x, n):
            """
            Do a Lagrange interpolation of function f sampled at points r,
            evaluated at point x, assuming a polynomial order n.

            :type f: np.array
            :param f: function to interpolate at x
            :type r: float
            :param r: radius to interpolate to
            :type x: ndarray (float)
            :param x: radius nodes at which function f is defined
            :type n: integer
            :param n: polynomial order of lagrange element
            :returns: interpolated value at r
            :rtype: float
            """
            assert r.min() <= x <= r.max()

            # find element
            eidx = np.argmax(r >= x) // (n + 1)

            # element indices
            idx1 = eidx * (n + 1)
            idx2 = (eidx + 1) * (n + 1)

            # calculate lagrange interpolator
            lvec = np.ones(n+1)
            _r = r[idx1:idx2]

            for i in np.arange(n + 1):
                for j in np.arange(n + 1):
                    if (j == i):
                        continue

                    lvec[i] *= (x - _r[j]) / (_r[i] - _r[j])

            return np.inner(lvec, f[..., idx1:idx2])
