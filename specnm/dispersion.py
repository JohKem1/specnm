#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Classes to provide convenient interfaces to pysw to compute Love and rayleigh
surfaces waves.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2017
    Federico D. Munch, 2017
    Johannes Kemper, 2018
:license:
    None
"""
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline

from . import rayleigh
from . import love


class SurfaceWaveDispersion(object):
    """
    A class to provide a convenient interface to specnm
    (spectral element normal mode code)
    """

    def __init__(self, modelfile, mtype='love', number_of_branches=None,
                 shortest_period=50., l0=50,
                 dlog10l=0.018, interpolation_order=3,
                 extrapolation_limit=1e-5, attenuation=False,
                 gravity_on=True):
        """
        :param modelfile: path to the model file
        :type modelfile: string
        :param mtype: Surface wave type: 'love' or 'rayleigh'
        :type mtype: string
        :param number_of_branches: Number of overtone branches.
        :type number_of_branches: integer
        :param shortest_period: shortest period to compute dispersion curves
        :type shortest_period: float
        :param l0: Initial angular order (controls minimum frequency computed)
        :type l0: int
        :param dlog10l: log10 of angular order step
                        l = int(10 ** ( np.log10(l) + dlog10l ))
        :type dlog10l: float
        :param interpolation_order: order used for spline interpolation of the
        dispersion curves, should be in [1, 5]
        :type interpolation_order: integer
        :param extrapolation_limit: maximum frequency difference to interpolate
        the dispersion curve outside the range where it was computed
        :type extrapolation_limit: float
        :param gravity_on: Take into account gravity effect (Cowling approx.)
                           ONLY AVAILABLE FOR RAYLEIGH WAVES
        :type gravity_on: boolean
        :param attenuation: Consider attenuation effect
        :type attenuation: boolean
        """
        if number_of_branches is None:
            number_of_branches = 10

        self.modelfile = modelfile
        self.mtype = mtype
        self.min_overtone_number = 0
        self.number_of_branches = number_of_branches
        self.shortest_period = shortest_period
        self.max_overtone_number = number_of_branches - 1
        self.overtone_numbers = range(number_of_branches)
        self.l0 = l0
        self.dlog10l = dlog10l
        self.attenuation = attenuation
        self.gravity_on = gravity_on

        att_type = 'eigenvector continuation' if attenuation else 'elastic'

        if (mtype == 'love'):
            lov = love.love(model=modelfile,
                            fmax=1 / shortest_period)
            modearray = lov.love_problem(l0=l0,
                                         logskip=True,
                                         dlog10l=dlog10l,
                                         attenuation_mode=att_type,
                                         n_total=number_of_branches)
            self.planet_radius = lov.radius
        else:
            ray = rayleigh.rayleigh(
                model=modelfile,
                fmax=1 / shortest_period,
                gravity=gravity_on)
            modearray =\
                ray.rayleigh_problem(l0=l0,
                                     logskip=True,
                                     dlog10l=dlog10l,
                                     attenuation_mode=att_type,
                                     n_total=number_of_branches)
            self.planet_radius = ray.radius

        angular_order_list = np.unique(modearray['angular orders'])

        # build interpolation objects for each overtone number
        self.__group_velocity = {}
        self.__phase_velocity = {}
        self.__min_freq = {}
        self.__max_freq = {}

        f = modearray['frequencies']
        pv = modearray['phase velocities'] * self.planet_radius
        gv = modearray['group velocities'] * self.planet_radius

        for nn in range(number_of_branches):
            indices = [np.where(modearray['angular orders'] == ll)[0][nn - 1]
                       for ll in angular_order_list
                       if sum(modearray['angular orders'] == ll) >= nn]
            fint = f[indices]

            # min and max frequency can depend on overtone number
            self.__min_freq[nn] = fint.min() - extrapolation_limit
            self.__max_freq[nn] = fint.max() + extrapolation_limit

            self.__group_velocity[nn] = \
                InterpolatedUnivariateSpline(fint,
                                             gv[indices],
                                             k=interpolation_order)
            self.__phase_velocity[nn] = \
                InterpolatedUnivariateSpline(fint,
                                             pv[indices],
                                             k=interpolation_order)

    def __check_overtone_number(self, overtone_number):
        if overtone_number not in self.overtone_numbers:
            raise ValueError(
                'overtone number %d not included in initialization' %
                overtone_number)

    def __check_frequencies(self, frequencies, overtone_number):
        if (np.min(frequencies) < self.__min_freq[overtone_number] or
                np.max(frequencies) > self.__max_freq[overtone_number]):
            raise ValueError(
                'period outside the range covered by this instance')

    def travel_time(self, period, distance, units='km', overtone_number=0):
        """
        Travel time as a function of period and distance.

        :param period: seismic period
        :type period: float
        :param distance: epicentral distance in km, m, degree or radian
        :type distance: float
        :param units: units of the distance
        :type units: string
        :param overtone_number: overton number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        """
        if units == 'km':
            distance_km = distance
        elif units == 'm':
            distance_km = distance / 1e3
        elif units == 'degree':
            distance_km = np.radians(distance) * self.planet_radius / 1e3
        elif units == 'radian':
            distance_km = distance * self.planet_radius / 1e3
        else:
            raise ValueError('unknown units: ' + units)

        self.__check_overtone_number(overtone_number)

        return (distance_km /
                self.group_velocity(period, overtone_number))

    def phase_velocity(self, period, overtone_number=0):
        """
        Phase velocity as a function of period.

        :param period: seismic period
        :type period: float
        :param overtone_number: overton number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        """

        f = 1. / period

        self.__check_overtone_number(overtone_number)
        self.__check_frequencies(f, overtone_number)

        ret_val = self.__phase_velocity[overtone_number](f)
        if ret_val.size == 1:
            ret_val = float(ret_val)
        return ret_val

    def group_velocity(self, period, overtone_number=0):
        """
        Group velocity as a function of period.

        :param period: seismic period
        :type period: float
        :param overtone_number: overton number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        """

        f = 1. / period

        self.__check_overtone_number(overtone_number)
        self.__check_frequencies(f, overtone_number)

        ret_val = self.__group_velocity[overtone_number](f)
        if ret_val.size == 1:
            ret_val = float(ret_val)
        return ret_val

    def min_period(self, overtone_number=0):
        """
        Minimum period that can be queried from this instance.

        :param overtone_number: overton number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        """
        self.__check_overtone_number(overtone_number)
        return 1. / self.__max_freq[overtone_number]

    def max_period(self, overtone_number=0):
        """
        Maximum period that can be queried from this instance.

        :param overtone_number: overton number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        """
        self.__check_overtone_number(overtone_number)
        return 1. / self.__min_freq[overtone_number]

    def min_freq(self, overtone_number=0):
        """
        Minimum frequency that can be queried from this instance.

        :param overtone_number: overton number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        """
        self.__check_overtone_number(overtone_number)
        return self.__min_freq[overtone_number]

    def max_freq(self, overtone_number=0):
        """
        Maximum frequency that can be queried from this instance.

        :param overtone_number: overton number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        """
        self.__check_overtone_number(overtone_number)
        return self.__max_freq[overtone_number]

    def periods(self, overtone_number=0, nsamp=50, bound=0.):
        """
        Convenience method to get linearly spaced periods in the range covered
        by this instance for each overtone number.

        :param overtone_number: overton number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        :param nsamp: number of samples
        :type nsamp: integer
        """
        self.__check_overtone_number(overtone_number)
        return np.linspace(self.min_period(overtone_number)*(1.+bound),
                           self.max_period(overtone_number)*(1.-bound), nsamp,
                           endpoint=True)

    def frequencies(self, overtone_number=0, nsamp=50):
        """
        Convenience method to get linearly spaced frequencies in the range
        covered by this instance for each overtone number.

        :param overtone_number: overtone number to be used; constrained by the
            initialization parameters of the class
        :type overtone_number: integer
        :param nsamp: number of samples
        :type nsamp: integer
        """
        self.__check_overtone_number(overtone_number)
        return np.linspace(self.min_freq(overtone_number),
                           self.max_freq(overtone_number), nsamp,
                           endpoint=True)

    def plot(self, abscissa='period', logscale=False, nsamp=100, marker=False,
             mode='both', show=True):
        """
        Plot dispersion curves,

        :param abscissa: plot as a function of 'period' or 'frequency'
        :type abscissa: string
        :param logscale: use logarithmic x-axis
        :type logscale: Boolean
        :param nsamp: number of samples
        :type nsamp: integer
        :param marker: Add markers at the values returned from the library.
        :type markers: Boolean
        :param mode: plot 'phase' or 'group' velocity or 'both'.
        :type mode: string
        :param show: Show the figure or return it.
        :type show: Boolean
        """

        # fig = plt.figure()
        fig = plt.gcf()
        ax = plt.gca()

        # get colors
        color_cycle = ax._get_lines.color_cycle
        colors = \
            [color_cycle.next() for n in np.arange(self.number_of_branches)]

        for n in (np.arange(self.number_of_branches) +
                  self.min_overtone_number):

            if abscissa == 'period':
                p = self.periods(n, nsamp)
                f = 1 / p

                def xmap(x): return 1. / x
            elif abscissa == 'frequency':
                f = self.frequencies(n, nsamp)
                p = 1 / f

                def xmap(x): return x
            else:
                raise ValueError

            color_id = n - self.min_overtone_number

            if mode in ['both', 'phase']:
                plt.plot(xmap(f), self.phase_velocity(p, n), ls='-',
                         color=colors[color_id])
                if marker:
                    plt.plot(xmap(self.__phase_velocity[n]._data[0]),
                             self.__phase_velocity[n]._data[1], ls='None',
                             marker='x', color=colors[color_id])

            if mode in ['both', 'group']:
                plt.plot(xmap(f), self.group_velocity(p, n), ls='--',
                         color=colors[color_id])

                if marker:
                    plt.plot(xmap(self.__group_velocity[n]._data[0]),
                             self.__group_velocity[n]._data[1], ls='None',
                             marker='o', color=colors[color_id])

        if abscissa == 'period':
            plt.xlabel('period / s')
        elif abscissa == 'frequency':
            plt.xlabel('frequency / Hz')

        if logscale:
            plt.gca().set_xscale('log')

        plt.ylabel('velocity / (km / s)')

        title_map = {'rayleigh': 'Rayleigh Wave Dispersion Curve',
                     'love': 'Love Wave Dispersion Curve'}
        plt.title(title_map[self.mtype])

        line1 = mlines.Line2D([], [], color='k', label='phase velocity',
                              ls='-')
        line2 = mlines.Line2D([], [], color='k', label='group velocity',
                              ls='--')
        plt.legend(handles=[line1, line2])

        if show:
            plt.show()
        else:
            return fig
