#!/usr/bin/env python
"""
Wrapper for slepc eigenvalue solver to efficiently compute all eigenvalues in a
given interval.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2021
    Johannes Kemper (johanneskemper@gmx.de), 2024
:license:
    GNU Lesser General Public License , Version 3
    (http://www.gnu.org/copyleft/lgpl.html)
"""
import numpy as np
from petsc4py import PETSc
from slepc4py import SLEPc


def multishift_slepc(K, M, fmin, fmax=None, n_total=None, n_per_shift=20,
                     residual_tol=1e-12, maxiter=20, max_similarity=0.999):
    """
    Solve an eigenvalue problem of the form
    K v = (2 pi f) ^ 2 M v
    and find all eigenfrequencies f in [fmin, fmax]. Uses shift invert to find
    n_per_shift values near a shift value and then varies the shift value
    within [fmin, fmax] to find all eigenvalues. Duplicates are filtered based
    on the scalarproduct of the eigenvectors > max_similarity.

    It is assumed that both K and M are real valued and symmetric and M is
    positive (semi) definite.

    :type K: scipy.sparse.lil_matrix
    :param K: stiffness matrix in sparse format
    :type M: scipy.sparse.lil_matrix
    :param M: mass matrix in sparse format
    :type fmin: float
    :param fmin: minimum frequency
    :type fmax: float
    :param fmax: maximum frequency (default=None)
    :type n_total: integer
    :param n_total: total eigenpairs to calculate (default=None)
    :type n_per_shift: integer
    :param n_per_shift: shift the shift invert for every 20 modes (default=20)
    :type residual_tol: float
    :param residual_tol: tolerance of the solver (default=1e-12)
    :type maxiter: integer
    :param maxiter: maximum number of iterations of the krylovschur solver
    :type max_similarity: float
    :param max_similarity: maximum similarity used in matching of
                           eigenfunctions at bracket borders (ev.dot(ev))
    :returns: Eigenpairs as (eigenfrequencies, eigenvectors)
    :rtype: Tuple(ndarray (float), ndarray(float))
    """
    def _get_similarity_mask(eigenvectors, max_similarity=0.999):
        eigenvectors = np.array(eigenvectors)
        similarity = np.abs(eigenvectors.dot(eigenvectors.T))
        similarity = np.tril(similarity, k=-1)
        return np.all(similarity < max_similarity, axis=1)

    if fmax is not None:
        if not fmin < fmax:
            raise ValueError('fmin should be smaller than fmax')

    if n_total is not None:
        if (n_total <= 0):
            raise ValueError('n_total has to be a positive integer.')
        n_per_shift = max(15, min(n_per_shift, n_total))

    # initalize matrices in parallel
    K_mat = PETSc.Mat().createAIJ(size=K.shape,
                                  csr=(K.indptr,
                                       K.indices,
                                       K.data),
                                  comm=PETSc.COMM_WORLD)
    K_mat.assemble()

    M_mat = PETSc.Mat().createAIJ(size=M.shape,
                                  csr=(M.indptr,
                                       M.indices,
                                       M.data),
                                  comm=PETSc.COMM_WORLD)
    M_mat.assemble()

    Mnorm = M.max()

    # setup eigenvalue solver
    eigv = SLEPc.EPS().create(PETSc.COMM_WORLD)
    eigv.setOperators(K_mat, M_mat)

    # setting up problem type
    eigv.setProblemType(SLEPc.EPS.ProblemType.GHEP)
    eigv.setType('krylovschur')

    # convergence options
    eigv.setConvergenceTest(2)  # 2: weighted by matrix norm
    eigv.setTolerances(residual_tol, maxiter)

    # setting up shift-invert
    st = eigv.getST()
    st.setType('sinvert')
    eigv.setWhichEigenpairs(eigv.Which.TARGET_REAL)

    # preconditioning
    ksp = st.getKSP()
    pc = ksp.getPC()
    pc.setFactorSolverType('mumps')  # mumps for better parallelization

    # setting up real positive interval for eigenvalues
    rg = eigv.getRG()
    rg.setType('interval')

    # storage for eigenvalues and eigenvectors
    eigenfrequencies = []
    eigenvectors = []
    vr, _ = K_mat.getVecs()

    # init frequencies for shift
    _fmin = fmin

    ev_min = (2 * np.pi * fmin) ** 2

    while True:
        if fmax is not None and _fmin >= fmax:
            break

        if n_total is not None:
            if len(eigenvectors) > 0:
                n_unique = _get_similarity_mask(eigenvectors,
                                                max_similarity).sum()
                if n_unique >= n_total:
                    break

        # avoid using exactly an eigenvalue, which leads to larger residuals
        # as the shift invert becomes singular
        # as we might hit an eigenvalue by accident, adding a random term so if
        # convergence is bad, on next iteration we may be luckier
        _fmin *= 1 - 1e-4 - np.random.rand() * 1e-3
        shift = (2 * np.pi * _fmin) ** 2

        # set new shift and interval
        eigv.setTarget(shift)
        rg.setIntervalEndpoints(max(shift, ev_min),
                                PETSc.INFINITY,
                                -PETSc.INFINITY,
                                PETSc.INFINITY)

        # try smaller krylov spaces first, more efficient but less stable
        # TODO: monitor for real problems if we ever hit this issue, as this
        #   seems to have become much better with the backwards norm and the
        #   symmetric matrices
        for ncv in [  # PETSc.DECIDE,
                    4 * n_per_shift,
                    8 * n_per_shift,
                    16 * n_per_shift,
                    32 * n_per_shift]:

            # set the krylov space size
            eigv.setDimensions(n_per_shift, ncv)

            # SOLVE the general eigenvalue problem with slepsc
            eigv.solve()

            # get converged eigenpairs
            nconv = eigv.getConverged()

            if not eigv.getConvergedReason() == 1:
                # less eigenvalues converged than requested
                if nconv > 1:
                    # raise RuntimeError('not converged')
                    # we can still move on if there is at least 2 evs
                    break
                else:
                    print('warning: not enough eigenvalues found')
                    print('nconv       : ', nconv)
                    print('reason      : ', eigv.getConvergedReason())
                    print('niter       : ', eigv.getIterationNumber())
                    print('ncv         : ', ncv)
                    print('n_per_shift : ', n_per_shift)
                    print('_fmin       : ', _fmin)
                    print('shift       : ', shift)
                    if n_total is not None:
                        print('n_total    : ', n_total)
                        print('n_unique    : ', n_unique)
            else:
                # enough evs have been found, move on
                break

            # only 1 ev found, likely the one that was used to compute the
            # shift
            # try closing the gap with an increased krylov space dimension
            # this is often the case when fmin was chosen to small or
            # there is a large gap between the spurious modes/undertones and
            # the fundamental mode
        else:
            # if nothing else worked, we may still try a new random shift and
            # hope we are lucky
            print(f'trying new shift {_fmin}')
            continue

        # # check sorting for debugging, seems true always
        # frequencies = [eigv.getEigenpair(i, vr).real ** 0.5 / 2 / np.pi
        #                for i in range(nconv)]
        # assert np.all(frequencies == sorted(frequencies))

        for i in range(nconv):
            k = eigv.getEigenpair(i, vr).real

            # check residual weighted by norm of mass matrix
            residual = (
                np.linalg.norm((K - k * M).dot(np.array(vr)))
                / np.linalg.norm(np.array(vr)) / Mnorm
            )

            # if the residual is too large, solve again with a shift closer to
            # this ev
            if residual > residual_tol:
                _fmin = k ** 0.5 / 2 / np.pi
                break

            eigenfrequencies.append(k ** 0.5 / 2 / np.pi)

            # renormalize, as slepc seems to norm by the same norm as used in
            # the residual computation
            _v = np.array(vr)
            _v /= np.linalg.norm(_v)
            eigenvectors.append(_v)

        else:
            _fmin = eigenfrequencies[-1]

    eigenfrequencies = np.array(eigenfrequencies)
    eigenvectors = np.array(eigenvectors)

    if len(eigenfrequencies) > 0:
        # filter duplicate modes at the boundary of the intervals based on the
        # scalar product between eigenvectors
        mask = _get_similarity_mask(eigenvectors, max_similarity)

        # also filter below max frequency
        if fmax is not None:
            mask *= eigenfrequencies < fmax

        eigenfrequencies = eigenfrequencies[mask]
        eigenvectors = eigenvectors[mask]

        if n_total is not None:
            if (fmax is None and mask.sum() < n_total):
                raise RuntimeError
            eigenfrequencies = eigenfrequencies[:n_total]
            eigenvectors = eigenvectors[:n_total]

    return eigenfrequencies, eigenvectors
